<?php

namespace App\Traits\Order;

use App\Enums\OrderStatus;

trait OrderStatusTrait
{
    public function isPending()
    {
        return $this->status === OrderStatus::PENDING;
    }

    public function isWaiting()
    {
        return $this->status === OrderStatus::WAITING;
    }

    public function isProcessing()
    {
        return $this->status === OrderStatus::PROCESSING;
    }

    public function isDelivered()
    {
        return $this->status === OrderStatus::DELIVERED;
    }

    public function isCanceled()
    {
        return $this->status === OrderStatus::CANCELED;
    }
}