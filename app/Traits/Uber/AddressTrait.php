<?php

namespace App\Traits\Uber;

trait AddressTrait
{
    public function buildAddress(array $data)
    {
        $data = collect($data);

        $streetAddress = [
            $data->get('address'),
            $data->get('address_extra'),
        ];

        $dropoffAddress = collect()
            ->put('street_address', array_filter($streetAddress))
            ->put('city', 'São Paulo')
            ->put('state', 'SP')
            ->put('zip_code', $data->get('cep'))
            ->put('country', 'BR');

        $dropoffAddress = json_encode(
            $dropoffAddress->toArray(), JSON_UNESCAPED_UNICODE
        );

        return $dropoffAddress;
    }
}