<?php

namespace App\Traits\Uber;

trait ConfigDeliveryTrait
{
    public function hasPickupReadyMinute()
    {
        return ! is_null(config('uber.delivery.pickup_ready.min', null));
    }

    public function pickupReadyMinute()
    {
        return (int) config('uber.delivery.pickup_ready.min', 0);
    }

    public function hasPickupReadyHour()
    {
        return ! is_null(config('uber.delivery.pickup_ready.hour', null));
    }

    public function pickupReadyHour()
    {
        return config('uber.delivery.pickup_ready.hour');
    }

    public function hasPickupDeadlineMinute()
    {
        return ! is_null(config('uber.delivery.pickup_deadline.min', null));
    }

    public function pickupDeadlineMinute()
    {
        return (int) config('uber.delivery.pickup_deadline.min', 0);
    }

    public function hasPickupDeadlineHour()
    {
        return ! is_null(config('uber.delivery.pickup_deadline.hour', null));
    }

    public function pickupDeadlineHour()
    {
        return config('uber.delivery.pickup_deadline.hour');
    }

    public function hasDropoffReadyMinute()
    {
        return ! is_null(config('uber.delivery.dropoff_ready.min', null));
    }

    public function dropoffReadyMinute()
    {
        return (int) config('uber.delivery.dropoff_ready.min', 0);
    }

    public function hasDropoffReadyHour()
    {
        return ! is_null(config('uber.delivery.dropoff_ready.hour', null));
    }

    public function dropoffReadyHour()
    {
        return config('uber.delivery.dropoff_ready.hour');
    }

    public function hasDropoffDeadlineMinute()
    {
        return ! is_null(config('uber.delivery.dropoff_deadline.min', null));
    }

    public function dropoffDeadlineMinute()
    {
        return (int) config('uber.delivery.dropoff_deadline.min', 0);
    }

    public function hasDropoffDeadlineHour()
    {
        return ! is_null(config('uber.delivery.dropoff_deadline.hour', null));
    }

    public function dropoffDeadlineHour()
    {
        return config('uber.delivery.dropoff_deadline.hour');
    }
}