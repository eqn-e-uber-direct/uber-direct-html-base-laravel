<?php

namespace App\Traits\Uber;

use App\Models\Product;

trait ManifestItemTrait
{
    public function buildManifestItem(Product $product)
    {
        $data = collect()
            ->put('name', (string) $product->name)
            ->put('quantity', 1)
            ->put('dimensions', [
                'length' => $product->length,
                'height' => $product->height,
                'depth' => $product->depth,
            ])
            ->put('price', 0)
            ->put('must_be_upright', false)
            ->put('weight', $product->weight)
            ->put('vat_percentage', 0);

        return $data->toArray();
    }
}