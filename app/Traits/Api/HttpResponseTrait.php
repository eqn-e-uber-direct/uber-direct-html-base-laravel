<?php

namespace App\Traits\Api;

use Illuminate\Support\Collection;

trait HttpResponseTrait
{
    public function responseSuccess(?Collection $data = null, ?string $message = null)
    {
        $data = $data ?? collect();

        return response()->json([
            'data' => $data->toArray(),
            'message' => $message ?? 'Operation completed successfully.',
            'success' => true,
        ], 200);
    }

    public function responseError(int $status = 500, ?Collection $data = null, ?string $message = null)
    {
        $data = $data ?? collect();

        return response()->json([
            'data' => $data->toArray(),
            'message' => $message ?? 'Internal server error.',
            'success' => false,
        ], $status);
    }

    public function responseUnauthorized(?Collection $data = null, ?string $message = null)
    {
        $data = $data ?? collect();

        return response()->json([
            'data' => $data->toArray(),
            'message' => $message ?? 'Invalid credentials provided.',
            'success' => false,
        ], 401);
    }
}