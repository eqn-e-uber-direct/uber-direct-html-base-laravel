<?php

namespace App\Traits\Delivery;

use App\Enums\DeliveryStatus;

trait DeliveryStatusTrait
{
    public function isPending()
    {
        return $this->status === DeliveryStatus::PENDING->value;
    }

    public function isNotPending()
    {
        return !$this->isPending();
    }

    public function isBatchReady()
    {
        return $this->status === DeliveryStatus::BATCH_READY->value;
    }

    public function isNotBatchReady()
    {
        return !$this->isBatchReady();
    }

    public function isPickup()
    {
        return $this->status === DeliveryStatus::PICKUP->value;
    }

    public function isNotPickup()
    {
        return !$this->isPickup();
    }

    public function isPickupComplete()
    {
        return $this->status === DeliveryStatus::PICKUPCOMPLETE->value;
    }

    public function isNotPickupComplete()
    {
        return !$this->isPickupComplete();
    }

    public function isDropoff()
    {
        return $this->status === DeliveryStatus::DROPOFF->value;
    }

    public function isNotDropoff()
    {
        return !$this->isDropoff();
    }

    public function isDelivered()
    {
        return $this->status === DeliveryStatus::DELIVERED->value;
    }

    public function isNotDelivered()
    {
        return !$this->isDelivered();
    }

    public function isCanceled()
    {
        return $this->status === DeliveryStatus::CANCELED->value;
    }

    public function isNotCanceled()
    {
        return !$this->isCanceled();
    }

    public function isReturned()
    {
        return $this->status === DeliveryStatus::RETURNED->value;
    }

    public function isNotReturned()
    {
        return !$this->isReturned();
    }
}