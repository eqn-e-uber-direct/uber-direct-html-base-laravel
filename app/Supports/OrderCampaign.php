<?php

namespace App\Supports;

use App\Models\Campaign;
use App\Enums\OrderStatus;
use Illuminate\Support\Collection;

class OrderCampaign
{
    public function __construct(protected Campaign $campaign){}

    protected function queryOrders(null|string|array $status = null, ?int $limit = null)
    {
        $query = $this->campaign->orders()

            // Vai aceitar apenas uma string ou array com strings.
            ->when($status, function ($query) use ($status) {
                if (is_string($status)) $query->where('status', $status);
                if (is_array($status)) $query->whereIn('status', $status);
            })

            // Quantidade de pedidos por página.
            ->when($limit, fn ($query) => $query->limit($limit))

            // Ordena pelo mais recente.
            ->orderBy('created_at', 'desc');

        return $query;
    }

    protected function addSelects($query, ?Collection $select = null)
    {
        $select = $select ?? collect();

        $query = $query->when($select->isNotEmpty(),
            function ($query) use ($select) {
                foreach ($select as $field) {
                    $query->addSelect($field);
                }

                return $query;
            }
        );

        return $query;
    }

    protected function addConditions($query, ?Collection $conditions = null)
    {
        $conditions = $conditions ?? collect();

        $query = $query->when($conditions->isNotEmpty(),
            function ($query) use ($conditions) {
                foreach ($conditions as $condition) {
                    if ($condition->has('type')) {
                        $query->{$condition->get('type')}(
                            $condition->get('name'), $condition->get('value')
                        );
                    }
                }

                return $query;
            }
        );

        return $query;
    }

    protected function addSorts($query, ?Collection $sort = null)
    {
        $sort = $sort ?? collect();

        $query = $query->when($sort->isNotEmpty(),
            function ($query) use ($sort) {
                foreach ($sort as $column) {
                    $query->orderBy(
                        $column->get('name'), $column->get('direction')
                    );
                }

                return $query;
            }
        );

        return $query;
    }

    public function orders(?int $limit = 10, ?Collection $params = null)
    {
        $params = $params ?? collect();

        $query = $this->queryOrders(null, $limit);

        $query = $this->addSelects($query, $params->get('selects'));

        $query = $this->addConditions($query, $params->get('conditions'));

        $query = $this->addSorts($query, $params->get('sorts'));

        return $query->get();
    }

    public function pending(?int $limit = 10, ?Collection $params = null)
    {
        $params = $params ?? collect();

        $query = $this->queryOrders(OrderStatus::PENDING->value, $limit);

        $query = $this->addSelects($query, $params->get('selects'));

        $query = $this->addConditions($query, $params->get('conditions'));

        $query = $this->addSorts($query, $params->get('sorts'));

        return $query->get();
    }

    public function delivered(?int $limit = 10, ?Collection $params = null)
    {
        $params = $params ?? collect();

        $query = $this->queryOrders(OrderStatus::DELIVERED->value, $limit);

        $query = $this->addSelects($query, $params->get('selects'));

        $query = $this->addConditions($query, $params->get('conditions'));

        $query = $this->addSorts($query, $params->get('sorts'));

        return $query->get();
    }

    public function canceled(?int $limit = 10, ?Collection $params = null)
    {
        $params = $params ?? collect();

        $query = $this->queryOrders(OrderStatus::CANCELED->value, $limit);

        $query = $this->addSelects($query, $params->get('selects'));

        $query = $this->addConditions($query, $params->get('conditions'));

        $query = $this->addSorts($query, $params->get('sorts'));

        return $query->get();
    }

    public function processing(?int $limit = 10, ?Collection $params = null)
    {
        $params = $params ?? collect();

        $query = $this->queryOrders([
            OrderStatus::WAITING->value, OrderStatus::PROCESSING->value
        ], $limit);

        $query = $this->addSelects($query, $params->get('selects'));

        $query = $this->addConditions($query, $params->get('conditions'));

        $query = $this->addSorts($query, $params->get('sorts'));

        return $query->get();
    }

    public function countOrders()
    {
        $count = $this->queryOrders(null, null)->count();

        return $count;
    }

    public function countPending()
    {
        $count = $this->queryOrders(OrderStatus::PENDING->value, null)->count();

        return $count;
    }

    public function countProcessing()
    {
        $count = $this->queryOrders([
            OrderStatus::WAITING->value,
            OrderStatus::PROCESSING->value,
        ], null)->count();

        return $count;
    }

    public function countProcessingNotWaiting()
    {
        $count = $this->queryOrders([
            OrderStatus::PROCESSING->value,
        ], null)->count();

        return $count;
    }

    public function countCanceled()
    {
        $count = $this->queryOrders(OrderStatus::CANCELED->value)->count();

        return $count;
    }

    public function countDelivered()
    {
        $count = $this->queryOrders(OrderStatus::DELIVERED->value)->count();

        return $count;
    }

    public function canComplete(Collection $orders)
    {
        foreach ($orders as $order) {
            $delivery = $order->lastDelivery();

            if (!$delivery) return false;

            if ($delivery->canComplete() === false) return false;
        }

        return true;
    }
}