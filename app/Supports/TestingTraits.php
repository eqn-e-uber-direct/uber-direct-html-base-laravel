<?php

namespace App\Supports;

use App\Traits\Uber\ConfigDeliveryTrait;

class TestingTraits
{
    use ConfigDeliveryTrait;
}