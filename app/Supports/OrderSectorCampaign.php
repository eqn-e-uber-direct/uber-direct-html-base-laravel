<?php

namespace App\Supports;

use App\Models\Sector;
use App\Models\Campaign;

class OrderSectorCampaign extends OrderCampaign
{
   public function __construct(Campaign $campaign, protected Sector $sector)
   {
      parent::__construct($campaign);
   }

   protected function queryOrders(null|string|array $status = null, ?int $limit = null)
   {
      $query = parent::queryOrders($status, $limit);

      $query->where('sector_id', $this->sector->id);

      return $query;
   }
}