<?php

namespace App\Console\Commands\Uber;

use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use App\Jobs\Uber\RefreshTokenJob;

class RefreshUberTokenCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uber:token.refresh
                            {date? : The date in YYYY-MM-DD format.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh token for Uber Direct connection';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $date = $this->argument('date');

        if ($date !== null) {
            $date = Carbon::parse($date);
        }

        RefreshTokenJob::dispatch($date);
    }
}
