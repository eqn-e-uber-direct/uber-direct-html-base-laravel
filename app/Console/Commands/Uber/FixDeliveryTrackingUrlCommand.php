<?php

namespace App\Console\Commands\Uber;

use App\Models\Delivery;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FixDeliveryTrackingUrlCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uber:fix.trackingurl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set all tracking urls where it was null on Delivery model';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        DB::transaction(function () {
            $deliveries = Delivery::query()->whereNull('tracking_url')->get();

            $deliveries->each(function ($delivery) {
                $data = collect($delivery->dataToJson());

                $trackingUrl = $data->get('tracking_url');

                if (is_null($trackingUrl) && $data->has('data')) {
                    $trackingUrl = collect($data->get('data', []))
                        ->get('tracking_url');
                }

                $delivery->update(['tracking_url' => $trackingUrl]);
            });
        });
    }
}
