<?php

namespace App\Console\Commands\Uber;

use App\Models\Delivery;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FixDeliveryUberUuidCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uber:fix.deliveryuuid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set all uber uuids where it was null';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        DB::transaction(function () {
            $deliveries = Delivery::query()->whereNull('uber_uuid')->get();

            $deliveries->each(function ($delivery) {
                $data = collect($delivery->dataToJson());

                $uuid = $data->get('uuid');

                if (is_null($uuid) && $data->has('data')) {
                    $uuid = collect($data->get('data', []))->get('uuid');
                }

                $delivery->update(['uber_uuid' => $uuid]);
            });
        });
    }
}
