<?php

namespace App\Console;

use App\Jobs\Uber\ForceUpdateAllDeliveriesJob;
use App\Jobs\Uber\RefreshTokenJob;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        // Verifica o token a cada 24 horas e atualiza
        // se estiver prestes a experirar.
        if (config('eqn.schedules.updates.refresh_token')) {
            $schedule->job(new RefreshTokenJob(now()))->daily();
        }

        // Força a atualização de todas as entregas que estiverem
        // ainda em andamento.
        if (config('eqn.schedules.updates.deliveries')) {
            $schedule->job(new ForceUpdateAllDeliveriesJob)
                ->dailyAt(config('eqn.schedules.updates.deliveries_hour'));
        }
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
