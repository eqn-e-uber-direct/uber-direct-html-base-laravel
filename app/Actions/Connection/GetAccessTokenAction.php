<?php

namespace App\Actions\Connection;

use App\Models\UberDirectConnection;
use Lorisleiva\Actions\Concerns\AsAction;

class GetAccessTokenAction
{
    use AsAction;

    public function handle()
    {
        $connection = UberDirectConnection::latest()->first();

        if ($connection) {
            return $connection->token;
        }

        return null;
    }
}
