<?php

namespace App\Actions\Delivery;

use App\Models\Order;
use App\Models\Sector;
use App\Models\Campaign;
use App\Models\Delivery;
use App\Enums\OrderStatus;
use Lorisleiva\Actions\Concerns\AsAction;

class ProcessUberDeliveryTimes
{
    use AsAction;

    protected string $tableOrder;

    protected string $tableDelivery;

    public function __construct()
    {
        $this->tableOrder = app(Order::class)->getTable();

        $this->tableDelivery = app(Delivery::class)->getTable();
    }

    public function handle(Campaign $campaign, ?Sector $sector = null)
    {
        $deliveries = Delivery::query()
            ->select(
                "{$this->tableDelivery}.id",
                "{$this->tableDelivery}.created_at"
            )
            ->join(
                $this->tableOrder,
                "{$this->tableDelivery}.order_id", '=', "{$this->tableOrder}.id"
            )
            ->where("{$this->tableOrder}.campaign_id", $campaign->id)
            ->when($sector !== null, fn ($query) =>
                $query->where("{$this->tableOrder}.sector_id", $sector->id)
            )
            ->whereNull("{$this->tableOrder}.deleted_at")
            ->whereIn("{$this->tableOrder}.status", [
                OrderStatus::WAITING->value,
                OrderStatus::PROCESSING->value,
            ])
            ->whereNull("{$this->tableDelivery}.deleted_at")
            ->orderBy("{$this->tableDelivery}.created_at")
            ->first();

        if (is_null($deliveries)) return null;

        $start = $deliveries->created_at;

        $pickup = (clone $start);

        if (config('uber.delivery.pickup_ready.min') !== null) {
            $pickup->addMinutes((int)config('uber.delivery.pickup_ready.min'));
        }

        if (config('uber.delivery.pickup_ready.hour') !== null) {
            $pickup->setTimeFromTimeString(
                config('uber.delivery.pickup_ready.hour') . ":00"
            );
        }

        $dropoff = (clone $start);

        if (config('uber.delivery.dropoff_deadline.min') !== null) {
            $dropoff->addMinutes((int)config('uber.delivery.dropoff_deadline.min'));
        }

        if (config('uber.delivery.dropoff_deadline.hour') !== null) {
            $dropoff->setTimeFromTimeString(
                config('uber.delivery.dropoff_deadline.hour') . ":00"
            );
        }

        return collect([
            'start' => $start,
            'pickup' => $pickup,
            'dropoff' => $dropoff,
        ]);
    }
}