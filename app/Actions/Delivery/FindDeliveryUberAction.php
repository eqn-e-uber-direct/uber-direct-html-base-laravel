<?php

namespace App\Actions\Delivery;

use App\Models\Delivery;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Actions\Uber\TrueDeliveryIdAction;

class FindDeliveryUberAction
{
    use AsAction;

    public function handle(string $uberId)
    {
        $uberDeliveryId = TrueDeliveryIdAction::run($uberId);

        if (is_null($uberDeliveryId)) return null;

        $delivery = Delivery::query()
            ->where('uber_id', 'like', "%_{$uberDeliveryId}")
            ->orderBy('created_at', 'desc')
            ->first();

        return $delivery;
    }
}
