<?php

namespace App\Actions\Sector;

use App\Models\Sector;
use Illuminate\Support\Facades\Log;
use Lorisleiva\Actions\Concerns\AsAction;

class FindSectorFromCep
{
    use AsAction;

    public function handle(string $cep): Sector|null
    {
        $prefixCep = substr($cep, 0, 5);

        $sector = Sector::query()
            ->where('start', '<=', $prefixCep)
            ->where('end', '>=', $prefixCep)
            ->first();

        if (!$sector) {
            Log::error('Nenhum setor encontrado para o CEP ' . $cep);
        }

        return $sector;
    }
}
