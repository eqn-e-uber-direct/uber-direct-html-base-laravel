<?php

namespace App\Actions\Campaign;

use App\Models\Campaign;
use Spatie\QueryBuilder\QueryBuilder;
use Lorisleiva\Actions\Concerns\AsAction;

class ListCampaignsAction
{
    use AsAction;

    public function handle()
    {
        $campaigns = QueryBuilder::for(Campaign::class)
            ->allowedFilters([
                'title',
                'slug',
            ])
            ->orderBy('start_at', 'desc')
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->paginate(request('paginate', 21));


        return $campaigns;
    }
}
