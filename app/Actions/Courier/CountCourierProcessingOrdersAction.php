<?php

namespace App\Actions\Courier;

use App\Models\Sector;
use App\Models\Campaign;
use Lorisleiva\Actions\Concerns\AsAction;

class CountCourierProcessingOrdersAction
{
    use AsAction;

    public function handle(?Campaign $campaign = null, ?Sector $sector = null)
    {
        $couriers = ListCouriersByProcessingOrdersAction::run($campaign, $sector);

        $ordersQty = $couriers->sum(fn ($courier) =>
            $courier->processingOrdersQty($sector)
        );

        return $ordersQty;
    }
}
