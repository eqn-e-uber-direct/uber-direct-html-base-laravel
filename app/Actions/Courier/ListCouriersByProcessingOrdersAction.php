<?php

namespace App\Actions\Courier;

use App\Models\Order;
use App\Models\Sector;
use App\Models\Courier;
use App\Models\Campaign;
use App\Models\Delivery;
use App\Enums\OrderStatus;
use App\Enums\DeliveryStatus;
use Illuminate\Support\Facades\DB;
use Lorisleiva\Actions\Concerns\AsAction;

class ListCouriersByProcessingOrdersAction
{
    use AsAction;

    public function handle(?Campaign $campaign = null, ?Sector $sector = null)
    {
        $deliveries = Delivery::query()
            ->select(DB::raw('MAX('.Delivery::TABLE.'.id) as id'), 'order_id')
            ->join(
                Order::TABLE,
                Order::TABLE.'.id', '=', Delivery::TABLE.'.order_id'
            )
            ->whereNotNull('courier_id')
            ->where(Order::TABLE.'.status', OrderStatus::PROCESSING->value)
            ->when($campaign, fn ($query) =>
                $query->where(Order::TABLE.'.campaign_id', $campaign->id ?? null)
            )
            ->when($sector, fn ($query) =>
                $query->where(Order::TABLE.'.sector_id', $sector->id ?? null)
            )
            ->groupBy('order_id')
            ->get();

        $couriers = Courier::query()
            ->select(Courier::TABLE.'.*', Delivery::TABLE.'.id as delivery_id')
            ->join(
                Delivery::TABLE,
                Delivery::TABLE.'.courier_id', '=', Courier::TABLE.'.id'
            )
            ->whereIn(
                Delivery::TABLE.'.id', $deliveries->pluck('id')->toArray()
            )
            ->whereNotIn(Delivery::TABLE.'.status', [
                DeliveryStatus::RETURNED->value,
                DeliveryStatus::DELIVERED->value,
                DeliveryStatus::BATCH_READY->value,
            ])
            ->get();

        $groupedCouriers = $couriers->groupBy(fn ($item) =>
            strtolower($item->name).'-'.
            strtolower($item->vehicle_type).'-'.
            strtolower($item->vehicle_make).'-'.
            strtolower($item->vehicle_model).'-'.
            strtolower($item->vehicle_color)
        )->values();

        $groupedCouriers->transform(function ($group) {
            $deliveries = $group->count('delivery_id');
            $courier = $group->first();
            return collect()
                ->put('courier', $courier)
                ->put('deliveries', $deliveries);
        });

        return $groupedCouriers;
    }
}
