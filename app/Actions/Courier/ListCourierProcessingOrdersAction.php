<?php

namespace App\Actions\Courier;

use App\Models\Order;
use App\Models\Sector;
use App\Models\Courier;
use App\Models\Delivery;
use App\Enums\OrderStatus;
use App\Enums\DeliveryStatus;
use Lorisleiva\Actions\Concerns\AsAction;

class ListCourierProcessingOrdersAction
{
    use AsAction;

    protected function buildQuery(Courier $courier, ?Sector $sector = null)
    {
        $query = $courier->deliveries()
            ->join(Order::TABLE, Order::TABLE.'.id', '=', Delivery::TABLE.'.order_id')
            ->whereNotIn(Delivery::TABLE.'.status', [
                DeliveryStatus::RETURNED->value,
                DeliveryStatus::DELIVERED->value,
            ])
            ->where('orders.status', OrderStatus::PROCESSING->value)
            ->whereNotNull(Delivery::TABLE.'.courier_id')
            ->when($sector !== null, fn ($query) =>
                $query->where(Order::TABLE.'.sector_id', $sector->id)
            )
            ->orderByDesc(Delivery::TABLE.'.created_at')
            ->limit(1);

        return $query;
    }

    public function handle(Courier $courier, ?Sector $sector = null)
    {
        return $this->buildQuery($courier, $sector)->get();
    }

    public static function count(Courier $courier, ?Sector $sector = null)
    {
        return (new self)->buildQuery($courier, $sector)->count();
    }
}
