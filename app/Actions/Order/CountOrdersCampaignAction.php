<?php

namespace App\Actions\Order;

use App\Models\Order;
use App\Models\Sector;
use App\Models\Campaign;
use Spatie\QueryBuilder\QueryBuilder;
use Lorisleiva\Actions\Concerns\AsAction;

class CountOrdersCampaignAction
{
    use AsAction;

    public function handle(Campaign $campaign, ?Sector $sector = null, array $statuses = [])
    {
        $statuses = collect($statuses);

        $statusValues = $statuses->map(fn ($status) => $status->value);

        $orders = QueryBuilder::for(Order::class)
            ->when($statusValues->isNotEmpty(), fn ($query) =>
                $query->whereIn('status', $statusValues->toArray())
            )
            ->where('campaign_id', $campaign->id)
            ->when($sector !== null, fn ($query) =>
                $query->where('sector_id', $sector->id)
            )
            ->count();

        return $orders;
    }
}
