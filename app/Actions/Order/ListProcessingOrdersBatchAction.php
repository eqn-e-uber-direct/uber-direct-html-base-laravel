<?php

namespace App\Actions\Order;

use App\Models\Order;
use App\Models\Sector;
use App\Models\Campaign;
use App\Enums\OrderStatus;
use Spatie\QueryBuilder\QueryBuilder;
use Lorisleiva\Actions\Concerns\AsAction;

class ListProcessingOrdersBatchAction
{
    use AsAction;

    public function handle(Campaign $campaign, ?Sector $sector = null)
    {
        $orders = QueryBuilder::for(Order::class)
            ->select('id')
            ->where('status', OrderStatus::PROCESSING->value)
            ->where('campaign_id', $campaign->id)
            ->when($sector !== null, fn ($query) =>
                $query->where('sector_id', $sector->id)
            )
            ->get();

        // Coleta o último delivery de cada order.
        $deliveries = $orders->map(function ($order) {
            return $order->lastDelivery();
        });

        // Remove todos os deliveries que não estam em batch_ready.
        $deliveries = $deliveries->filter(fn ($delivery) =>
            $delivery->isBatchReady() && is_null($delivery->courier_id)
        );

        // Cria um agrupamento por batch_id.
        $batches = $deliveries->groupBy('batch_id')
            ->filter(fn ($items, $key) => empty($key) === false);

        return $batches;
    }
}
