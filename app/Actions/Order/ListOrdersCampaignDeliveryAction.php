<?php

namespace App\Actions\Order;

use App\Models\Sector;
use App\Models\Campaign;
use App\Enums\OrderStatus;
use Illuminate\Support\Collection;
use Lorisleiva\Actions\Concerns\AsAction;

class ListOrdersCampaignDeliveryAction
{
    use AsAction;

    public function handle(Campaign $campaign, int $qty, ?Collection $sort = null, ?Sector $sector = null)
    {
        $orders = $campaign->orders()
            ->where('status', OrderStatus::PENDING->value)
            ->when($sector !== null, fn ($query) =>
                $query->where('sector_id', $sector->id)
            )
            ->when($sort !== null, function ($query) use ($sort) {
                foreach ($sort as $field) {
                    $query->orderBy($field->name, $field->direction);
                }
                return $query;
            })
            ->limit($qty)
            ->get();

        return $orders;
    }
}