<?php

namespace App\Actions\Order;

use App\Models\Sector;
use App\Models\Campaign;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Actions\Order\QueryOrdersCampaignAction;

class ListOrdersCampaignAction
{
    use AsAction;

    public function handle(Campaign $campaign, ?Sector $sector = null, array $statuses = [], bool $deliveryStatus = false, array $sorts = [])
    {
        $query = QueryOrdersCampaignAction::run(
            $campaign, $sector, $statuses, $deliveryStatus, $sorts
        );

        $orders = $query->paginate(request('paginate', 300));

        return $orders;
    }
}
