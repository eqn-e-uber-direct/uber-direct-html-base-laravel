<?php

namespace App\Actions\Order;

use App\Models\Order;
use App\Models\Sector;
use App\Models\Campaign;
use App\Models\Delivery;
use App\Enums\DeliveryStatus;
use Illuminate\Support\Facades\DB;
use Spatie\QueryBuilder\QueryBuilder;
use Lorisleiva\Actions\Concerns\AsAction;

class QueryOrdersCampaignAction
{
    use AsAction;

    public function handle(Campaign $campaign, ?Sector $sector = null, array $statuses = [], bool $deliveryStatus = false, array $sorts = [])
    {
        $statuses = collect($statuses);

        $statusValues = $statuses->map(fn ($status) => $status->value);

        $sorts = collect($sorts);

        $query = QueryBuilder::for(Order::class)
            ->when($deliveryStatus, function ($query) {
                $latestDeliveriesQuery = DB::table(Delivery::TABLE)
                    ->select('order_id', DB::raw('MAX(created_at) as latest_created_at'))
                    ->groupBy('order_id');

                $query
                    ->leftJoin(Delivery::TABLE,
                        Order::TABLE.'.id', '=', Delivery::TABLE.'.order_id'
                    )
                    ->leftJoinSub($latestDeliveriesQuery, 'latest_deliveries', function ($join) {
                        $join->on(Order::TABLE.'.id', '=', 'latest_deliveries.order_id');
                    })
                    ->select(Order::TABLE.'.*', DB::raw('CASE
                        WHEN '.Delivery::TABLE.'.problem = true
                            AND '.Delivery::TABLE.'.cancel_order = false
                            AND '.Delivery::TABLE.'.reschedule_order = false
                            THEN 0
                        WHEN '.Delivery::TABLE.'.cancel_order = true THEN 1
                        WHEN '.Delivery::TABLE.'.status = "'. DeliveryStatus::CANCELED->value.'" THEN 1
                        WHEN '.Delivery::TABLE.'.reschedule_order = true THEN 2
                        WHEN '.Delivery::TABLE.'.status = "'. DeliveryStatus::RETURNED->value.'" THEN 2
                        WHEN '.Delivery::TABLE.'.status = "'. DeliveryStatus::DELIVERED->value.'" THEN 3
                        WHEN '.Delivery::TABLE.'.status = "'. DeliveryStatus::DROPOFF->value.'" THEN 4
                        WHEN '.Delivery::TABLE.'.status = "'. DeliveryStatus::PICKUPCOMPLETE->value.'" THEN 4
                        WHEN '.Delivery::TABLE.'.status = "'. DeliveryStatus::PICKUP->value.'" THEN 5
                        WHEN '.Delivery::TABLE.'.status = "'. DeliveryStatus::PENDING->value.'" THEN 5
                        ELSE 999
                    END AS sort_status'))
                    ->orderBy('sort_status');
            })
            ->when($statusValues->isNotEmpty(), fn ($query) =>
                $query->whereIn(Order::TABLE.'.status', $statusValues->toArray())
            )
            ->where(Order::TABLE.'.campaign_id', $campaign->id)
            ->when($sector !== null, fn ($query) =>
                $query->where(Order::TABLE.'.sector_id', $sector->id)
            )
            ->when($sorts->isNotEmpty(), function ($query) use ($sorts) {
                foreach ($sorts as $sort) {
                    $query->orderBy(
                        Order::TABLE . '.' . $sort['name'],
                        $sort['direction']
                    );
                }
            })
            ->when($sorts->isEmpty(), function ($query) {
                $query->orderBy(Order::TABLE . '.created_at', 'desc')
                    ->orderBy(Order::TABLE . '.id', 'desc');
            });

        return $query;
    }
}