<?php

namespace App\Actions\Order;

use App\Helpers\OrderHelper;
use App\Models\Sector;
use App\Models\Campaign;
use Lorisleiva\Actions\Concerns\AsAction;

class CanCompleteOrdersCampaignAction
{
    use AsAction;

    public function handle(Campaign $campaign, ?Sector $sector = null, array $statuses = [], bool $deliveryStatus = false): bool
    {
        $query = QueryOrdersCampaignAction::run(
            $campaign, $sector, $statuses, $deliveryStatus
        );

        $orders = $query->get();

        return OrderHelper::canComplete($orders);
    }
}