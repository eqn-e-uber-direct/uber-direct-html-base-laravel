<?php

namespace App\Actions\Order;

use App\Enums\OrderStatus;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Actions\Uber\CreateDeliveryAction;
use App\Jobs\Uber\CreateDeliveryRequestJob;
use App\Actions\Uber\CompleteDeliveryAction;
use App\Actions\Test\Uber\CreateDeliveryAction as TestCreateDeliveryAction;
use Illuminate\Support\Facades\Log;

class DeliverOrdersToUberAction
{
    use AsAction;

    public function handle(Collection $orders, Carbon $startDate, bool $noMessage = false, bool $testing = false)
    {
        try {
            // Atualiza o status de todos os pedidos para WAITING.
            $orders->each(fn ($order) =>
                $order->update(['status' => OrderStatus::WAITING->value])
            );

            // Tenta fazer pelo menos o primeiro envio
            // para atualizar o token se for necessário.
            // Os próximos envios serão feitos por jobs.
            $firstOrder = $orders->first();

            // Quando tiver colocado como testing, usa o
            // robô para executar o delivery automaticamente.
            if ($testing) {
                $firstDelivery = TestCreateDeliveryAction::run(
                    $firstOrder, $startDate, $noMessage, $testing
                );
            }

            if (!$testing) {
                $firstDelivery = CreateDeliveryAction::run(
                    $firstOrder, $startDate, $noMessage
                );
            }

            // Completa o $firstDelivery independente de erro ou sucesso.
            CompleteDeliveryAction::run($firstOrder, $firstDelivery);

            // Agora tenta mandar e salvar as próximas entregas.
            // Usará jobs para não demorar a resposta.
            $orders->forget(0);

            $orders->each(fn ($order) =>
                CreateDeliveryRequestJob::dispatch(
                    $order->id, $startDate, $testing
                )
            );

            return true;
        }
        catch (\Exception $e) {
            Log::error($e->getMessage());

            return false;
        }
    }
}