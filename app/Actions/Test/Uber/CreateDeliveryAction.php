<?php

namespace App\Actions\Test\Uber;

use App\Models\Order;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Actions\Uber\CreateDeliveryAction as UberCreateDeliveryAction;
use Illuminate\Support\Carbon;

class CreateDeliveryAction extends UberCreateDeliveryAction
{
    use AsAction;

    public function handle(Order $order, ?Carbon $startDate = null, bool $noMessage = false, ?bool $testing = null)
    {
        $this->order = $order;

        $this->startDate = $startDate;

        $this->noMessage = $noMessage;

        if (is_null($startDate)) $startDate = Carbon::now();

        $request = request();

        $testing = $testing === null
            ? filter_var($request->input('testing'), FILTER_VALIDATE_BOOLEAN)
            : $testing;

        $roboCourier = collect()
            ->when($testing, function ($roboCourier) use ($request, $startDate) {
                $cannotAccess = filter_var(
                    $request->input('cannotAccess'), FILTER_VALIDATE_BOOLEAN
                );

                $cannotFind = filter_var(
                    $request->input('cannotFind'), FILTER_VALIDATE_BOOLEAN
                );

                $rejected = filter_var(
                    $request->input('rejected'), FILTER_VALIDATE_BOOLEAN
                );

                $unavailable = filter_var(
                    $request->input('unavailable'), FILTER_VALIDATE_BOOLEAN
                );

                $now = Carbon::now()->addMinutes(5);

                $roboCourier->put('mode', 'custom')

                    ->put(
                        'enroute_for_pickup_at',
                        (clone $startDate)->addMinutes(8)->toRfc3339String()
                    )

                    ->put(
                        'pickup_imminent_at',
                        (clone $startDate)->addMinutes(11)->toRfc3339String()
                    )

                    ->put(
                        'pickup_at',
                        (clone $startDate)->addMinutes(14)->toRfc3339String()
                    )

                    ->put(
                        'dropoff_imminent_at',
                        (clone $startDate)->addMinutes(17)->toRfc3339String()
                    )

                    ->put(
                        'dropoff_at',
                        (clone $startDate)->addMinutes(20)->toRfc3339String()
                    )

                    ->when($cannotAccess, fn ($roboCourier) =>
                        $roboCourier->put(
                            'cancel_reason', "cannot_access_customer_location"
                        )
                    )
                    ->when($cannotFind, fn ($roboCourier) =>
                        $roboCourier->put(
                            'cancel_reason', "cannot_find_customer_address"
                        )
                    )
                    ->when($rejected, fn ($roboCourier) =>
                        $roboCourier->put(
                            'cancel_reason', "customer_rejected_order"
                        )
                    )
                    ->when($unavailable, fn ($roboCourier) =>
                        $roboCourier->put(
                            'cancel_reason', "customer_unavailable"
                        )
                    );
            });

        $pickupReadyDate = (clone $startDate)->addMinutes(5);
        $pickupDeadlineDate = (clone $startDate)->addMinutes(35);
        $dropoffReadyDate = (clone $startDate)->addMinutes(5);
        $dropoffDeadlineDate = (clone $startDate)->addMinutes(65);

        $this->body = $this->buildBody($this->order, $this->startDate)
            ->put('test_specifications', [
                'robo_courier_specification' => $roboCourier->toArray(),
            ])
            ->put('pickup_ready_dt', $pickupReadyDate->toRfc3339String())
            ->put('pickup_deadline_dt', $pickupDeadlineDate->toRfc3339String())
            ->put('dropoff_ready_dt', $dropoffReadyDate->toRfc3339String())
            ->put('dropoff_deadline_dt', $dropoffDeadlineDate->toRfc3339String());

        return $this->response();
    }
}
