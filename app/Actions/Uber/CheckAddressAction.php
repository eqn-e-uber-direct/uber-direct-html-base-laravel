<?php

namespace App\Actions\Uber;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Actions\Uber\RenewTokenAndRetryAction;
use App\Actions\Connection\GetAccessTokenAction;

class CheckAddressAction
{
    use AsAction;

    public string $path;
    public Collection $headers;
    public int $tries;
    public Collection $user;
    public Collection $query;

    public function handle(string $address)
    {
        $this->query = collect(['address' => $address]);

        return $this->response();
    }

    protected function response()
    {
        if ($this->tries > 1) return false;

        $response = Http::withHeaders($this->headers->toArray())
            ->get($this->path, $this->query->toArray());

        if ($response->successful() === false) {
            $statusCode = $response->status();

            // Se não está autorizado, tenta gerar um novo token e se logar.
            // Depois tenta executar novamente esta ação.
            if ($statusCode === 401) {
                $shouldRetry = RenewTokenAndRetryAction::run($this);

                if ($shouldRetry) {
                    // Evita que entre num loop infinito.
                    $this->tries++;
                    // Reenvia a solicitação
                    return $this->response();
                }
            }

            return false;
        }

        return $response->json();
    }

    public function __construct()
    {
        $this->path = config('uber.path').
            "/customers/".
            config('uber.customer_id').
            "/serviceability";

        $token = GetAccessTokenAction::run();

        $this->headers = collect(['Authorization' => "Bearer {$token}"]);

        $this->tries = 0;
    }
}
