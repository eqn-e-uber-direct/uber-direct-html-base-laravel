<?php

namespace App\Actions\Uber;

use Illuminate\Support\Facades\Http;
use Lorisleiva\Actions\Concerns\AsAction;

class GetAccessToken
{
    use AsAction;

    protected $path;
    protected $body;

    public function handle()
    {
        $response = Http::asForm()
            ->post($this->path, $this->body->toArray());

        if ($response->successful()) {
            return $response->json();
        }

        return false;
    }

    public function __construct()
    {
        $this->path = config('uber.login_path');

        $this->body = collect([
            'grant_type' => 'client_credentials',
            'scope' => 'eats.deliveries',
            'client_id' => config('uber.client_id'),
            'client_secret' => config('uber.client_secret'),
        ]);
    }
}
