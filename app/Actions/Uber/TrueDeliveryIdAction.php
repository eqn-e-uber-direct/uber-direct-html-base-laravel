<?php

namespace App\Actions\Uber;

use Illuminate\Support\Str;
use Lorisleiva\Actions\Concerns\AsAction;

class TrueDeliveryIdAction
{
    use AsAction;

    public function handle(string $deliveryId)
    {
        // Prefixo ret_ usado para quando o Uber Direct estiver atualizando
        // o envio do pedido.
        if (Str::startsWith($deliveryId, 'del_')) {
            $deliveryId = Str::replaceFirst('del_', '', $deliveryId);
            return $deliveryId;
        }

        // Prefixo ret_ usado para quando o Uber Direct estiver atualizando
        // o retorno do pedido.
        if (Str::startsWith($deliveryId, 'ret_')) {
            $deliveryId = Str::replaceFirst('ret_', '', $deliveryId);
            return $deliveryId;
        }

        // Se não conseguir encontrar o código com os prefixos, retorna nulo.
        return null;
    }
}