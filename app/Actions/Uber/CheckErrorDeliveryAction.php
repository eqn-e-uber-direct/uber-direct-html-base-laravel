<?php

namespace App\Actions\Uber;

use App\Models\Order;
use Illuminate\Http\Client\Response;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Actions\Uber\CancelDeliveryAction;
use App\Actions\Uber\Errors\HandleDeliveryErrorAction;

class CheckErrorDeliveryAction
{
    use AsAction;

    /**
     * Verifica e trata os erros de ações de delivery pelo Uber Direct.
     *
     * @param Order $order O pedido.
     * @param Response $response A resposta da requisição.
     * @param mixed &$parentAction A ação pai.
     *   Exemplos: CancelDeliveryAction, CreateDeliveryAction.
     * @return bool
     *   Quando retornar TRUE, significa que deve refazer a requisição parent.
     *   Se for FALSE, tem que considerar como erro e não refazer a requisição.
     * */
    public function handle(
        Order $order, Response $response, mixed &$parentAction
    )
    {
        $statusCode = $response->status();

        if ($statusCode === 401) {
            return RenewTokenAndRetryAction::run($parentAction);
        }

        // Trato os erros do Delivery quando não for cancelada.
        // Se for cancelada, apenas retorna FALSE.
        if ($parentAction instanceof CancelDeliveryAction === false) {
            HandleDeliveryErrorAction::run($order, $response);
        }

        return false;
    }
}
