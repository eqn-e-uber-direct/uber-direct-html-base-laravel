<?php

namespace App\Actions\Uber;

use App\Models\Order;
use App\Enums\OrderStatus;
use App\Actions\Uber\StoreDeliveryAction;
use App\Jobs\Eqn\OrderProcessingJob;
use Lorisleiva\Actions\Concerns\AsAction;

class CompleteDeliveryAction
{
    use AsAction;

    public function handle(Order $order, mixed $deliveryUber)
    {
        // Salva os dados da delivery caso tenha sido criada pelo Uber Direct.
        if ($deliveryUber) {
            StoreDeliveryAction::run($order, collect($deliveryUber));
        }
        else {
            // Se não recebeu dados do Uber Direct e tem um delivery,
            // aconteceu um erro e ele está salvo em delivery. Coloca o
            // status do pedido como PROCESSING para que o admin decida
            // o que fazer.
            // Se recebeu dados do Uber Direct e não tem delivery, algum
            // outro tipo de erro inesperado aconteceu e deve retornar o
            // pedido como status PENDING.
            $orderStatus = $order->lastDelivery()
                ? OrderStatus::PROCESSING->value
                : OrderStatus::PENDING->value;

            $order->update(['status' => $orderStatus]);
        }

        // Se o pedido estiver em PROCESSING, envia os dados para o
        // EQN do drupal para atualizar o status do pedido.
        if ($order->isProcessing()) {
            OrderProcessingJob::dispatch((string)$order->id);
        }

        $order->refresh();

        return $order;
    }
}
