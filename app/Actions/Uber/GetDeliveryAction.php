<?php

namespace App\Actions\Uber;

use Illuminate\Support\Facades\Http;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Actions\Connection\GetAccessTokenAction;
use App\Models\Delivery;

class GetDeliveryAction
{
    use AsAction;

    protected $path;

    protected $headers;

    public function handle(Delivery $delivery)
    {
        // TODO: Seria bom ter um novo tipo de exceção pra tratar apenas ela.
        throw_if($delivery->uber_id === null, new \Exception(
            "The delivery's uber ID cannot be null."
        ));

        $path = "{$this->path}/{$delivery->uber_id}";

        $response = Http::withHeaders($this->headers->toArray())->get($path);

        throw_if($response->successful() === false, new \Exception(
            $response->json('message', 'An error occurred while trying to get the delivery.')
        ));

        return $response->json();
    }

    public function __construct()
    {
        $this->path = config('uber.path').
            "/customers/".
            config('uber.customer_id').
            "/deliveries";

        $token = GetAccessTokenAction::run();

        $this->headers = collect(['Authorization' => "Bearer {$token}"]);
    }
}