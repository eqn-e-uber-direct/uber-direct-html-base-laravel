<?php

namespace App\Actions\Uber;

use App\Actions\Uber\RefreshTokenAction;
use Lorisleiva\Actions\Concerns\AsAction;

class RenewTokenAndRetryAction
{
    use AsAction;

    /**
     * Renova o token e reenvia a requisição baseada na ação passada.
     *
     * @param mixed $parentAction
     *   A classe com Action que será usada para reenviar a requisição.
     */
    public function handle(mixed &$parentAction)
    {
        $newToken = RefreshTokenAction::run();

        // Quando recupera o token, sinaliza que deve reenviar
        // A requisição original já vai ter o seu cabeçalho atualizado.
        if ($newToken) {
            $parentAction->headers->put(
                'Authorization', "Bearer {$newToken}"
            );

            // Indica reenvio da solicitação.
            return true;
        }

        // Indica falha ao renovar o token.
        return false;
    }
}
