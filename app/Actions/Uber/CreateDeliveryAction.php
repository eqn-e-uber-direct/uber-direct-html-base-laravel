<?php

namespace App\Actions\Uber;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Traits\Uber\AddressTrait;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use App\Traits\Uber\ManifestItemTrait;
use App\Traits\Uber\ConfigDeliveryTrait;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Actions\Uber\CheckErrorDeliveryAction;
use App\Actions\Connection\GetAccessTokenAction;

// TODO: Mover para um construtor e fazer o handle() receber um body caso
// resolva editar o body antes do envio. Ótimo para rotinas de testes.
class CreateDeliveryAction
{
    use AsAction;
    use AddressTrait;
    use ManifestItemTrait;
    use ConfigDeliveryTrait;

    public string $path;
    public Collection $headers;
    public int $tries;
    public Order $order;
    public Collection $body;
    public Collection $user;
    public Collection $address;
    public Collection $barcode;
    public ?Carbon $startDate;
    public bool $noMessage;

    public function handle(Order $order, ?Carbon $startDate = null, bool $noMessage = false)
    {
        $this->order = $order;

        $this->startDate = $startDate;

        $this->noMessage = $noMessage;

        $this->body = $this->buildBody(
            $this->order, $this->startDate, $this->noMessage
        );

        $this->applyRequestInputs();

        return $this->response();
    }

    // TODO: Talvez seja melhor remover os parâmetros $order e $startDate
    // e usar o $this->order e $this->startDate.
    protected function buildBody(Order $order, ?Carbon $startDate = null, bool $noMessage = false)
    {
        if (is_null($startDate)) $startDate = Carbon::now();

        // Adiciona o pickup ready caso seja informado por minutos.
        if ($this->hasPickupReadyMinute()) {
            $pickupReadyDate = (clone $startDate)
                ->addMinutes($this->pickupReadyMinute());
        }

        // Adiciona o pickup ready caso seja informado pela hora específica.
        if ($this->hasPickupReadyHour()) {
            $pickupReadyDate = Carbon::createFromFormat(
                'H:i', $this->pickupReadyHour()
            );
            $pickupReadyDate->setDate(
                $startDate->year, $startDate->month, $startDate->day
            );
        }

        // Adiciona o pickup deadline caso seja informado por minutos.
        if ($this->hasPickupDeadlineMinute()) {
            $pickupDeadlineDate = (clone $startDate)
                ->addMinutes($this->pickupDeadlineMinute());
        }

        // Adiciona o pickup deadline caso seja informado pela hora específica.
        if ($this->hasPickupDeadlineHour()) {
            $pickupDeadlineDate = Carbon::createFromFormat(
                'H:i', $this->pickupDeadlineHour()
            );
            $pickupDeadlineDate->setDate(
                $startDate->year, $startDate->month, $startDate->day
            );
        }

        // Adiciona o dropoff ready caso seja informado por minutos.
        if ($this->hasDropoffReadyMinute()) {
            $dropoffReadyDate = (clone $startDate)
                ->addMinutes($this->dropoffReadyMinute());
        }

        // Adiciona o dropoff ready caso seja informado pela hora específica.
        if ($this->hasDropoffReadyHour()) {
            $dropoffReadyDate = Carbon::createFromFormat(
                'H:i', $this->dropoffReadyHour()
            );
            $dropoffReadyDate->setDate(
                $startDate->year, $startDate->month, $startDate->day
            );
        }

        // Adiciona o dropoff deadline caso seja informado por minutos.
        if ($this->hasDropoffDeadlineMinute()) {
            $dropoffDeadlineDate = (clone $startDate)
                ->addMinutes($this->dropoffDeadlineMinute());
        }

        // Adiciona o dropoff deadline caso seja informado pela hora específica.
        if ($this->hasDropoffDeadlineHour()) {
            $dropoffDeadlineDate = Carbon::createFromFormat(
                'H:i', $this->dropoffDeadlineHour()
            );
            $dropoffDeadlineDate->setDate(
                $startDate->year, $startDate->month, $startDate->day
            );
        }

        $body = collect()

            ->put('pickup_name', (string) $this->user->get('name'))

            ->put('pickup_address', (string) $this->buildAddress([
                'cep' => $this->address->get('zipcode'),
                'address' => $this->address->get('street'),
                'address_extra' => $this->address->get('extra'),
            ]))

            ->put('pickup_phone_number', (string) $this->user->get('phone'))

            ->put('pickup_notes', $this->address->get('note'))

            ->put('pickup_verification', ['barcodes' => [[
                'value' => $this->barcode->get('number'),
                'type' => $this->barcode->get('type'),
            ]]])

            ->put('external_store_id', (string) $this->user->get('store_code'))

            ->put('dropoff_name', (string) $order->consumer)

            ->put('dropoff_address', (string) $this->buildAddress(
                $order->only('cep', 'address', 'address_extra')
            ))

            ->put('dropoff_phone_number', $this->dropoffPhoneNumber(
                $order, $noMessage
            ))

            ->put('dropoff_notes', (string) $order->note)

            ->put('dropoff_seller_notes', (string) $order->consumer)

            ->put('dropoff_verification', ['signature_requirement' => [
                'enabled' => true,
                'collect_signer_name' => true,
                'collect_signer_relationship' => true,
            ]])

            // A data para o entregador pegar a encomenda sempre
            // vai ser a data de quando for encaminhado o pedido.
            ->put('pickup_ready_dt',
                (string) $pickupReadyDate->toRfc3339String())

            // A data limite que o entregador tem sempre
            // vai ser 1 hora depois da data encaminhada.
            ->put('pickup_deadline_dt',
                (string) $pickupDeadlineDate->toRfc3339String())

            // Início da janela de entrega, sempre vai ser
            // a mesma da data de limite da janela de despacho.
            ->put('dropoff_ready_dt',
                (string) $dropoffReadyDate->toRfc3339String())

            // Fim da janela de entrega, sempre vai ser
            // 30 minutos após a data inicial de entrega.
            ->put('dropoff_deadline_dt',
                (string) $dropoffDeadlineDate->toRfc3339String())

            // Manifest Items é sempre 1 item, mas igual tem que mandar um array.
            ->put('manifest_items', [
                $this->buildManifestItem($order->campaign->product)
            ])

            ->put('manifest_total_value', 0)

            ->put('manifest_reference', (string) $order->id)

            ->put('undeliverable_action', 'return')

            ->put('idempotency_key', (string) $order->makeDeliveryUid())

            ->put('return_verification', [
                'picture' => true,
                'signature_requirement' => [
                    'enabled' => true,
                    'collect_signer_name' => true,
                    'collect_signer_relationship' => true,
                ]
            ]);

        return $body;
    }

    protected function response()
    {
        if ($this->tries > 1) return false;

        $response = Http::withHeaders($this->headers->toArray())
            ->post($this->path, $this->body->toArray());

        Log::info("Create Delivery response for order {$this->order->id}", [
            'response' => $response->json(),
            'order' => $this->order->toArray(),
        ]);

        if ($response->successful() === false) {
            Log::error("Create Delivery error for order {$this->order->id}", [
                'response' => $response->json(),
                'order' => $this->order->toArray(),
            ]);

            // Verifica quais erros ocorreram, resolve eles e se
            // retornar TRUE, deve refazer a requisição.
            $shouldRetry = CheckErrorDeliveryAction::run(
                $this->order, $response, $this
            );

            if ($shouldRetry) {
                // Evita que entre num loop infinito.
                $this->tries++;
                // Reenvia a solicitação
                return $this->response();
            }

            return false;
        }

        Log::info("Create Delivery success for order {$this->order->id}", [
            'response' => $response->json(),
            'order' => $this->order->toArray(),
        ]);

        return $response->json();
    }

    protected function dropoffPhoneNumber(Order $order, bool $noMessage = false)
    {
        if ($noMessage) return "+5511999999999";

        return (string) $order->phone_number;
    }

    public function applyRequestInputs(?Request $request = null)
    {
        // Busca os dados enviados no request pelo formulário.
        if (!$request) $request = request();

        // Remove todas as datas de pickup.
        $noPickupDates = filter_var(
            $request->input('noPickupDates'), FILTER_VALIDATE_BOOLEAN
        );

        if ($noPickupDates) {
            $this->body->forget('pickup_ready_dt')
                ->forget('pickup_deadline_dt');
        }

        // Remove todas as datas de dropoff.
        $noDropoffDates = filter_var(
            $request->input('noDropoffDates'), FILTER_VALIDATE_BOOLEAN
        );

        if ($noDropoffDates) {
            $this->body->forget('dropoff_ready_dt')
                ->forget('dropoff_deadline_dt');
        }
    }

    public function __construct()
    {
        $this->path = config('uber.path').
            "/customers/".
            config('uber.customer_id').
            "/deliveries";

        $token = GetAccessTokenAction::run();

        $this->headers = collect(['Authorization' => "Bearer {$token}"]);

        $this->user = collect(config('uber.user', []));

        $this->address = collect(config('uber.address', []));

        $this->barcode = collect(config('uber.barcode', []));

        $this->tries = 0;
    }
}
