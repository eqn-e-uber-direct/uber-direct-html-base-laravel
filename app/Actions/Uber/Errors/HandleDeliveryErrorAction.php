<?php

namespace App\Actions\Uber\Errors;

use App\Enums\DeliveryStatus;
use App\Enums\Uber\DeliveryErrorRequestCodes;
use App\Enums\Uber\ErrorBadRequestCodes;
use App\Models\Order;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Log;
use Lorisleiva\Actions\Concerns\AsAction;

class HandleDeliveryErrorAction
{
    use AsAction;

    public function handle(Order $order, Response $response)
    {
        $content = collect($response->json());

        // Adiciona log de erro para saber exatamente a resposta.
        Log::error($content->get('message'), ['data' => $content->toArray()]);

        // Trata todos os erros descritos na API do Uber Direct e traduz.
        $reason = $this->setReasonByCode(
            $response->status(), $content->get('code')
        );

        $reason = !$reason
            ? $content->get('message')
            : $reason->reasonBrazil();

        $deliveryData = collect()
            ->put('order_id', $order->id)
            ->put('status', DeliveryStatus::CANCELED->value)
            ->put('reason', $reason)
            ->put('problem', true)
            ->put('cancel_order', false)
            ->put('reschedule_order', false);

        $delivery = $order->deliveries()->create($deliveryData->toArray());

        return $delivery;
    }

    protected function setReasonByCode(int $status = null, string $code = null)
    {
        if (empty($status) || empty($code)) {
            return null;
        }

        if ($status === 400) {
            return ErrorBadRequestCodes::from($code);
        }

        $commonStatus = [402, 403, 404, 409, 429, 500, 503];

        if (in_array($status, $commonStatus)) {
            return DeliveryErrorRequestCodes::tryFrom($code);
        }

        return null;
    }
}
