<?php

namespace App\Actions\Uber;

use App\Models\Delivery;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Actions\Uber\CheckErrorDeliveryAction;
use App\Actions\Connection\GetAccessTokenAction;

class CancelDeliveryAction
{
    use AsAction;

    public string $path;
    public Collection $headers;
    public int $tries;
    public Delivery $delivery;

    public function handle(Delivery $delivery)
    {
        $this->delivery = $delivery;

        $this->setPath();

        return $this->response();
    }

    protected function response()
    {
        if ($this->tries > 1) return false;

        $response = Http::withHeaders($this->headers->toArray())
            ->post($this->path, ['_method' => 'DELETE']);

        Log::info("Cancel Delivery response for delivery {$this->delivery->id}", [
            'response' => $response->json(),
            'delivery' => $this->delivery,
        ]);

        if ($response->successful() === false) {
            Log::error("Cancel Delivery error for delivery {$this->delivery->id}", [
                'response' => $response->json(),
                'delivery' => $this->delivery,
            ]);

            // Verifica quais erros ocorreram, resolve eles e se
            // retornar TRUE, deve refazer a requisição.
            $shouldRetry = CheckErrorDeliveryAction::run(
                $this->delivery->order, $response, $this
            );

            if ($shouldRetry) {
                // Evita que entre num loop infinito.
                $this->tries++;
                // Reenvia a solicitação
                return $this->response();
            }

            return false;
        }

        Log::info("Cancel Delivery success for delivery {$this->delivery->id}", [
            'response' => $response->json(),
            'delivery' => $this->delivery,
        ]);

        return $response->json();
    }

    protected function setPath()
    {
        $this->path .= "/{$this->delivery->uber_id}/cancel";
    }

    public function __construct()
    {
        $this->path = config('uber.path').
            "/customers/".
            config('uber.customer_id').
            "/deliveries";

        $token = GetAccessTokenAction::run();

        $this->headers = collect([
            'Authorization' => "Bearer {$token}",
            'Content-Type' => 'application/json',
        ]);

        $this->tries = 0;
    }
}
