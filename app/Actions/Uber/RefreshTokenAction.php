<?php

namespace App\Actions\Uber;

use App\Jobs\Uber\StoreAccessTokenJob;
use Lorisleiva\Actions\Concerns\AsAction;

class RefreshTokenAction
{
    use AsAction;

    public function handle()
    {
        $response = GetAccessToken::run();

        if ($response) {
            StoreAccessTokenJob::dispatch($response);
            return $response['access_token'];
        }

        return null;
    }
}
