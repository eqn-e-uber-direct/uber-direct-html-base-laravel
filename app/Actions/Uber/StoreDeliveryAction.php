<?php

namespace App\Actions\Uber;

use App\Enums\DeliveryStatus;
use App\Models\Order;
use App\Enums\OrderStatus;
use Illuminate\Support\Collection;
use Lorisleiva\Actions\Concerns\AsAction;

class StoreDeliveryAction
{
    use AsAction;

    public function handle(Order $order, Collection $data)
    {
        $uberUuid = $data->get('uuid');

        if (is_null($uberUuid) && $data->has('data')) {
            $uberUuid = collect($data->get('data', []))->get('uuid');
        }

        $trackingUrl = $data->get('tracking_url');

        if (is_null($trackingUrl) && $data->has('tracking_url')) {
            $trackingUrl = collect($data->get('data', []))->get('tracking_url');
        }

        $storage = collect()
            ->put('uid', $data->get('external_id'))
            ->put('uber_uuid', $uberUuid)
            ->put('uber_id', $data->get('id'))
            ->put('status', DeliveryStatus::PENDING->value)
            ->put('data', json_encode($data->toArray()))
            ->put('problem', false)
            ->put('cancel_order', false)
            ->put('reschedule_order', false)
            ->put('tracking_url', $trackingUrl);

        $delivery = $order->deliveries()->create($storage->toArray());

        // Talvez mover isso aqui para um observer.
        if ($delivery) {
            $order->update(['status' => OrderStatus::PROCESSING->value]);
        }

        return $delivery;
    }
}
