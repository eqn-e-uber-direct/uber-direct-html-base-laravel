<?php

namespace App\Actions\Uber;

use App\Models\Courier;
use App\Models\Delivery;
use App\Enums\DeliveryStatus;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use App\Enums\Uber\UndeliverableReason;
use Lorisleiva\Actions\Concerns\AsAction;

class UpdateDeliveryAction
{
    use AsAction;

    public function handle(Delivery $delivery, Collection $content)
    {
        // Coleta o objeto data do payload.
        $data = collect($content->get('data'));

        // Coleta os dados do courier para atualizar ou criar um novo.
        $courierData = collect($data->get('courier', []));

        // Atualiza/Cria o courier.
        if ($courierData->isNotEmpty()) {
            $courier = Courier::UpdateOrCreate([
                'phone_number' => $courierData->get('phone_number'),
            ], $courierData->only(
                'name', 'phone_number',
                'vehicle_type', 'vehicle_make',
                'vehicle_model', 'vehicle_color'
            )->toArray());
        }

        // Se houver alguma razão para não ser entregue.
        $undeliverableReason = $data->get('undeliverable_reason', null);

        $undeliverableAction = $data->get('undeliverable_action', null);

        // Monta todos os dados para atualizar o delivery.
        $updateData = collect()

            ->put('status', DeliveryStatus::from($content->get('status'))->value)

            // Se o Uber UUID ainda estiver vazio, tem que atualizar.
            ->when(
                is_null($delivery->uber_uuid),
                function ($collection) use ($content, $data) {
                    $uberUuid = $content->get('uuid');
                    if (is_null($uberUuid) && $data->has('uuid')) {
                        $uberUuid = $data->get('uuid');
                    }
                    $collection->put('uber_uuid', $uberUuid ?? null);
                }
            )

            // Se o Tracking Url ainda estiver vazio, tem que atualizar.
            ->when(
                is_null($delivery->tracking_url),
                function ($collection) use ($content, $data) {
                    $trackingUrl = $content->get('tracking_url');
                    if (is_null($trackingUrl) && $data->has('tracking_url')) {
                        $trackingUrl = $data->get('tracking_url');
                    }
                    $collection->put('tracking_url', $trackingUrl ?? null);
                }
            )

            // Vincula o courier se for possível.
            ->when($courier ?? null, fn ($data) =>
                $data->put('courier_id', $courier->id ?? null)
            )

            // Sempre atualiza este campo com o payload
            // mais atualizado pela Uber Direct.
            ->put('data', json_encode($content->toArray()))

            ->when(!empty($undeliverableReason),
                function ($data) use ($undeliverableReason, $undeliverableAction) {
                    $reason = null;

                    if (!empty($undeliverableAction)) {
                        $reason = UndeliverableReason::tryFrom(
                            strtolower($undeliverableReason.".".$undeliverableAction)
                        );

                        $data->when($reason, fn ($data) =>
                            $data->put('reason', $reason->reasonBrazil())
                        );
                    }

                    if (empty($undeliverableAction)) {
                        $reason = UndeliverableReason::tryFrom(
                            strtolower($undeliverableReason.".".$undeliverableReason)
                        );
                    }

                    // Quando a razão existe no nosso sistema, coloca
                    // a sua versão traduzida.
                    $data->when($reason, fn ($data) =>
                        $data->put('reason', $reason->reasonBrazil())
                    );

                    // Quando a razão não existe no nosso sistema, coloca
                    // a versão original.
                    $data->when(is_null($reason), fn ($data) =>
                        $data->put('reason', $undeliverableReason)
                    );
                }
            );

        Log::info("Delivery update data for Delivery ID {$delivery->uber_id}", [
            'newData' => $updateData->toArray(),
            'oldData' => $delivery->toArray(),
            'content' => $content->toArray(),
        ]);

        // Atualiza os dados do delivery.
        $delivery->update($updateData->toArray());

        $delivery->refresh();

        return $delivery;
    }
}