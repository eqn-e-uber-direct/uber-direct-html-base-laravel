<?php

namespace App\Actions\Eqn;

use App\Models\Order;

use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use App\Actions\Eqn\BaseConnection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use Lorisleiva\Actions\Concerns\AsAction;

class CancelOrderAction extends BaseConnection
{
    use AsAction;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(Order $order, Collection $data)
    {
        // Impede a conexão caso esteja desativada.
        if (!$this->status) return false;

        $response = Http::withBasicAuth($this->username, $this->password)
            ->post($this->path."/orders/".$order->id."/cancel", [
                'date' => Carbon::parse($data->get('date'))->timestamp,
                'reason' => $data->get('reason'),
            ]);

        if ($response->successful()) {
            $logLevel = 'info';
            $logMessage = "Cancel Order EQN success from order {$order->id}";
        }
        else {
            $logLevel = 'error';
            $logMessage = "Cancel Order EQN error from order {$order->id}";
        }

        Log::log($logLevel, $logMessage, [
            'response' => $response->body(),
            'order' => $order,
        ]);

        // Ainda não existe um processo para quando ocorre algum erro.
        return $response->successful();
    }
}
