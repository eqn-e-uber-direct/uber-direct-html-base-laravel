<?php

namespace App\Actions\Eqn;

use App\Models\Order;
use App\Actions\Eqn\BaseConnection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use Lorisleiva\Actions\Concerns\AsAction;

class DeliverOrderAction extends BaseConnection
{
    use AsAction;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(Order $order)
    {
        // Impede a conexão caso esteja desativada.
        if (!$this->status) return false;

        $response = Http::withBasicAuth($this->username, $this->password)
            ->post("{$this->path}/orders/{$order->id}/deliver");

        if ($response->successful()) {
            $logLevel = 'info';
            $logMessage = "Deliver Order EQN success from order {$order->id}";
        }
        else {
            $logLevel = 'error';
            $logMessage = "Deliver Order EQN error from order {$order->id}";
        }

        Log::log($logLevel, $logMessage, [
            'response' => $response->body(),
            'order' => $order,
        ]);

        // Ainda não existe um processo para quando ocorre algum erro.
        return $response->successful();
    }
}
