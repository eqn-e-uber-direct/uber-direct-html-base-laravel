<?php

namespace App\Actions\Eqn;

use App\Models\Order;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use Lorisleiva\Actions\Concerns\AsAction;

class RescheduleOrderAction extends BaseConnection
{
    use AsAction;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(Order $order)
    {
        // Impede a conexão caso esteja desativada.
        if (!$this->status) return false;

        $response = Http::withBasicAuth($this->username, $this->password)
            ->post("{$this->path}/orders/{$order->id}/reschedule");

        if ($response->successful()) {
            $logLevel = 'info';
            $logMessage = "Reschedule Order EQN success from order {$order->id}";
        }
        else {
            $logLevel = 'error';
            $logMessage = "Reschedule Order EQN error from order {$order->id}";
        }

        Log::log($logLevel, $logMessage, [
            'response' => $response->body(),
            'order' => $order,
        ]);

        // Ainda não existe um processo para quando ocorre algum erro.
        return $response->successful();
    }
}
