<?php

namespace App\Actions\Eqn;

use App\Actions\Eqn\BaseConnection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use Lorisleiva\Actions\Concerns\AsAction;

class OrderProcessingAction extends BaseConnection
{
    use AsAction;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(string $id)
    {
        // Impede a conexão caso esteja desativada.
        if (!$this->status) return false;

        $response = Http::withBasicAuth($this->username, $this->password)
            ->post($this->path."/orders/".$id."/incoming");

        if ($response->successful()) {
            $logLevel = 'info';
            $logMessage = "Update Order Processing EQN success from order {$id}";
        }
        else {
            $logLevel = 'error';
            $logMessage = "Update Order Processing EQN error from order {$id}";
        }

        Log::log($logLevel, $logMessage, [
            'response' => $response->body(),
        ]);

        // Ainda não existe um processo para quando ocorre algum erro.
        return $response->successful();
    }
}
