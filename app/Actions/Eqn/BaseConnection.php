<?php

namespace App\Actions\Eqn;

/**
 * Tecnicamente não é uma ação como as outras classes, mas está aqui para
 * facilitar o seu uso dentro das ações que se conectam com o EQN drupal.
 */
class BaseConnection
{
    protected $username;
    protected $password;
    protected $path;
    protected $status;

    public function __construct()
    {
        // Validations.
        throw_if(is_null(config('eqn.username')), \Exception::class, 'EQN username not found');
        throw_if(is_null(config('eqn.password')), \Exception::class, 'EQN password not found');
        throw_if(is_null(config('eqn.path')), \Exception::class, 'EQN path not found');

        // Settings.
        $this->username = config('eqn.username');
        $this->password = config('eqn.password');
        $this->path = config('eqn.path');
        $this->status = (bool)config('eqn.active', 0);
    }
}
