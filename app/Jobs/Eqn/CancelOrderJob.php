<?php

namespace App\Jobs\Eqn;

use App\Models\Order;
use App\Models\Delivery;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Carbon;
use App\Actions\Eqn\CancelOrderAction;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class CancelOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Order $order;
    protected Delivery $delivery;
    protected Carbon $date;

    /**
     * Create a new job instance.
     */
    public function __construct(string $orderId, string $deliveryId, string $datetime)
    {
        $this->order = Order::findOrFail($orderId);
        $this->delivery = Delivery::findOrFail($deliveryId);
        $this->date = Carbon::parse($datetime);
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $data = collect()
            ->put('date', $this->date)
            ->put('reason', $this->delivery->reason);

        // Envia o cancelamento para o EQN.
        CancelOrderAction::run($this->order, $data);
    }
}
