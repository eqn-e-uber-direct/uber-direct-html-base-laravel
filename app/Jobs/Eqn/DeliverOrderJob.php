<?php

namespace App\Jobs\Eqn;

use App\Actions\Eqn\DeliverOrderAction;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class DeliverOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Order $order;

    /**
     * Create a new job instance.
     */
    public function __construct(string $orderId)
    {
        $this->order = Order::findOrFail($orderId);
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        DeliverOrderAction::run($this->order);
    }
}
