<?php

namespace App\Jobs\Eqn;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Actions\Eqn\OrderProcessingAction;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class OrderProcessingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(protected string $orderId){}

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        // Atualiza o pedido em EQN para PROCESSING.
        OrderProcessingAction::run($this->orderId);
    }
}
