<?php

namespace App\Jobs\Uber;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Actions\Uber\CreateDeliveryAction;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Actions\Uber\CompleteDeliveryAction;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Actions\Test\Uber\CreateDeliveryAction as TestCreateDeliveryAction;

class CreateDeliveryRequestJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Order $order;

    /**
     * Create a new job instance.
     */
    public function __construct(
        string $orderId,
        protected ?Carbon $startDate = null,
        protected bool $testing = false
    )
    {
        $this->order = Order::findOrFail($orderId);
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $deliveryUber = $this->testing
            ? TestCreateDeliveryAction::run($this->order, $this->startDate, $this->testing)
            : CreateDeliveryAction::run($this->order, $this->startDate);

        CompleteDeliveryAction::run($this->order, $deliveryUber);
    }
}
