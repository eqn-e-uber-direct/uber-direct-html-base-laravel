<?php

namespace App\Jobs\Uber;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Carbon;
use App\Models\UberDirectConnection;
use Illuminate\Queue\SerializesModels;
use App\Actions\Uber\RefreshTokenAction;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class RefreshTokenJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected UberDirectConnection $uberDirectConnection;

    /**
     * Create a new job instance.
     */
    public function __construct(protected ?Carbon $date = null)
    {
        $this->uberDirectConnection = UberDirectConnection::latest()->first();
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        // Se não informar uma data, recarrega o token.
        if ($this->date === null) {
            RefreshTokenAction::run();

            return;
        }

        $expiresAt = $this->uberDirectConnection->expires_at;

        // Se for informado o $date e houver data de expiração salva no
        // banco de dados, tenta recarregar conforme a data de expiração.
        if ($expiresAt) {
            // Compara apenas os dias.
            $diffInDays = $this->date->startOfDay()
                ->diffInDays($expiresAt->startOfDay());

            if ($diffInDays <= 7) {
                RefreshTokenAction::run();
            }

            return;
        }

        // Se não tem $date nem data de expiração, apenas recarrega o token.
        RefreshTokenAction::run();
    }
}
