<?php

namespace App\Jobs\Uber;

use App\Models\Delivery;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Collection;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Actions\Uber\UpdateDeliveryAction;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class UpdateDeliveryJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Delivery $delivery;
    protected Collection $content;

    /**
     * Create a new job instance.
     */
    public function __construct(string $deliveryId, array $content)
    {
        $this->delivery = Delivery::findOrFail($deliveryId);

        $this->content = collect($content);
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $delivery = UpdateDeliveryAction::run($this->delivery, $this->content);

        if ($delivery->storeProblem()) {
            $delivery->update(['problem' => true]);
        }
    }
}
