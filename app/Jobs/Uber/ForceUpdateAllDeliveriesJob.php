<?php

namespace App\Jobs\Uber;

use App\Models\Delivery;
use App\Enums\DeliveryStatus;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Jobs\Uber\ForceUpdateDeliveryJob;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class ForceUpdateAllDeliveriesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(protected $limit = 100)
    {}

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $page = 1;

        do {
            $deliveries = Delivery::query()
                ->whereIn('status', [
                    DeliveryStatus::PENDING,
                    DeliveryStatus::BATCH_READY,
                    DeliveryStatus::PICKUP,
                    DeliveryStatus::PICKUPCOMPLETE,
                    DeliveryStatus::DROPOFF,
                    DeliveryStatus::RETURNED,
                ])
                ->paginate($this->limit, ['id'], 'page', $page);

            Log::info("Force Updating {$deliveries->count()} deliveries");

            $deliveries->each(fn ($delivery) =>
                ForceUpdateDeliveryJob::dispatch($delivery->id)
            );

            $page++;
        }
        while ($deliveries->count() > 0);
    }
}
