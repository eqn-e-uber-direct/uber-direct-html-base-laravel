<?php

namespace App\Jobs\Uber;

use App\Models\Delivery;
use App\Enums\OrderStatus;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Actions\Uber\CancelDeliveryAction;
use App\Enums\DeliveryStatus;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class CancelDeliveryJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Delivery $delivery;

    /**
     * Create a new job instance.
     */
    public function __construct(string $deliveryId)
    {
        $this->delivery = Delivery::findOrFail($deliveryId);
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        // Cancela o pedido no Uber Direct.
        CancelDeliveryAction::run($this->delivery);
    }
}
