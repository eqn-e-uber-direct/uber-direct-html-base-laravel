<?php

namespace App\Jobs\Uber;

use App\Models\Delivery;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use App\Actions\Uber\GetDeliveryAction;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class ForceUpdateDeliveryJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $delivery;

    /**
     * Create a new job instance.
     */
    public function __construct(int $deliveryId)
    {
        $this->delivery = Delivery::findOrFail($deliveryId);
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $contents = collect(GetDeliveryAction::run($this->delivery));

        Log::info("Get Delivery ({$this->delivery->id}) contents", [
            'contents' => $contents->toArray(),
        ]);

        throw_if($contents->isEmpty(), new \Exception(
            "The delivery contents are empty."
        ));

        UpdateDeliveryJob::dispatch($this->delivery->id, $contents->toArray());
    }
}
