<?php

namespace App\Jobs\Uber;

use App\Actions\Uber\StoreDeliveryAction;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Collection;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class StoreDeliveryResponseJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected Order $order;
    protected Collection $data;

    /**
     * Create a new job instance.
     */
    public function __construct(string $orderId, array $data)
    {
        $this->order = Order::findOrFail($orderId);
        $this->data = collect($data);
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        StoreDeliveryAction::run($this->order, $this->data);
    }
}
