<?php

namespace App\Jobs\Uber;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Models\UberDirectConnection;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use function PHPUnit\Framework\throwException;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class StoreAccessTokenJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Collection $response;
    protected UberDirectConnection|null $uberDirectConnection;
    // public $tries = 1;

    /**
     * Create a new job instance.
     */
    public function __construct(array $response)
    {
        $this->response = collect($response);

        $this->uberDirectConnection = UberDirectConnection::query()
            ->orderBy('created_at', 'desc')
            ->firstOr(fn () => null);
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        throw_unless(
            $this->response->has('access_token'),
            \Exception::class,
            'Access token not found'
        );

        DB::transaction(function () {
            // Cria uma nova conexão com o token obtido recentemente.
            UberDirectConnection::create([
                'expires_at' => Carbon::now()->addSeconds(
                    $this->response->get('expires_in')
                ),
                'token' => $this->response->get('access_token'),
            ]);

            // Deleta completamente a conexão existente.
            if ($this->uberDirectConnection) {
                $this->uberDirectConnection->forceDelete();
            }
        });
    }
}
