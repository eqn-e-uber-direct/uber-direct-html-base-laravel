<?php

namespace App\Jobs\Api;

use App\Models\Order;
use App\Enums\OrderStatus;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Actions\Sector\FindSectorFromCep;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class UpdateOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Collection $data;

    /**
     * Create a new job instance.
     */
    public function __construct(array $data)
    {
        $this->data = collect($data);
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        // Adiciona o status PENDENTE caso não esteja
        // informado e seja um novo pedido.
        $this->data->when(
            !$this->data->get('status') && !Order::exists($this->data->get('id')),
            fn () => $this->data->put('status', OrderStatus::PENDING)
        );

        // TODO: Remover o código abaixo depois de devidamente testado em produção.
        // // Remove a pontuação do CEP e adiciona seu setor.
        // $this->data->when($this->data->has('cep'), function ($data) {
        //     $cep = preg_replace('/[^0-9]/', '', $data->get('cep'));
        //     $sector = (new FindSectorFromCep)->handle($cep);
        //     $data->put('cep', $cep);
        //     $data->put('sector_id', $sector->id);
        // });

        // Remove o código do EQN do nome do consumidor.
        $this->data->when($this->data->has('consumer'), function ($data) {
            $consumer = current(explode('-', $data->get('consumer')));
            $data->put('consumer', $consumer);
        });

        Order::updateOrCreate(
            ['id' => $this->data->get('id')],
            $this->data->toArray()
        );

        Log::info("Update Order EQN successful for id {$this->data->get('id')}", [
            'data' => $this->data->toArray(),
        ]);
    }
}
