<?php

namespace App\Jobs\Api;

use App\Models\Order;
use App\Enums\OrderStatus;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class CancelOrdersJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $orders;

    /**
     * Create a new job instance.
     */
    public function __construct(array $ids)
    {
        $this->orders = Order::whereIn('id', $ids)->get();
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->orders->each(function ($order) {
            // Se tiver algum delivery, tem que cancelar ele.
            $delivery = $order->lastDelivery();

            if ($delivery) {
                $delivery->update(['cancel_order' => true]);
            }

            $order->update(['status' => OrderStatus::CANCELED->value]);

            Log::info("Cancel Order EQN successful for id {$order->id}", [
                'order' => $order->toArray(),
            ]);
        });
    }
}
