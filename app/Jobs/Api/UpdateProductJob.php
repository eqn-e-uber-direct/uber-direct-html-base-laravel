<?php

namespace App\Jobs\Api;

use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Support\Collection;

class UpdateProductJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Collection $data;

    /**
     * Create a new job instance.
     */
    public function __construct(array $data)
    {
        $this->data = collect($data);
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Product::updateOrCreate(
            ['id' => $this->data->get('id')],
            $this->data->toArray()
        );
    }
}
