<?php

namespace App\Jobs\Api;

use App\Models\Product;
use App\Models\Campaign;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class UpdateCampaignJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Collection $data;

    /**
     * Create a new job instance.
     */
    public function __construct(array $data)
    {
        $this->data = collect($data);
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        // Primeiro atualiza/cria o produto se for enviado.
        if ($this->data->has('product')) {
            $productData = collect($this->data->pull('product'));

            $product = Product::updateOrCreate(
                ['id' => $productData->get('id')],
                $productData->toArray()
            );
        }

        // Atualiza/cria a campanha.
        $campaignData = $this->data
            ->when($product ?? null, fn($data) =>
                $data->put('product_id', $product->id)
            );

        // Remove a barra '/' do slug.
            $campaignData->when($campaignData->has('slug'), fn($data) =>
            $data->put('slug', ltrim($campaignData->get('slug'), '/'))
        );

        Campaign::updateOrCreate(
            ['id' => $campaignData->get('id')],
            $campaignData->toArray()
        );
    }
}
