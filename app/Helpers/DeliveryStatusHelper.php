<?php

namespace App\Helpers;

use App\Enums\DeliveryStatus;
use App\Models\Delivery;

class DeliveryStatusHelper
{
    public static function name(?Delivery $delivery)
    {
        if (is_null($delivery))
            return DeliveryStatus::PENDING->toView();

        $status = DeliveryStatus::tryFrom($delivery->status);

        return $status->toView();
    }

    public static function notes(?Delivery $delivery)
    {
        if (is_null($delivery))
            return DeliveryStatus::PENDING->notes($delivery);

        $status = DeliveryStatus::tryFrom($delivery->status);

        return $status->notes($delivery);
    }

    public static function tagClass(?Delivery $delivery)
    {
        // Não muda a classe caso não encontre a entrega.
        if (is_null($delivery)) return '';

        // Quando o status é 'delivered', tem que mudar a classe para tal.
        if ($delivery->isDelivered()) return 'delivered';

        // Quando NÃO possuir problema e o usuário resolveu reagendar a
        // entrega, retorna a classe 'rescheduled'.
        if ((bool)$delivery->problem === false && $delivery->reschedule_order)
            return 'reschedule';

        // Não muda a classe caso a entrega não tenha problema
        // e não estiver com o status 'delivered'.
        if ((bool)$delivery->problem === false) return '';

        // Quando possui problema, se o usuário marcou como cancelado,
        // retorna a classe 'canceled'.
        if ($delivery->cancel_order) return 'canceled';

        // Quando possui problema, se o usuário marcou como reagendado,
        // retorna a classe 'rescheduled'.
        if ($delivery->reschedule_order) return 'reschedule';

        // Quando possui problema, mas não está marcado nem como cancelado,
        // nem como reagendado, então retorna a classe 'not-sent'.
        return 'not-sent';
    }
}