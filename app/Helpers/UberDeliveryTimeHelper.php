<?php

namespace App\Helpers;

use Illuminate\Support\Carbon;

class UberDeliveryTimeHelper
{
    public static function pickup()
    {
        $pickup = config('uber.delivery.pickup_ready.min') !== null
            ? Carbon::now()->addMinutes((int)config('uber.delivery.pickup_ready.min'))
            : Carbon::now()->setTimeFromTimeString(
                config('uber.delivery.pickup_ready.hour') . ":00"
            );

        return $pickup;
    }

    public static function dropoff()
    {
        $dropoff = config('uber.delivery.dropoff_deadline.min') !== null
            ? Carbon::now()->addMinutes((int)config('uber.delivery.dropoff_deadline.min'))
            : Carbon::now()->setTimeFromTimeString(
                config('uber.delivery.dropoff_deadline.hour') . ":00"
            );

        return $dropoff;
    }
}