<?php

namespace App\Helpers;

class TextHelper
{
    public static function pluralize(int $count, string $singular, string $plural)
    {
        return $count > 1 ? "{$plural}" : "{$singular}";
    }

    public static function formatNumber(int $number)
    {
        return number_format($number, 0, ',', '.');
    }
}