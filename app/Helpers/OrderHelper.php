<?php

namespace App\Helpers;

use Illuminate\Support\Collection;

class OrderHelper
{
    public static function canComplete(Collection $orders)
    {
        foreach ($orders as $order) {
            $delivery = $order->lastDelivery();

            if (!$delivery) return false;

            if ($delivery->canComplete() === false) return false;
        }

        return true;
    }
}