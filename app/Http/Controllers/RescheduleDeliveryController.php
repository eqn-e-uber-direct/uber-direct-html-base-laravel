<?php

namespace App\Http\Controllers;

use App\Models\Delivery;
use Illuminate\Http\Request;
use App\Jobs\Uber\CancelDeliveryJob;

class RescheduleDeliveryController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Delivery $delivery, Request $request)
    {
        $errorText = 'A entrega não pode ser reagendada';

        if ($delivery->order->isProcessing() === false) {
            return back()->withErrors(['reschedule_order' => $errorText]);
        }

        if ($delivery->problem === false) {
            return back()->withErrors(['reschedule_order' => $errorText]);
        }

        if ($request->has('reschedule_order') === false) {
            return back()->withErrors(['reschedule_order' => $errorText]);
        }

        $delivery->update([
            'reschedule_order' => (bool)$request->input('reschedule_order')
        ]);

        // Cancela o pedido no Uber Direct.
        CancelDeliveryJob::dispatch($delivery->id);

        $consumer = $delivery->order->consumer;

        $address = implode(" ", array_filter([
            $delivery->order->address,
            $delivery->order->address_extra
        ]));

        return back()
            ->with(
                'success',
                "A entrega para {$consumer} em {$address} foi reagendada"
            );
    }
}
