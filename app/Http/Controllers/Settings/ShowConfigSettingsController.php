<?php

namespace App\Http\Controllers\Settings;
use Illuminate\Support\Facades\Route;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ShowConfigSettingsController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        Route::current()->getName();

        return view('web.settings.configs');
    }
}
