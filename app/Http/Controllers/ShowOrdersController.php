<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Enums\OrderStatus;
use Illuminate\Support\Carbon;
use App\Supports\OrderCampaign;
use App\Actions\Delivery\ProcessUberDeliveryTimes;
use App\Actions\Order\CanCompleteOrdersCampaignAction;
use App\Actions\Order\ListProcessingOrdersBatchAction;
use App\Actions\Courier\ListCouriersByProcessingOrdersAction;

class ShowOrdersController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Campaign $campaign)
    {
        $now = Carbon::now();

        $orderResource = new OrderCampaign($campaign);

        $ordersCount = $orderResource->countOrders();

        $pendingCount = $orderResource->countPending();

        $deliveredCount = $orderResource->countDelivered();

        $canceledCount = $orderResource->countCanceled();

        $processingCount = $orderResource->countProcessing();

        $processingNotWaitingCount = $orderResource->countProcessingNotWaiting();

        $params = collect([
            'sorts' => collect([
                collect(['name' => 'cep', 'direction' => 'asc']),
            ]),
            'selects' => collect([
                'id',
                'consumer',
                'address',
                'note',
                'address_extra',
                'cep',
                'status',
                'created_at',
            ]),
        ]);

        $pendingOrders = $orderResource->pending(10, $params);

        $deliveredOrders = $orderResource->delivered(10, $params);

        $canceledOrders = $orderResource->canceled(10, $params);

        $processingOrders = $orderResource->processing(10, $params);

        $couriers = ListCouriersByProcessingOrdersAction::run($campaign);

        $couriersCount = $couriers->count();

        // Lista todos os pedidos com delivery em batch_ready.
        $batches = ListProcessingOrdersBatchAction::run($campaign);

        // Verifica se os pedidos em processamento já podem ser cancelados ou não.
        $canCompleteOrders = CanCompleteOrdersCampaignAction::run(
            $campaign, null, [OrderStatus::PROCESSING, OrderStatus::WAITING]
        );

        // Lista com as estimativas de horários de saída e entrega.
        $uberProcessingTimes = ProcessUberDeliveryTimes::run($campaign);

        return view('web.orders.list', compact(
            'batches',
            'campaign',
            'canceledCount',
            'canceledOrders',
            'canCompleteOrders',
            'couriers',
            'couriersCount',
            'deliveredCount',
            'deliveredOrders',
            'now',
            'ordersCount',
            'pendingCount',
            'pendingOrders',
            'processingCount',
            'processingNotWaitingCount',
            'processingOrders',
            'uberProcessingTimes',
        ));
    }
}
