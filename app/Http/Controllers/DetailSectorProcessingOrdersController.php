<?php

namespace App\Http\Controllers;

use App\Models\Sector;
use App\Models\Campaign;
use App\Enums\OrderStatus;
use Illuminate\Support\Carbon;
use App\Actions\Order\ListOrdersCampaignAction;
use App\Actions\Order\CountOrdersCampaignAction;
use App\Actions\Order\CanCompleteOrdersCampaignAction;
use App\Actions\Order\ListProcessingOrdersBatchAction;
use App\Actions\Courier\ListCouriersByProcessingOrdersAction;

class DetailSectorProcessingOrdersController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Campaign $campaign, Sector $sector)
    {
        $orders = ListOrdersCampaignAction::run(
            $campaign, $sector, [OrderStatus::PROCESSING, OrderStatus::WAITING], true
        );

        $ordersCount = CountOrdersCampaignAction::run(
            $campaign, $sector, [OrderStatus::PROCESSING, OrderStatus::WAITING]
        );

        $canCompleteOrders = CanCompleteOrdersCampaignAction::run(
            $campaign, $sector, [OrderStatus::PROCESSING, OrderStatus::WAITING]
        );

        $couriers = ListCouriersByProcessingOrdersAction::run($campaign, $sector);

        $couriersCount = $couriers->count();

        $currentUrl = route('campaigns.sectors.orders.show.processing', [
            'campaign' => $campaign->slug,
            'sector' => $sector->id,
        ]);

        $now = Carbon::now();

        $status = OrderStatus::PROCESSING;

        // Lista todos os pedidos com delivery em batch_ready.
        $batches = ListProcessingOrdersBatchAction::run($campaign, $sector);

        return view('web.sectors.details.processing', compact(
            'batches',
            'campaign',
            'canCompleteOrders',
            'couriers',
            'couriersCount',
            'currentUrl',
            'now',
            'orders',
            'ordersCount',
            'sector',
            'status',
        ));
    }
}
