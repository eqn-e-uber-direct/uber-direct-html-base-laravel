<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Enums\OrderStatus;
use App\Actions\Order\ListOrdersCampaignAction;
use App\Actions\Order\CountOrdersCampaignAction;

class DetailDeliveredOrdersController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Campaign $campaign)
    {
        $queryStatus = [OrderStatus::DELIVERED];

        $querySorts = [['name' => 'cep', 'direction' => 'asc']];

        $orders = ListOrdersCampaignAction::run(
            $campaign, null, $queryStatus, false, $querySorts
        );

        $ordersCount = CountOrdersCampaignAction::run(
            $campaign, null, $queryStatus
        );

        $currentUrl = route('campaigns.orders.show.delivered', [
            'campaign' => $campaign->slug,
        ]);

        $status = OrderStatus::DELIVERED;

        return view('web.orders.details.standard', [
            'campaign' => $campaign,
            'currentUrl' => $currentUrl,
            'orders' => $orders,
            'ordersCount' => $ordersCount,
            'status' => $status,
        ]);
    }
}
