<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class DetailCampaignsController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Campaign $campaign)
    {
        $now = Carbon::now();

        return view('web.campaigns.sectors', [
            'campaign' => $campaign,
            'now' => $now,
        ]);
    }
}
