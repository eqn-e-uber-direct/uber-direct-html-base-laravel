<?php

namespace App\Http\Controllers;

use App\Models\Delivery;
use Illuminate\Http\Request;
use App\Jobs\Uber\CancelDeliveryJob;

class CancelDeliveryController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Delivery $delivery, Request $request)
    {
        $errorText = 'A entrega não pode ser cancelada';

        if ($delivery->order->isProcessing() === false) {
            return back()->withErrors(['cancel_order' => $errorText]);
        }

        if ($delivery->problem === false) {
            return back()->withErrors(['cancel_order' => $errorText]);
        }

        if ($request->has('cancel_order') === false) {
            return back()->withErrors(['cancel_order' => $errorText]);
        }

        $delivery->update([
            'cancel_order' => (bool)$request->input('cancel_order')
        ]);

        // Cancela o pedido no Uber Direct.
        CancelDeliveryJob::dispatch($delivery->id);

        $consumer = $delivery->order->consumer;

        $address = implode(" ", array_filter([
            $delivery->order->address,
            $delivery->order->address_extra
        ]));

        return back()
            ->with(
                'success',
                "A entrega para {$consumer} em {$address} foi cancelada"
            );
    }
}
