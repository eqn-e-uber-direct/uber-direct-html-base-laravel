<?php

namespace App\Http\Controllers;

use App\Models\Sector;
use App\Models\Campaign;
use App\Enums\OrderStatus;
use App\Actions\Order\ListOrdersCampaignAction;
use App\Actions\Order\CountOrdersCampaignAction;

class DetailSectorDeliveredOrdersController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Campaign $campaign, Sector $sector)
    {
        $orders = ListOrdersCampaignAction::run(
            $campaign, $sector, [OrderStatus::DELIVERED]
        );

        $ordersCount = CountOrdersCampaignAction::run(
            $campaign, $sector, [OrderStatus::DELIVERED]
        );

        $currentUrl = route('campaigns.sectors.orders.show.delivered', [
            'campaign' => $campaign->slug,
            'sector' => $sector->id,
        ]);

        $status = OrderStatus::DELIVERED;

        return view('web.sectors.details.standard', [
            'campaign' => $campaign,
            'currentUrl' => $currentUrl,
            'orders' => $orders,
            'ordersCount' => $ordersCount,
            'sector' => $sector,
            'status' => $status,
        ]);
    }
}
