<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Actions\Campaign\ListCampaignsAction;

class ShowCampaignsController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $campaigns = ListCampaignsAction::run();

        $now = Carbon::now();

        return view('web.campaigns.list', [
            'campaigns' => $campaigns,
            'now' => $now,
        ]);
    }
}
