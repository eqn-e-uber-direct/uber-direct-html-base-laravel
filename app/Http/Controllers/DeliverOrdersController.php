<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Actions\Order\DeliverOrdersToUberAction;
use App\Actions\Order\ListOrdersCampaignDeliveryAction;

class DeliverOrdersController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Campaign $campaign, Request $request)
    {
        if (!$request->get('qty')) {
            return back()
                ->withErrors([
                    'qty' => 'A quantidade de pedidos deve ser informada.'
                ])
                ->withInput();
        }

        $startDate = Carbon::now();

        $sort = collect([
            (object) ['name' => 'cep', 'direction' => 'asc'],
        ]);

        $orders = ListOrdersCampaignDeliveryAction::run(
            $campaign, (int)$request->get('qty'), $sort
        );

        if ($orders->isEmpty()) {
            // Apresentar um erro.
            return back()
                ->withErrors([
                    'qty' => 'Nenhum pedido encontrado para este setor.'
                ])
                ->withInput();
        }

        $testing = filter_var($request->input('testing'), FILTER_VALIDATE_BOOLEAN);

        $noMessage = filter_var($request->input('noMessage'), FILTER_VALIDATE_BOOLEAN);

        DeliverOrdersToUberAction::run($orders, $startDate, $noMessage, $testing);

        return back()
            ->with(
                'success',
                'Os pedidos estarão sendo enviados automaticamente.'
            );
    }
}
