<?php

namespace App\Http\Controllers\Api\Dev;

use App\Models\Delivery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Jobs\Uber\ForceUpdateDeliveryJob;
use App\Http\Controllers\Api\ApiController;

class ForceUpdateDeliveriesController extends ApiController
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $status = $request->input('status');

        $deliveryIds = Delivery::where('status', $status)->pluck('id');

        $dispatched = 0;

        Log::info("Force Updating {$deliveryIds->count()} deliveries with status {$status}");

        foreach ($deliveryIds as $id) {
            ForceUpdateDeliveryJob::dispatch((int)$id);
            $dispatched++;
        }

        return $this->responseSuccess(collect([
            'deliveryStatus' => $status,
            'total' => $deliveryIds->count(),
            'dispatched' => $dispatched,
        ]));
    }
}
