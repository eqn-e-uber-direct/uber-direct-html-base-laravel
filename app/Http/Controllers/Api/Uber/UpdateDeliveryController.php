<?php

namespace App\Http\Controllers\Api\Uber;

use Illuminate\Http\Request;
use App\Enums\DeliveryStatus;
use Illuminate\Support\Facades\Log;
use App\Jobs\Uber\UpdateDeliveryJob;
use App\Http\Controllers\Api\ApiController;
use App\Actions\Delivery\FindDeliveryUberAction;

class UpdateDeliveryController extends ApiController
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $uberSignature = $request->header('x-uber-signature');

        $payload = $request->getContent();

        $webhook = collect(config('uber.webhook', []));

        $hashedKey = hash_hmac('sha256', $payload, $webhook->get('status'));

        // Primeiro verifica se a chave está correta.
        if ($hashedKey !== $uberSignature) {
            Log::error("Delivery update failed, invalid signature", [
                'hashed_key' => $hashedKey,
                'uber_signature' => $uberSignature
            ]);
            return $this->responseUnauthorized();
        }

        // Coloca o conteúdo numa collection.
        $content = $request->collect();

        Log::info("Delivery update signature verified for Delivery ID
        {$content->get('delivery_id')}", [
            'status' => $content->get('status'),
            'data' => $content->toArray(),
        ]);

        // Agora verifica se a delivery existe.
        $delivery = FindDeliveryUberAction::run($content->get('delivery_id'));

        // Log comentado para ser adicionado de novo quando precisar.
        // Log::info("Update Delivery ({$content->get('delivery_id')})", [
        //     'status' => $content->get('status'),
        //     'data' => $content->toArray(),
        // ]);

        // Atualiza a delivery caso tenha sido encontrada.
        if ($delivery) {
            $newStatus = DeliveryStatus::from($content->get('status'));

            // Log comentado para ser adicionado de novo quando precisar.
            // Log::info("Update Delivery ({$content->get('delivery_id')}) check status", [
            //     'newStatus' => $newStatus,
            //     'dbStatus' => $delivery->status,
            // ]);

            if ($delivery->canUpdateStatus($newStatus)) {
                UpdateDeliveryJob::dispatch(
                    (string)$delivery->id, $content->toArray()
                );

                Log::info("Delivery update dispatched for Delivery ID
                {$content->get('delivery_id')}", [
                    'delivery' => $delivery->toArray(),
                    'data' => $content->toArray()
                ]);
            }
            else {
                Log::info("Delivery update can't change status for Delivery ID
                {$content->get('delivery_id')}", [
                    'status', $newStatus->value,
                    'delivery' => $delivery->toArray(),
                    'data' => $content->toArray()
                ]);
            }

            // Sempre retorna sucesso pois a delivery
            // vai ser atualizada através do job.
            return $this->responseSuccess();
        }

        Log::error("Delivery update failed for Delivery ID
        {$content->get('delivery_id')}", [
            'data' => $content->toArray()
        ]);

        // Retorna um erro por padrão se a delivery não existir.
        return $this->responseError(500, null, "Unexpected error.");
    }
}
