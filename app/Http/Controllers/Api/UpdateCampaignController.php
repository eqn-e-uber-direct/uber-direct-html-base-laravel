<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use App\Models\Campaign;
use App\Jobs\Api\UpdateCampaignJob;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\UpdateCampaignRequest;

class UpdateCampaignController extends ApiController
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(UpdateCampaignRequest $request)
    {
        $validated = collect($request->validated());

        $campaignData = $validated->only(
            'id', 'title', 'slug', 'color',
            'start_at', 'end_at', 'created_at', 'updated_at'
        );

        $productData = $validated->only(
            'product.id', 'product.name', 'product.sku', 'product.weight',
            'product.length', 'product.height', 'product.depth',
            'product.created_at', 'product.updated_at',
        );

        try {
            UpdateCampaignJob::dispatch($validated->toArray());
            $message = "Update or create campaign queued.";
            $success = true;
            $code = 200;

            Log::info("Campaign and product updated from EQN", [
                'data' => $validated->toArray(),
            ]);
        }
        catch (\Exception $e) {
            $message = $e->getMessage();
            $success = false;
            $errors = collect($e->getTraceAsString());
            $code = 500;

            Log::error("Campaign and product could not be updated from EQN", [
                'data' => $validated->toArray(),
            ]);
        }

        return response()->json([
            'data' => [
                Campaign::TABLE => $campaignData,
                Product::TABLE => $productData,
            ],
            'success' => $success,
            'message' => $message,
            'errors' => $errors ?? null,
        ], $code);
    }
}
