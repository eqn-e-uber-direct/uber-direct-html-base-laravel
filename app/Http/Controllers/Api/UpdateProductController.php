<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\UpdateProductRequest;
use App\Jobs\Api\UpdateProductJob;
use App\Models\Product;

class UpdateProductController extends ApiController
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(UpdateProductRequest $request)
    {
        $validated = collect($request->validated());

        $productData = $validated->only(
            'id', 'name', 'sku', 'weight', 'length',
            'height', 'depth', 'created_at', 'updated_at',
        );

        try {
            UpdateProductJob::dispatch($request->validated());
            $message = "Update or create product queued.";
            $success = true;
            $code = 200;
        }
        catch (\Exception $e) {
            $message = $e->getMessage();
            $success = false;
            $errors = collect($e->getTraceAsString());
            $code = 500;
        }

        return response()->json([
            'data' => [Product::TABLE => $productData],
            'success' => $success,
            'message' => $message,
            'errors' => $errors ?? null,
        ], $code);
    }
}
