<?php

namespace App\Http\Controllers\Api;

use App\Actions\Uber\CheckAddressAction;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\CheckAddressRequest;

class CheckAddressController extends ApiController
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(CheckAddressRequest $request)
    {
        $address = collect($request->validated())->get('address');

        $response = CheckAddressAction::run($address);

        if ($response === false) {
            return $this->responseError(500, null, 'Um erro inesperado aconteceu');

        }

        if (is_null($response)) {
            return $this->responseError(404, null, 'Nenhum valor retornado');
        }

        $response = collect($response);

        $data = collect([
            'serviceable' => $response->get('is_serviceable'),
            'address' => $response->get('address'),
            'detailed_address' => $response->get('detailed_address'),
        ]);

        return $this->responseSuccess($data, 'Endereço verificado com sucesso');
    }
}
