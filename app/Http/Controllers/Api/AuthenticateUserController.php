<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\ApiController;

/**
 * TODO: Criar um response melhor para este endpoint e que seja reutilizável.
 */
class AuthenticateUserController extends ApiController
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $user = Auth::user();

            $authorizationData = collect()
                ->put('token', $user->createToken('apiToken')->plainTextToken)
                ->put('type', 'bearer');

            $userData = collect()
                ->put('id', $user->id)
                ->put('email', $user->email)
                ->put('name', $user->name);

            return response()->json([
                'data' => [
                    'authorization' => $authorizationData, 'user' => $userData
                ],
                'success' => true,
                'message' => 'Login successful',
                'errors' => null,
            ]);
        }

        return response()->json([
            'data' => null,
            'success' => false,
            'message' => 'Invalid credentials',
            'errors' => null,
        ], 401);
    }
}
