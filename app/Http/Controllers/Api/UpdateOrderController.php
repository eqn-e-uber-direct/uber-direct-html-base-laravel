<?php

namespace App\Http\Controllers\Api;

use App\Models\Order;
use App\Jobs\Api\UpdateOrderJob;
use Illuminate\Support\Facades\Log;
use App\Actions\Sector\FindSectorFromCep;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\UpdateOrderRequest;

class UpdateOrderController extends ApiController
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(UpdateOrderRequest $request)
    {
        $validated = collect($request->validated());

        $orderData = $validated->only(
            'id', 'status', 'cep', 'phone_number',
            'address', 'address_extra', 'note', 'consumer',
            'campaign_id', 'created_at', 'updated_at',
        );

        // Verifica se o pedido já existe e impede de atualizar caso exista.
        $orderId = $validated->get('id');

        $order = Order::find($orderId);

        if ($order) {
            Log::alert("Update Order EQN already exists for id {$order->id}", [
                'order' => $order->toArray()
            ]);

            return response()->json([
                'data' => [Order::TABLE => $orderData],
                'success' => false,
                'message' => "Order ({$orderId}) already exists.",
                'errors' => null,
            ], 400);
        }

        // Verifica se o CEP está dentro dos setores permitidos.
        // Se estiver, atualiza o CEP e adiciona o setor nos dados.
        if ($validated->has('cep')) {
            $cep = preg_replace('/[^0-9]/', '', $validated->get('cep'));

            $sector = (new FindSectorFromCep)->handle($cep);

            // Impede que o pedido prossiga se o CEP informado não estiver
            // dentro da lista de setores permitidos.
            if (is_null($sector)) {
                Log::alert("Update Order EQN has a CEP outside
                of allowed sectors for id {$orderId}");

                return response()->json([
                    'data' => [Order::TABLE => $orderData],
                    'success' => false,
                    'message' => "Order ({$orderId}) has a CEP outside of allowed sectors.",
                    'errors' => null,
                ], 400);
            }

            $validated->put('cep', $cep);

            $validated->put('sector_id', $sector->id);
        }

        try {
            UpdateOrderJob::dispatch($validated->toArray());
            $message = "Update or create order queued.";
            $success = true;
            $code = 200;
        }
        catch (\Exception $e) {
            $message = $e->getMessage();
            $success = false;
            $errors = collect($e->getTraceAsString());
            $code = 500;

            Log::error("Update Order EQN has failed for id {$orderId}", [
                'message' => $message,
                'errors' => $errors,
            ]);
        }

        return response()->json([
            'data' => [Order::TABLE => $orderData],
            'success' => $success,
            'message' => $message,
            'errors' => $errors ?? null,
        ], $code);
    }
}
