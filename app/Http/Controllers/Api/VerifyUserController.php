<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * TODO: Criar um response melhor para este endpoint e que seja reutilizável.
 */
class VerifyUserController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $user = $request->user();

        return response()->json([
            'data' => [
                'user' => [
                    'id' => $user->id,
                    'email' => $user->email,
                    'name' => $user->name,
                ],
            ],
            'success' => true,
            'message' => 'Authenticated',
            'errors' => null,
        ]);
    }
}
