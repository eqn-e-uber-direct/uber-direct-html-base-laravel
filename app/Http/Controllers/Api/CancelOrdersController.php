<?php

namespace App\Http\Controllers\Api;

use App\Models\Order;
use App\Jobs\Api\CancelOrdersJob;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CancelOrdersRequest;

class CancelOrdersController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(CancelOrdersRequest $request)
    {
        $validated = collect($request->validated());

        try {
            CancelOrdersJob::dispatch($validated->get('ids'));
            $message = "cancel orders queued.";
            $success = true;
            $code = 200;
        }
        catch (\Exception $e) {
            $message = $e->getMessage();
            $success = false;
            $errors = collect($e->getTraceAsString());
            $code = 500;

            Log::error("Cancel Orders EQN failed", [
                'message' => $message,
                'errors' => $errors,
                'ids' => $validated->get('ids'),
            ]);
        }

        $ids = collect($validated->get('ids'))->map(fn ($id) => ['id' => $id]);

        return response()->json([
            'data' => [Order::TABLE => $ids->toArray()],
            'success' => $success,
            'message' => $message,
            'errors' => $errors ?? null,
        ], $code);
    }
}
