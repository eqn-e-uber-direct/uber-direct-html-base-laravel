<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Enums\OrderStatus;
use Illuminate\Http\Request;
use App\Enums\DeliveryStatus;
use Illuminate\Support\Carbon;
use App\Jobs\Eqn\CancelOrderJob;
use App\Jobs\Eqn\DeliverOrderJob;
use App\Jobs\Eqn\RescheduleOrderJob;

class CompleteDeliveriesController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        if (!$request->get('campaign')) {
            return back()
                ->withErrors([
                    'sector' => "Campanha não informada."
                ]);
        }

        // Busca todos os pedidos em processamento no setor e campanha informados.
        // São ordenados pela data de criação e ID mais recentes.
        $orders = Order::query()
            ->where('campaign_id', $request->get('campaign'))
            ->where('status', OrderStatus::PROCESSING)
            ->when($request->get('sector') !== null,
                function ($query) use ($request) {
                    $query->where('sector_id', $request->get('sector'))
                        ->orderBy('created_at', 'desc')
                        ->orderBy('id', 'desc');
                }
            )
            ->when($request->get('sector') === null,
                function ($query) {
                    $query->orderBy('cep', 'desc');
                }
            )
            ->get();

        $orders->each(function ($order) {
            $delivery = $order->lastDelivery();

            // Cancela o pedido e envia os dados para o EQN.
            if ($delivery->cancel_order && !$delivery->reschedule_order) {
                $delivery->update([
                    'status' => DeliveryStatus::CANCELED->value,
                    'reason' => 'Cancelado pelo administrador do sistema.',
                    'reschedule_order' => false,
                    'cancel_order' => false,
                ]);

                $order->update(['status' => OrderStatus::CANCELED->value]);

                // Cancela o pedido no EQN do drupal.
                CancelOrderJob::dispatch(
                    $order->id, $delivery->id, Carbon::now()->toDateTimeString()
                );
            }

            // Reagenda o pedido e envia os dados para o EQN.
            if ($delivery->reschedule_order && !$delivery->cancel_order) {
                $delivery->update([
                    'status' => DeliveryStatus::CANCELED->value,
                    'reason' => 'Cancelado pelo administrador do sistema.',
                    'reschedule_order' => false,
                    'cancel_order' => false,
                ]);

                $order->update(['status' => OrderStatus::PENDING->value]);

                // Reagenda o pedido no EQN do drupal.
                RescheduleOrderJob::dispatch($order->id);
            }

            // Finaliza o pedido e envia os dados para o EQN.
            if ($delivery->isDelivered()) {
                $order->update(['status' => OrderStatus::DELIVERED->value]);

                // Finaliza o pedido no EQN do drupal.
                DeliverOrderJob::dispatch($order->id);
            }
        });

        return back()->with('success', "{$orders->count()} pedidos estão sendo
            finalizados. Aguarde alguns instantes.");
    }
}
