<?php

namespace App\Http\Controllers;

use App\Models\Sector;
use App\Models\Campaign;
use App\Enums\OrderStatus;
use App\Actions\Order\ListOrdersCampaignAction;
use App\Actions\Order\CountOrdersCampaignAction;

class DetailSectorCanceledOrdersController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Campaign $campaign, Sector $sector)
    {
        $orders = ListOrdersCampaignAction::run(
            $campaign, $sector, [OrderStatus::CANCELED]
        );

        $ordersCount = CountOrdersCampaignAction::run(
            $campaign, $sector, [OrderStatus::CANCELED]
        );

        $currentUrl = route('campaigns.sectors.orders.show.canceled', [
            'campaign' => $campaign->slug,
            'sector' => $sector->id,
        ]);

        $status = OrderStatus::CANCELED;

        return view('web.sectors.details.standard', [
            'campaign' => $campaign,
            'sector' => $sector,
            'orders' => $orders,
            'currentUrl' => $currentUrl,
            'status' => $status,
            'ordersCount' => $ordersCount
        ]);
    }
}
