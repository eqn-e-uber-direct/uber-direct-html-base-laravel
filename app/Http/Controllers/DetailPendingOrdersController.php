<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Enums\OrderStatus;
use App\Actions\Order\ListOrdersCampaignAction;
use App\Actions\Order\CountOrdersCampaignAction;

class DetailPendingOrdersController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Campaign $campaign)
    {
        $queryStatus = [OrderStatus::PENDING];

        $querySorts = [['name' => 'cep', 'direction' => 'asc']];

        $orders = ListOrdersCampaignAction::run(
            $campaign, null, $queryStatus, false, $querySorts
        );

        $ordersCount = CountOrdersCampaignAction::run(
            $campaign, null, $queryStatus
        );

        $currentUrl = route('campaigns.orders.show.pending', [
            'campaign' => $campaign->slug,
        ]);

        $status = OrderStatus::PENDING;

        return view('web.orders.details.standard', [
            'campaign' => $campaign,
            'currentUrl' => $currentUrl,
            'orders' => $orders,
            'ordersCount' => $ordersCount,
            'status' => $status,
        ]);
    }
}
