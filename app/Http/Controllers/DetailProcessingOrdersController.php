<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Enums\OrderStatus;
use Illuminate\Support\Carbon;
use App\Actions\Order\ListOrdersCampaignAction;
use App\Actions\Order\CountOrdersCampaignAction;
use App\Actions\Order\CanCompleteOrdersCampaignAction;
use App\Actions\Order\ListProcessingOrdersBatchAction;
use App\Actions\Courier\ListCouriersByProcessingOrdersAction;

class DetailProcessingOrdersController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Campaign $campaign)
    {
        $queryStatus = [OrderStatus::PROCESSING, OrderStatus::WAITING];

        $querySorts = [['name' => 'cep', 'direction' => 'asc']];

        $orders = ListOrdersCampaignAction::run(
            $campaign, null, $queryStatus, true, $querySorts
        );

        $ordersCount = CountOrdersCampaignAction::run(
            $campaign, null, $queryStatus
        );

        $canCompleteOrders = CanCompleteOrdersCampaignAction::run(
            $campaign, null, $queryStatus
        );

        $couriers = ListCouriersByProcessingOrdersAction::run($campaign);

        $couriersCount = $couriers->count();

        $currentUrl = route('campaigns.orders.show.processing', [
            'campaign' => $campaign->slug,
        ]);

        $now = Carbon::now();

        $status = OrderStatus::PROCESSING;

        // Lista todos os pedidos com delivery em batch_ready.
        $batches = ListProcessingOrdersBatchAction::run($campaign);

        return view('web.orders.details.processing', compact(
            'batches',
            'campaign',
            'canCompleteOrders',
            'couriers',
            'couriersCount',
            'currentUrl',
            'now',
            'orders',
            'ordersCount',
            'status',
        ));
    }
}
