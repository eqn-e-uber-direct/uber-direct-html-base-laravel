<?php

namespace App\Http\Requests\Api;

use App\Models\Campaign;
use App\Enums\OrderStatus;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'id' => 'required',
            'status' => [
                'nullable',
                'sometimes',
                'string',
                Rule::in(OrderStatus::values()->toArray())
            ],
            'cep' => 'required|string',
            'phone_number' => 'required|string',
            'address' => 'required|string',
            'address_extra' => 'required|string',
            'note' => 'nullable|sometimes|string',
            'consumer' => 'required|string',
            'campaign_id' => 'required|exists:'.Campaign::TABLE.',id',
            'created_at' => 'sometimes|date_format:Y-m-d H:i:s|nullable',
            'updated_at' => 'sometimes|date_format:Y-m-d H:i:s|nullable',
        ];
    }
}
