<?php

namespace App\Http\Requests\Api;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCampaignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'id' => 'required',
            'title' => 'required|string',
            'slug' => 'required|string',
            'color' => 'required|string',
            'start_at' => 'required|date_format:Y-m-d H:i:s|nullable',
            'end_at' => 'required|date_format:Y-m-d H:i:s|nullable',
            'created_at' => 'sometimes|date_format:Y-m-d H:i:s|nullable',
            'updated_at' => 'sometimes|date_format:Y-m-d H:i:s|nullable',

            // Dados do produto.
            'product.id' => 'required',
            'product.name' => 'required|string',
            'product.sku' => 'required|string',
            'product.weight' => 'required|numeric|min:1',
            'product.length' => 'required|numeric|min:1',
            'product.height' => 'required|numeric|min:1',
            'product.depth' => 'required|numeric|min:1',
            'product.created_at' => 'sometimes|date_format:Y-m-d H:i:s|nullable',
            'product.updated_at' => 'sometimes|date_format:Y-m-d H:i:s|nullable',
        ];
    }
}
