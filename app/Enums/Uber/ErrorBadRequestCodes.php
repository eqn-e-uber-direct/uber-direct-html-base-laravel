<?php

namespace App\Enums\Uber;

enum ErrorBadRequestCodes: string
{
    case ADDRESS_UNDELIVERABLE                      = 'address_undeliverable';
    case UNKNOWN_LOCATION                           = 'unknown_location';
    case INVALID_PARAMS                             = 'invalid_params';
    case PICKUP_WINDOW_TOO_SMALL                    = 'pickup_window_too_small';
    case DROPOFF_DEADLINE_TOO_EARLY                 = 'dropoff_deadline_too_early';
    case DROPOFF_DEADLINE_BEFORE_PICKUP_DEADLINE    = 'dropoff_deadline_before_pickup_deadline';
    case DROPOFF_READY_AFTER_PICKUP_DEADLINE        = 'dropoff_ready_after_pickup_deadline';
    case PICKUP_READY_TOO_EARLY                     = 'pickup_ready_too_early';
    case PICKUP_DEADLINE_TOO_EARLY                  = 'pickup_deadline_too_early';
    case PICKUP_READY_TOO_LATE                      = 'pickup_ready_too_late';
    case REQUEST_TIMEOUT                            = 'request_timeout';
    case ADDRESS_UNDELIVERABLE_LIMITED_COURIERS     = 'address_undeliverable_limited_couriers';
    case EXPIRED_QUOTE                              = 'expired_quote';
    case MISSING_PAYMENT                            = 'missing_payment';
    case NOT_ALLOWED                                = 'not_allowed';
    case MISMATCHED_PRICE_QUOTE                     = 'mismatched_price_quote';

    public function reasonBrazil(): string
    {
        return match ($this) {
            self::ADDRESS_UNDELIVERABLE =>
                'A Localização especificada não está em uma região de entrega.',

            self::UNKNOWN_LOCATION =>
                'A localização especificada não foi compreendida.',

            self::INVALID_PARAMS =>
                'Os parametros da sua requisição são inválidos.',

            self::PICKUP_WINDOW_TOO_SMALL =>
                'O horário de retirada deve ser maior que 10 minutos.',

            self::DROPOFF_DEADLINE_TOO_EARLY =>
                'O horário de entrega deve ser maior que 20 minutos depois do horário de retirada.',

            self::DROPOFF_DEADLINE_BEFORE_PICKUP_DEADLINE =>
                'O horário de entrega deve ser maior que o horário de retirada.',

            self::DROPOFF_READY_AFTER_PICKUP_DEADLINE =>
                'O horário de prepraro de entrega deve ser anterior ou igual ao horário de retirada.',

            self::PICKUP_READY_TOO_EARLY =>
                'O horário de prepraro de entrega não pode estar no passado.',

            self::PICKUP_DEADLINE_TOO_EARLY =>
                'O horário de entrega deve ser pelo menos 20 minutos maior que o horário atual.',

            self::PICKUP_READY_TOO_LATE =>
                'O horário de retirada deve estar dentro do prazo de 30 dias.',

            self::REQUEST_TIMEOUT =>
                'O tempo de requisição expirou.',

            self::ADDRESS_UNDELIVERABLE_LIMITED_COURIERS =>
                'O local especificado não está em uma região de entrega por enquanto porque todos os entradores estão ocupados.',

            self::EXPIRED_QUOTE =>
                'A cotação de preço especificada expirou.',

            self::MISSING_PAYMENT =>
                'A organização não possui uma política de pagamento.',

            self::NOT_ALLOWED =>
                'Os parâmetros da sua requisição são inválidos.',

            self::MISMATCHED_PRICE_QUOTE =>
                'Cotação errada, por favor, recrie a cotação.',
        };
    }
}
