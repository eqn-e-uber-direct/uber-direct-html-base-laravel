<?php

namespace App\Enums\Uber;

enum DeliveryErrorRequestCodes:string
{
    // 402
    case CUSTOMER_SUSPENDED     = 'customer_suspended';

    // 403
    case CUSTOMER_BLOCKED       = 'customer_blocked';

    // 404
    case CUSTOMER_NOT_FOUND     = 'customer_not_found';

    // 409
    case DUPLICATE_DELIVERY     = 'duplicate_delivery';

    // 429
    case CUSTOMER_LIMITED       = 'customer_limited';

    // 500
    case UNKNOWN_ERROR          = 'unknown_error';

    // 503
    case COURIERS_BUSY          = 'couriers_busy';
    case ROBO_COURIERS_BUSY     = 'robo_couriers_busy';
    case SERVICE_UNAVAILABLE    = 'service_unavailable';

    public function reasonBrazil()
    {
        return match ($this) {
            self::CUSTOMER_SUSPENDED     => 'Sua conta está suspensa. Precisa efetuar o pagamento.',
            self::CUSTOMER_BLOCKED       => 'Sua conta não está habilidade para criar entregas.',
            self::CUSTOMER_NOT_FOUND     => 'O cliente não foi encontrado.',
            self::DUPLICATE_DELIVERY     => 'Existe uma entrega ativa igual esta.',
            self::CUSTOMER_LIMITED       => 'Os limites de sua conta foram excedidos.',
            self::UNKNOWN_ERROR          => 'Erro desconhecido.',
            self::COURIERS_BUSY          => 'Todos os nossos entregadores estão ocupados.',
            self::ROBO_COURIERS_BUSY     => 'Todos os entregadores robôs estão ocupados.',
            self::SERVICE_UNAVAILABLE    => 'Serviço não disponível.',
        };
    }
}