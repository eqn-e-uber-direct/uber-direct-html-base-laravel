<?php

namespace App\Enums\Uber;

enum UndeliverableReason: string
{
    // Merchant cancellation
    case MERCHANT_CANCEL_MERCHANT_CANCEL                = 'merchant_cancel.merchant_cancel';
    case MERCHANT_CANCEL_CANCELLED_BY_MERCHANT_API      = 'merchant_cancel.cancelled_by_merchant_api';
    case MERCHANT_CANCEL_NO_SECURE_LOCATION_TO_DROPOFF  = 'merchant_cancel.no_secure_location_to_dropoff';
    // =====

    // Courier
    case COURIER_CANCEL_CUSTOMER_UNAVAILABLE            = 'courier_cancel.customer_unavailable';
    case COURIER_CANCEL_CUSTOMER_NOT_AVAILABLE          = 'courier_cancel.customer_not_available';
    case COURIER_CANCEL_CUSTOMER_REJECTED_ORDER         = 'courier_cancel.customer_rejected_order';
    case COURIER_CANCEL_CANNOT_FIND_CUSTOMER_ADDRESS    = 'courier_cancel.cannot_find_customer_address';
    case COURIER_CANCEL_WRONG_ADDRESS                   = 'courier_cancel.wrong_address';
    case COURIER_CANCEL_CANNOT_ACCESS_CUSTOMER_LOCATION = 'courier_cancel.cannot_access_customer_location';
    case COURIER_CANCEL_RECIPIENT_INTOXICATED           = 'courier_cancel.recipient_intoxicated';
    case COURIER_CANCEL_RECIPIENT_ID                    = 'courier_cancel.recipient_id';
    case COURIER_CANCEL_CUSTOMER_ID_CHECK_FAILED        = 'courier_cancel.customer_id_check_failed';
    case COURIER_CANCEL_RECIPIENT_AGE                   = 'courier_cancel.recipient_age';
    case COURIER_CANCEL_PIN_MATCH_ISSUE                 = 'courier_cancel.pin_match_issue';
    case COURIER_CANCEL_EXCESSIVE_WAIT_TIME             = 'courier_cancel.excessive_wait_time';
    case COURIER_CANCEL_UNABLE_TO_FIND_PICKUP           = 'courier_cancel.unable_to_find_pickup';
    case COURIER_CANCEL_RESTAURANT_CLOSED               = 'courier_cancel.restaurant_closed';
    case COURIER_CANCEL_MERCHANT_CLOSED                 = 'courier_cancel.merchant_closed';
    case COURIER_CANCEL_MERCHANT_REFUSED                = 'courier_cancel.merchant_refused';
    case COURIER_CANCEL_OVERSIZED_ITEM                  = 'courier_cancel.oversized_item';
    case COURIER_CANCEL_OTHER                           = 'courier_cancel.other';
    case COURIER_CANCEL_ITEM_LOST                       = 'courier_cancel.item_lost';
    case COURIER_CANCEL_SUPPLIER_CLOSED                 = 'courier_cancel.supplier_closed';
    case COURIER_CANCEL_OTHER_RETURN                    = 'courier_cancel.other_return';
    //  =====

    // Uber
    case UBER_CANCEL_UBER_CANCEL                        = 'uber_cancel.uber_cancel';
    case UBER_CANCEL_BATCH_FORCE_ENDED_EXPIRED_ORDER    = 'uber_cancel.batch_force_ended_expired_order';
    case UBER_CANCEL_CANNOT_DISPATCH_COURIER            = 'uber_cancel.cannot_dispatch_courier';
    case UBER_CANCEL_ORDER_TASK_FAILED                  = 'uber_cancel.order_task_failed';
    case UBER_CANCEL_COURIER_REPORT_CRASH               = 'uber_cancel.courier_report_crash';
    case UBER_CANCEL_UNFULFILLMENT                      = 'uber_cancel.unfulfillment';
    //  =====

    // Customer
    case CUSTOMER_CANCEL_CUSTOMER_CANCEL                = 'customer_cancel.customer_cancel';
    case CUSTOMER_UNAVAILABLE_RETURNED                  = 'customer_unavailable.returned';
    // =====

    // Returned
    case RETURNED_CANNOT_ACCESS_CUSTOMER_LOCATION       = 'returned.cannot_access_customer_location';
    case RETURNED_CUSTOMER_UNAVAILABLE                  = 'returned.customer_unavailable';
    case RETURNED_NO_SECURE_LOCATION_TO_DROPOFF         = 'returned.no_secure_location_to_dropoff';
    // =====

    // Unknown
    case UNKNOWN_CANCEL_UNKNOWN_CANCEL                  = 'unknown_cancel.unknown_cancel';
    // =====

    public function reasonBrazil(): string
    {
        return match ($this) {
            self::MERCHANT_CANCEL_MERCHANT_CANCEL =>
                'Cancelado pela loja',

            self::MERCHANT_CANCEL_CANCELLED_BY_MERCHANT_API =>
                'Cancelado pela loja',

            self::MERCHANT_CANCEL_NO_SECURE_LOCATION_TO_DROPOFF =>
                'Entregador não encontrou uma área segura para finalizar a entrega do produto',

            self::COURIER_CANCEL_CUSTOMER_UNAVAILABLE =>
                'Cliente não estava disponível para receber a entrega',

            self::COURIER_CANCEL_CUSTOMER_NOT_AVAILABLE =>
                'Cliente não estava disponível para receber a entrega',

            self::COURIER_CANCEL_CUSTOMER_REJECTED_ORDER =>
                'Cliente rejeitou receber a entrega',

            self::COURIER_CANCEL_CANNOT_FIND_CUSTOMER_ADDRESS =>
                'Entregador não conseguiu encontrar o endereço correto do cliente',

            self::COURIER_CANCEL_WRONG_ADDRESS =>
                'Endereço do cliente está errado',

            self::COURIER_CANCEL_CANNOT_ACCESS_CUSTOMER_LOCATION =>
                'Endereço do cliente está em uma área não acessível',

            self::COURIER_CANCEL_RECIPIENT_INTOXICATED =>
                'Cliente não está sóbrio para receber a entrega',

            self::COURIER_CANCEL_RECIPIENT_ID =>
                'O ID do cliente não bate com o requerido',

            self::COURIER_CANCEL_CUSTOMER_ID_CHECK_FAILED =>
                'O ID do cliente não bate com o requerido',

            self::COURIER_CANCEL_RECIPIENT_AGE =>
                'Cliente não é maior de idade para receber a entrega',

            self::COURIER_CANCEL_PIN_MATCH_ISSUE =>
                'Endereço do cliente está errado',

            self::COURIER_CANCEL_EXCESSIVE_WAIT_TIME =>
                'Entregador aguardou o tempo total mas o cliente não apareceu para receber a entrega',

            self::COURIER_CANCEL_UNABLE_TO_FIND_PICKUP =>
                'Entregador não encontrou a loja para retirar o produto',

            self::COURIER_CANCEL_RESTAURANT_CLOSED =>
                'A loja estava fechada',

            self::COURIER_CANCEL_OVERSIZED_ITEM =>
                'Itens muito grandes para realizar a retirada',

            self::COURIER_CANCEL_OTHER =>
                'Outros motivos',

            self::COURIER_CANCEL_ITEM_LOST =>
                'Os itens foram perdidos durante a viagem de retorno',

            self::COURIER_CANCEL_SUPPLIER_CLOSED =>
                'A loja estava fechada',

            self::COURIER_CANCEL_MERCHANT_CLOSED =>
                'A loja estava fechada',

            self::COURIER_CANCEL_MERCHANT_REFUSED =>
                'Loja recusou o produto',

            self::COURIER_CANCEL_OTHER_RETURN =>
                'Outros motivos',

            self::UBER_CANCEL_UBER_CANCEL =>
                'Cancelado pela Uber',

            self::UBER_CANCEL_BATCH_FORCE_ENDED_EXPIRED_ORDER =>
                'Tempo de tentativa de alocação de entregador excedido na região',

            self::UBER_CANCEL_CANNOT_DISPATCH_COURIER =>
                'Não foi possível alocar um entregador na região',

            self::UBER_CANCEL_ORDER_TASK_FAILED =>
                'Problemas Internos',

            self::UBER_CANCEL_COURIER_REPORT_CRASH =>
                'O entregador acidentou-se',

            self::UBER_CANCEL_UNFULFILLMENT =>
                'Não foi possível alocar um entregador na região',

            self::CUSTOMER_CANCEL_CUSTOMER_CANCEL =>
                'Cancelado pelo cliente final',

            self::CUSTOMER_UNAVAILABLE_RETURNED =>
                'Cliente não encontrado, entrega retornada para a loja',

            self::RETURNED_CANNOT_ACCESS_CUSTOMER_LOCATION =>
                'Retornado. Endereço do cliente não está acessível',

            self::RETURNED_CUSTOMER_UNAVAILABLE =>
                'Retornado. Cliente não está disponível para receber a entrega',

            self::RETURNED_NO_SECURE_LOCATION_TO_DROPOFF =>
                'Retornado. Entregador não encontrou uma área segura para finalizar a entrega do produto',

            self::UNKNOWN_CANCEL_UNKNOWN_CANCEL =>
                'Cancelamento desconhecido',
        };
    }
}