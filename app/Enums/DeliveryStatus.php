<?php

namespace App\Enums;

use App\Models\Delivery;
use Illuminate\Support\Collection;

enum DeliveryStatus: string
{
    case PENDING = 'pending';
    case BATCH_READY = 'batch_ready';
    case PICKUP = 'pickup';
    case PICKUPCOMPLETE = 'pickup_complete';
    case DROPOFF = 'dropoff';
    case DELIVERED = 'delivered';
    case CANCELED = 'canceled';
    case RETURNED = 'returned';

    // Properties

    public function notes(?Delivery $delivery = null): string
    {
        if ($delivery) {
            if ((bool)$delivery->problem) return (string)$delivery->reason;
        }

        return match ($this) {
            self::PENDING =>
                "Aguardando a confirmação do entregador",

            self::BATCH_READY =>
                "Aguardando a retirada pelo entregador",

            self::PICKUP =>
                "Aguardando a retirada pelo entregador",

            self::PICKUPCOMPLETE =>
                "Confirmação da coleta pelo entregador",

            self::DROPOFF =>
                "Pacote à caminho do endereço de entrega",

            self::DELIVERED =>
                "Entregador confirmou a entrega",

            self::CANCELED =>
                "Entrega cancelada por algum problema",

            self::RETURNED =>
                "Entrega retornada pelo entregador",
        };
    }

    public function weight(): int
    {
        return match ($this) {
            self::PENDING => 1,

            self::BATCH_READY => 2,

            self::PICKUP => 3,

            self::PICKUPCOMPLETE => 4,

            self::DROPOFF => 5,

            self::DELIVERED => 6,

            self::CANCELED => 7,

            self::RETURNED => 8,
        };
    }

    // Converters

    public function toView(): string
    {
        return match ($this) {
            self::PENDING => 'Em rota',
            self::BATCH_READY => 'Em rota',
            self::PICKUP => 'Em rota',
            self::PICKUPCOMPLETE => 'Em rota',
            self::DROPOFF => 'Em rota',
            self::DELIVERED => 'Entregue',
            self::CANCELED => 'Não entregue',
            self::RETURNED => 'Retornado',
        };
    }

    // Validators

    public function isNotSent(): bool
    {
        return $this === self::RETURNED;
    }

    public function isCanceled(): bool
    {
        return $this === self::CANCELED;
    }

    // Static helpers

    public static function all(): Collection
    {
        return collect([
            self::PENDING,
            self::BATCH_READY,
            self::PICKUP,
            self::PICKUPCOMPLETE,
            self::DROPOFF,
            self::DELIVERED,
            self::CANCELED,
            self::RETURNED,
        ]);
    }

    public static function values(): Collection
    {
        return collect([
            self::PENDING->value,
            self::BATCH_READY->value,
            self::PICKUP->value,
            self::PICKUPCOMPLETE->value,
            self::DROPOFF->value,
            self::DELIVERED->value,
            self::CANCELED->value,
            self::RETURNED->value,
        ]);
    }

    public static function toViewCollection(string $status): Collection
    {
        $status = self::tryFrom($status);

        if (is_null($status)) return collect();

        return collect()
            ->put('value', $status->toView())
            ->put('notes', $status->notes());
    }
}