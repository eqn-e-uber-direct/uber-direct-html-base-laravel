<?php

namespace App\Enums;

use Illuminate\Support\Collection;

enum OrderStatus: string
{
   case PENDING = 'pending';
   case WAITING = 'waiting';
   case PROCESSING = 'processing';
   case DELIVERED = 'delivered';
   case CANCELED = 'canceled';

   public function toView(): string
   {
      return match ($this) {
         self::PENDING => 'Pendente',
         self::WAITING => 'Aguardando',
         self::PROCESSING => 'Processando',
         self::DELIVERED => 'Entregue',
         self::CANCELED => 'Cancelado',
      };
   }

   public static function all(): Collection
   {
      return collect([
         self::PENDING,
         self::WAITING,
         self::PROCESSING,
         self::DELIVERED,
         self::CANCELED,
      ]);
   }

   public static function values(): Collection
   {
      return collect([
         self::PENDING->value,
         self::WAITING->value,
         self::PROCESSING->value,
         self::DELIVERED->value,
         self::CANCELED->value,
      ]);
   }

   public function isPending(): bool
   {
      return $this->value === self::PENDING->value;
   }

   public function isWaiting(): bool
   {
      return $this->value === self::WAITING->value;
   }

   public function isProcessing(): bool
   {
      return $this->value === self::PROCESSING->value;
   }

   public function isDelivered(): bool
   {
      return $this->value === self::DELIVERED->value;
   }

   public function isCanceled(): bool
   {
      return $this->value === self::CANCELED->value;
   }
}