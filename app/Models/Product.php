<?php

namespace App\Models;

use App\Models\Campaign;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    const TABLE = 'products';

    protected $fillable = [
        'id',
        'name',
        'sku',
        'weight',
        'length',
        'height',
        'depth',
        'created_at',
        'updated_at',
    ];

    // Relationships.
    public function campaigns()
    {
        return $this->hasMany(Campaign::class);
    }
}
