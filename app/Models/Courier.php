<?php

namespace App\Models;

use App\Actions\Courier\ListCourierProcessingOrdersAction;
use App\Models\Delivery;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Courier extends Model
{
    use HasFactory;
    use SoftDeletes;

    const TABLE = 'couriers';

    protected $fillable = [
        'phone_number',
        'name',
        'vehicle_type',
        'vehicle_make',
        'vehicle_model',
        'vehicle_color',
        'plate',
    ];

    // Relationships

    public function deliveries()
    {
        return $this->hasMany(Delivery::class);
    }

    public function processingOrdersQty(?Sector $sector = null)
    {
        return (new ListCourierProcessingOrdersAction)
            ->count($this, $sector);
    }

    // Views

    public function viewProcessingOrdersQty(?Sector $sector = null)
    {
        $qty = $this->processingOrdersQty($sector);

        $text = $qty > 1 ? 'pedidos' : 'pedido';

        return "{$qty} {$text}";
    }

    public function viewVehicle()
    {
        $vehicle = implode(' ', [
            $this->vehicle_make, $this->vehicle_model,
            $this->vehicle_color, $this->vehicle_type
        ]);

        return ucfirst($vehicle ?? '');
    }
}
