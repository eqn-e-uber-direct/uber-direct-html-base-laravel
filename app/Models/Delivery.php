<?php

namespace App\Models;

use App\Models\Order;
use App\Models\Courier;
use App\Enums\DeliveryStatus;
use App\Traits\Delivery\DeliveryStatusTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Log;

class Delivery extends Model
{
    use HasFactory;
    use SoftDeletes;
    use DeliveryStatusTrait;

    const TABLE = 'deliveries';

    protected $fillable = [
        'order_id',
        'uid',
        'uber_uuid',
        'uber_id',
        'batch_id',
        'status',
        'courier_id',
        'reason',
        'problem',
        'cancel_order',
        'reschedule_order',
        'tracking_url',
        'data',
    ];

    protected $casts = [
        'cancel_order' => 'boolean',
        'reschedule_order' => 'boolean',
    ];

    // Relationships

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function courier()
    {
        return $this->belongsTo(Courier::class);
    }

    // Getters

    public function dataToJson()
    {
        return json_decode($this->data, true);
    }

    public function storeProblem()
    {
        return !$this->problem && ($this->isCanceled() || $this->isReturned());
    }

    public function canComplete()
    {
        if (!$this->isDelivered() && !$this->cancel_order && !$this->reschedule_order) {
            return false;
        }

        return true;
    }

    public function canCancel()
    {
        if ($this->cancel_order || $this->reschedule_order)
            return false;

        if ($this->isCanceled() || $this->isReturned())
            return true;

        return false;
    }

    public function canReschedule()
    {
        if ($this->cancel_order || $this->reschedule_order)
            return false;

        if ($this->isPending() || $this->isCanceled() || $this->isReturned())
            return true;

        return false;
    }

    public function canUpdateStatus(DeliveryStatus $newStatus)
    {
        // Log comentado para ser adicionado de novo quando precisar.
        // Log::info("Update Delivery ID ({$this->id}) check CAN UPDATE", [
        //     'newStatus' => $newStatus,
        //     'dbStatus' => $this->status,
        // ]);

        // Não pode mudar um status entregue.
        if ($this->isDelivered()) return false;

        // Não pode mudar um status retornado.
        if ($this->isReturned()) return false;

        // Pode haver mudança no status cancelado por causa do retorno
        // que vai criar um novo pedido e vai repetir os outros status.
        if ($this->isCanceled()) return true;

        // Os outros status podem mudar, mas devem seguir uma ordem
        // conforme o seu peso.
        $status = DeliveryStatus::from($this->status);

        // Log comentado para ser adicionado de novo quando precisar.
        // Log::info("Delivery id ({$this->id}) CAN UPDATE STATUS", [
        //     'newStatus' => $newStatus,
        //     'newStatus weight' => $newStatus->weight(),
        //     'status' => $status,
        //     'status weight' => $status->weight(),
        // ]);

        if ($newStatus->weight() <= $status->weight()) return false;

        return true;
    }

    public function inBatch()
    {
        return ! is_null($this->batch_id);
    }

    public function uberUuid()
    {
        if (is_null($this->uber_uuid)) return null;

        $uberUuid = substr($this->uber_uuid, -5);

        return $uberUuid;
    }
}
