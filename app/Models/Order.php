<?php

namespace App\Models;

use App\Enums\OrderStatus;
use App\Models\Sector;
use App\Models\Campaign;
use App\Traits\Order\OrderStatusTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model
{
    use HasFactory;
    use SoftDeletes;
    use OrderStatusTrait;

    const TABLE = 'orders';

    protected $fillable = [
        'id',
        'status',
        'cep',
        'phone_number',
        'address',
        'address_extra',
        'note',
        'consumer',
        'phone_number',
        'sector_id',
        'campaign_id',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'status' => OrderStatus::class,
    ];

    // Relationships

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    public function sector()
    {
        return $this->belongsTo(Sector::class);
    }

    public function deliveries()
    {
        return $this->hasMany(Delivery::class);
    }

    // Queries

    public function lastDelivery()
    {
        return $this->deliveries()->latest()->first();
    }

    // Logics

    public function makeDeliveryUid()
    {
        return uniqid("{$this->id}|", true);
    }

    // Views

    public function formattedCep()
    {
        $cep = substr($this->cep, 0, 5) . '-' . substr($this->cep, 5, 3);

        return $cep;
    }

    public function fullAddress()
    {
        $address = [
            $this->address,
            $this->note,
            $this->address_extra,
            $this->formattedCep()
        ];

        return implode(' - ', array_filter($address));
    }

    public function lastDeliveryReason()
    {
        $delivery = $this->lastDelivery();

        if (!$delivery) return null;

        return $delivery->reason;
    }

    public function lastDeliveryUuid()
    {
        $delivery = $this->lastDelivery();

        if (!$delivery) return null;

        if (is_null($delivery->uber_uuid)) return null;

        $uberUuid = substr($delivery->uber_uuid, -5);

        return $uberUuid;
    }

    public function lastDeliveryTrackingUrl()
    {
        $delivery = $this->lastDelivery();

        if (!$delivery) return null;

        return $delivery->tracking_url;
    }
}
