<?php

namespace App\Models;

use App\Models\Order;
use App\Models\Sector;
use App\Models\Product;
use App\Enums\OrderStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Campaign extends Model
{
    use HasFactory;
    use SoftDeletes;

    const TABLE = 'campaigns';

    protected $fillable = [
        'id',
        'title',
        'slug',
        'color',
        'start_at',
        'end_at',
        'product_id',
        'created_at',
        'updated_at',
    ];

    // Relationships

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    // Actions

    public function ordersQty($format = false)
    {
        $count = $this->orders()->count();

        return $format ? number_format($count, 0, ',', '.') : $count;
    }

    public function pendingOrdersQty($format = false)
    {
        $count = $this->orders()
            ->where('status', OrderStatus::PENDING)
            ->count();

        return $format ? number_format($count, 0, ',', '.') : $count;
    }

    public function sectors()
    {
        $orderSectors = $this->orders()
            ->select('sector_id')
            ->groupBy('sector_id')
            ->get();

        return Sector::find($orderSectors->pluck('sector_id'));
    }
}
