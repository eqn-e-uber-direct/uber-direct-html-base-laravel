<?php

namespace App\Models;

use App\Models\Order;
use App\Models\Campaign;
use App\Enums\OrderStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Sector extends Model
{
    use HasFactory;
    use SoftDeletes;

    const TABLE = 'sectors';

    protected $fillable = [
        'name',
        'start',
        'end',
        'places',
    ];

    // Relationships

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    // Actions

    public function ordersQtyCampaign(Campaign $campaign, $format = false)
    {
        $count = $this->orders()
            ->where('campaign_id', $campaign->id)
            ->count();

        return $format ? number_format($count, 0, ',', '.') : $count;
    }

    public function pendingOrdersQtyCampaign(Campaign $campaign, $format = false)
    {
        $count = $this->orders()
            ->where('campaign_id', $campaign->id)
            ->where('status', OrderStatus::PENDING)
            ->count();

        return $format ? number_format($count, 0, ',', '.') : $count;
    }

    public function deliveredOrdersQtyCampaign(Campaign $campaign, $format = false)
    {
        $count = $this->orders()
            ->where('campaign_id', $campaign->id)
            ->where('status', OrderStatus::DELIVERED)
            ->count();

        return $format ? number_format($count, 0, ',', '.') : $count;
    }

    public function canceledOrdersQtyCampaign(Campaign $campaign, $format = false)
    {
        $count = $this->orders()
            ->where('campaign_id', $campaign->id)
            ->where('status', OrderStatus::CANCELED)
            ->count();

        return $format ? number_format($count, 0, ',', '.') : $count;
    }

    public function processingOrdersQtyCampaign(Campaign $campaign, $format = false)
    {
        $count = $this->orders()
            ->where('campaign_id', $campaign->id)
            ->whereIn('status', [OrderStatus::PROCESSING, OrderStatus::WAITING])
            ->count();

        return $format ? number_format($count, 0, ',', '.') : $count;
    }

    public function ceps()
    {
        return collect([$this->start, $this->end]);
    }

    public function places()
    {
        return collect(json_decode($this->places));
    }

    public function descriptionPlaces()
    {
        return implode(', ', $this->places()->toArray());
    }
}
