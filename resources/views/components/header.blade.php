<header>
    <div class="header">
        <div>
            <div>
                @if (Route::current()->getName() == 'settings.configs')
                <div class="btn-connection">
                    <a class="title-header-active" href="{{ route('settings.configs') }}">
                        <img src="{{ Vite::asset('resources/img/info_marrom.svg') }}" alt="">
                        <span class="text-info-connection">Informações de conexão</span>
                    </a>
                </div>
                @else
                <div class="btn-connection">
                    <a class="title-header-inactive" href="{{ route('settings.configs') }}">
                        <img src="{{ Vite::asset('resources/img/info.svg') }}" alt="">
                        <span class="text-info-connection">Informações de conexão</span>
                    </a>
                </div>
                @endif
            </div>
        </div>
        <a href="{{ route('campaigns.index') }}" class="logos">
            <img src="{{ Vite::asset('resources/img/logos-new.svg') }}" alt="Logo Uber e Eu Quero Nestle" />
</a>
        <form action="{{ route('logout') }}" method="post">
            @csrf
            <button type="submit" class="logout">Sair</button>
        </form>
    </div>

</header>