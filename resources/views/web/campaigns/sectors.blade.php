<x-guest-layout>
    @section('title', $campaign->title)
    <x-header></x-header>
    <main class="group" id="top">
        <section>
            <div class="title">
                <div class="content-title">
                    <div class="path">
                        <a href="{{ route('campaigns.index') }}">Todas as campanhas</a>
                        <p>Pedidos - {{ $campaign->title }}</p>
                    </div>
                    <h1>Pedidos {{ $campaign->title }}</h1>
                </div>
                <div class="info-title">
                    <div class="data-title">
                        <h3 class="number">{{ TextHelper::formatNumber($campaign->ordersQty()) }}</h3>
                        <h3 class="text">Pedidos</h3>
                    </div>
                    <div class="data-title">
                        <h3 class="number">{{ TextHelper::formatNumber($campaign->pendingOrdersQty()) }}</h3>
                        <h3 class="text">Pendentes</h3>
                    </div>
                </div>
            </div>
            @If ($campaign->ordersQty() > 0)
                <div class="content">
                    @foreach ($campaign->sectors() as $sector)
                        @php
                            $classProcessing = $sector->processingOrdersQtyCampaign($campaign) > 0
                                ? 'processing'
                                : '';
                        @endphp
                        <div class="card {{$classProcessing}}">
                            <div class="first-content">
                                <div class="content-wrapper">
                                    <h2>{{ $sector->name }}</h2>
                                    <p>CEPs com início entre {{ $sector->start }} e {{ $sector->end }}.</p>
                                    @if ($sector->places()->isEmpty() === false)
                                        <p>Bairros: {{ $sector->descriptionPlaces() }}.</p>
                                    @endif
                                </div>
                                <div class="content-wrapper">
                                    <div class="infos">
                                        <div class="data">
                                            <h3 class="number">{{ TextHelper::formatNumber($sector->ordersQtyCampaign($campaign, true)) }}</h3>
                                            <h3 class="text">Pedidos</h3>
                                        </div>
                                        <div class="data">
                                            <h3 class="number">{{ TextHelper::formatNumber($sector->pendingOrdersQtyCampaign($campaign, true)) }}</h3>
                                            <h3 class="text">Pendentes</h3>
                                        </div>
                                        <div class="data">
                                            <h3 class="number">{{ TextHelper::formatNumber($sector->deliveredOrdersQtyCampaign($campaign, true)) }}</h3>
                                            <h3 class="text">Entregues</h3>
                                        </div>
                                        <div class="data">
                                            <h3 class="number">{{ TextHelper::formatNumber($sector->canceledOrdersQtyCampaign($campaign, true)) }}</h3>
                                            <h3 class="text">Cancelados</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="final-content">
                                <a
                                    href="{{ route('campaigns.sectors.orders.index', [
                                        'campaign' => $campaign->slug,
                                        'sector' => $sector->id,
                                    ]) }}">Ver
                                    pedidos</a>
                                <p class="update">Atualizado em {{ $now->format('d/m/Y') }} às {{ $now->format('H:i') }}
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="empty">
                    <div class="empty-message">
                        <img src="{{ Vite::asset('resources/img/box.png') }}">
                        <h3>Não existem pedidos nesta campanha.</h3>
                    </div>
                </div>
            @endif
        </section>
        <a class="up" href="#top"></a>
    </main>
    <x-footer></x-footer>
</x-guest-layout>
