<x-guest-layout>
    <x-header></x-header>
    <main class="campaigns">
        @section('title', 'Campanhas em São Paulo SP')
        <section>
            <h1>Campanhas em São Paulo SP</h1>
            <div class="content">
                @foreach ($campaigns as $campaign)
                    <div class="card">
                        <div class="title">
                            <h2>{{ $campaign->title }}</h2>
                            <a href="{{ route('campaigns.orders.index', $campaign->slug) }}">Ver pedidos</a>
                        </div>
                        <div class="infos">
                            <div class="data">
                                <h3 class="number">{{ TextHelper::formatNumber($campaign->ordersQty()) }}</h3>
                                <h3 class="text">Pedidos</h3>
                            </div>
                            <div class="data">
                                <h3 class="number">{{ TextHelper::formatNumber($campaign->pendingOrdersQty()) }}</h3>
                                <h3 class="text">Pendentes</h3>
                            </div>
                        </div>
                        <p class="update">Atualizado em {{ $now->format('d/m/Y') }} às {{ $now->format('H:i') }}</p>
                    </div>
                @endforeach
            </div>
        </section>
    </main>
    <x-footer></x-footer>
    {{ $campaigns->appends(request()->query())->links() }}
</x-guest-layout>
