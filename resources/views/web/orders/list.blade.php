<x-guest-layout>
    @if ($errors->any())
        <div class="alert alert-error">
            @foreach($errors->all() as $error)
                <div>
                    <p>{{ $error }}</p>
                </div>
            @endforeach
        </div>
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            <div>
                <p>{{ session('success') }}</p>
            </div>
        </div>
    @endif
    @section('title', $campaign->title)
    <x-header></x-header>
    <main class="group-detail group-detail-inner" id="top">
        @include('web.orders.panels.intro')
        @include('web.orders.panels.processing')
        @include('web.orders.panels.pending')
        @include('web.orders.panels.delivered')
        @include('web.orders.panels.canceled')
        <a class="up" href="#top"></a>
    </main>
    <x-footer></x-footer>
</x-guest-layout>
