{{-- CÓDIGO PARA VOLTAR NO CAMINHO DE PAO --}}
@php
    $currentUrl = url()->current(); // Obtém a URL atual
    $currentUrlWithoutTrailingSlash = preg_replace('/\/[^\/]*$/', '', $currentUrl); // Remove a última barra e o conteúdo após ela
@endphp

<x-guest-layout>
    @section('title', $campaign->title . ' - ' . __('order_status.' . $status->value))
    <x-header></x-header>
    <main class="group-detail-inner" id="top">
        <section class="intro">
            <div class="title">
                <div class="path">
                    <a href="{{ route('campaigns.index') }}">Todas as campanhas</a>
                    <a href="{{ route('campaigns.orders.index', $campaign->slug) }}">Pedidos - {{ $campaign->title }}</a>
                    <p>{{ __('order_status.' . $status->value) }}</p>
                </div>
                <div class="intro">
                    <h2>{{ $campaign->title }}</h2>
                </div>
            </div>
        </section>
        <div class="orders delivery">
            @include('web.orders.panels.processing', [
                'processingOrders' => collect($orders->items()),
                'processingCount' => $ordersCount,
                'detail' => true,
            ])
        </div>
        {{ $orders->appends(request()->query())->links('partials.my-paginate', ['paginator' => $orders]) }}
        <a class="up" href="#top"></a>
    </main>
    <x-footer></x-footer>
</x-guest-layout>
