<div class="orders pending">
    <div class="title-orders">
        <h2>Pendentes</h2>
        <p>{{ TextHelper::formatNumber($pendingCount) }} {{ TextHelper::pluralize($pendingCount, 'pedido', 'pedidos') }}</p>
    </div>
    <div class="container-orders">
        <div class="intro-orders grid">
            <h3>Nome</h3>
            <h3>Endereço</h3>
            <h3>Setor</h3>
            {{-- <h3>Status</h3> --}}
            <h3>Data</h3>
        </div>
        @if ($pendingCount > 0)
            @foreach ($pendingOrders as $order)
                <div class="order-description grid">
                    <div class="content-container">
                        <p>{{ $order->consumer }}</p>
                    </div>
                    <div class="content-container">
                        <p>{{ $order->fullAddress() }}</p>
                    </div>
                    {{-- <div class="content-container">
                        <p>{{ $order->sector->name }}</p>
                    </div> --}}
                    <div class="content-container">
                        <span>Pendente</span>
                    </div>
                    <div class="content-container">
                        <span>{{ $order->created_at->format('d/m/Y') }}</span>
                    </div>
                </div>
            @endforeach
            @if ($pendingCount > 10)
                <a href="{{ route('campaigns.orders.show.pending', [
                    'campaign' => $campaign->slug,
                ]) }}">
                    Ver todos os pendentes
                </a>
            @endif
        @else
            <p class="notFound">Não existem pedidos pendentes</p>
        @endif
    </div>
</div>