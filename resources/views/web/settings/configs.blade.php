<x-guest-layout>
    @section('title', 'Informações de conexão')
    <x-header></x-header>
    <main class="group" id="top">
        <section>
            <div class="title-connection content">
                <div class="content-title">
                    <div class="arrow">
                        <a href="{{ route('campaigns.index') }}">VOLTAR PARA A PÁGINA ANTERIOR</a>
                    </div>
                    <h1 class="connection-info">Informações de Conexão</h1>
                </div>

                <div class="content-body connection-tables">
                    <div class="connection-table-container">
                        <table class="connection-table">
                            <thead>
                                <tr>
                                    <th>Uber Direct</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <p>UBER_PATH</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.path') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>UBER_LOGIN_PATH</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.path') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>UBER_CLIENT_SECRET</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.client_secret') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>UBER_CLIENT_ID</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.client_id') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>UBER_COSTUMER_ID</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.customer_id') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>UBER_WEBHOOK_STATUS_KEY</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.webhook.status') }}</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="connection-table-container">
                        <table class="connection-table">
                            <thead>
                                <tr>
                                    <th>Entrega (valores em minutos)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <p>UBER_DELIVERY_PICKUP_READY</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.delivery.pickup_ready.min') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>UBER_DELIVERY_PICKUP_READY_HOUR</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.delivery.pickup_ready.hour') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>UBER_DELIVERY_PICKUP_DEADLINE</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.delivery.pickup_deadline.min') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>UBER_DELIVERY_PICKUP_DEADLINE_HOUR</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.delivery.pickup_deadline.hour') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>UBER_DELIVERY_DROPOFF_READY</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.delivery.dropoff_ready.min') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>UBER_DELIVERY_DROPOFF_READY_HOUR</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.delivery.dropoff_ready.hour') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>UBER_DELIVERY_DROPOFF_DEADLINE</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.delivery.dropoff_deadline.min') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>UBER_DELIVERY_DROPOFF_DEADLINE_HOUR</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.delivery.dropoff_deadline.hour') }}</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="connection-table-container">
                        <table class="connection-table">
                            <thead>
                                <tr>
                                    <th>Endereço e loja</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <p>USER_NAME</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.user.name') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>USER_ADDRESS_STREET</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.address.street') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>USER_ADDRESS_EXTRA</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.address.extra') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>USER_ADDRESS_NOTE</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.address.note') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>USER_ADDRESS_CEP</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.address.zipcode') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>USER_PHONE</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.user.phone') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>USER_STORE_CODE</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.user.store_code') }}</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>


                    <div class="connection-table-container">
                        <table class="connection-table">
                            <thead>
                                <tr>
                                    <th>Código de barras</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <p>USER_BARCODE_NUMBER</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.barcode.number') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>USER_BARCODE_TYPE</p>
                                    </td>
                                    <td>
                                        <p>{{ config('uber.barcode.type') }}</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="connection-table-container">
                        <table class="connection-table">
                            <thead>
                                <tr>
                                    <th>Conexão com o EQN</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <p>EQN_STATUS</p>
                                    </td>
                                    <td>
                                        <p>{{ config('eqn.active') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>EQN_PATH</p>
                                    </td>
                                    <td>
                                        <p>{{ config('eqn.path') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>EQN_USERNAME</p>
                                    </td>
                                    <td>
                                        <p>{{ config('eqn.username') }}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>EQN_PASSWORD</p>
                                    </td>
                                    <td>
                                        <p>{{ config('eqn.password') }}</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
        </section>
        <a class="up" href="#top"></a>
    </main>
    <x-footer></x-footer>
</x-guest-layout>