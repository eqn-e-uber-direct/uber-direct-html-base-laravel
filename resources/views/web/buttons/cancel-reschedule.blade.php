@if ($delivery ?? null)
    <div class="not-sent-options">
        {{-- REAGENDAR --}}
        @if ($delivery->canReschedule())
            <form method="POST" action="{{ route('deliveries.reschedule', [
                'delivery' => $delivery->id,
            ]) }}">
                @csrf
                <input type="hidden" id="reschedule_order" name="reschedule_order" value=1/>
                <button type="submit">Reagendar</button>
            </form>
        @endif
        {{-- CANCELAR --}}
        @if ($delivery->canCancel())
            <form method="POST" action="{{ route('deliveries.cancel', [
                'delivery' => $delivery->id
            ]) }}">
                @csrf
                <input type="hidden" id="cancel_order" name="cancel_order" value=1/>
                <button type="submit">Cancelar</button>
            </form>
        @endif
    </div>
@endif