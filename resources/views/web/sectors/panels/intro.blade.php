<section class="intro">
    <div class="title">
        <div class="content-title">
            <div class="path">
                <a href="{{ route('campaigns.index') }}">Todas as campanhas</a>
                <a href="{{ route('campaigns.show', $campaign->slug) }}">Pedidos - {{ $campaign->title }}</a>
                <p>{{ $sector->name }}</p>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="card">
            <div class="first-content">
                <div class="content-wrapper">
                    <h2>{{ $sector->name }}</h2>
                    <p>
                        CEPs com início entre {{ $sector->start }} e {{ $sector->end }}.
                        @if ($processingCount < 1)
                        @php
                            $pickupHour = UberDeliveryTimeHelper::pickup()->format('H:i');
                            $dropoffHour = UberDeliveryTimeHelper::dropoff()->format('H:i');
                        @endphp
                            <strong>
                                Se uma janela for aberta agora, os entregadores deverão chegar próximos
                                @choice("da {$pickupHour} hora|das {$pickupHour} horas", $pickupHour)
                                e suas entregas e retornos devem ser concluídos por volta
                                @choice("da {$dropoffHour} hora|das {$dropoffHour} horas", $dropoffHour).
                            </strong>
                        @endif
                        @if ($processingNotWaitingCount > 0)
                            @php
                                $start = $uberProcessingTimes->get('start')->format('H:i');
                                $pickup = $uberProcessingTimes->get('pickup')->format('H:i');
                                $dropoff = $uberProcessingTimes->get('dropoff')->format('H:i');
                            @endphp
                            <strong>
                                A janela está aberta desde
                                @choice("{$start} hora|{$start} horas", $start),
                                os entregadores deverão/devem ter chegado próximos
                                @choice("da {$pickup} hora|das {$pickup} horas", $pickup)
                                e suas entregas e retornos devem ser concluídos por volta
                                @choice("da {$dropoff} hora|das {$dropoff} horas", $dropoff).
                            </strong>
                        @endif
                    </p>
                    @if ($sector->places()->isEmpty() === false)
                        <p class="neighborhoods">Bairros: {{ $sector->descriptionPlaces() }}.</p>
                    @endif
                </div>
                <div class="content-wrapper">
                    <div class="infos">
                        <div class="data">
                            <h3 class="number">{{ TextHelper::formatNumber($ordersCount) }}</h3>
                            <h3 class="text">Pedidos</h3>
                        </div>
                        <div class="data">
                            <h3 class="number">{{ TextHelper::formatNumber($pendingCount) }}</h3>
                            <h3 class="text">Pendentes</h3>
                        </div>
                        <div class="data">
                            <h3 class="number">{{ TextHelper::formatNumber($deliveredCount) }}</h3>
                            <h3 class="text">Entregues</h3>
                        </div>
                        <div class="data">
                            <h3 class="number">{{ TextHelper::formatNumber($canceledCount) }}</h3>
                            <h3 class="text">Cancelados</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="final-content">
                <p class="update">
                    Atualizado em {{ $now->format('d/m/Y') }} às {{ $now->format('H:i') }}
                </p>
            </div>
        </div>
        <!-- build ok -->
        <form method="POST" action="{{ route('campaigns.sectors.orders.deliver', [
            'campaign' => $campaign->slug,
            'sector' => $sector->id
        ]) }}" id="deliverOrdersForm">
            @csrf
            {{-- Conteúdo do form. --}}
            <div class="order-amount">
                {{-- Este input existe por enquanto para testarmos
                    o envio pelo robô da Uber Direct.
                    Value 1 = ativa o robô / Value 0 = envio normal --}}
                <input type="hidden" id="testing" name="testing" value=0>

                {{-- Campo que informa o cancelamento
                    de cannot_access_customer_location
                    Value 1 = cancela / Value 0 = envio normal --}}
                <input type="hidden" id="cannotAccess" name="cannotAccess" value=0>

                {{-- Campo que informa o cancelamento
                    de cannot_find_customer_address
                    Value 1 = cancela / Value 0 = envio normal --}}
                <input type="hidden" id="cannotFind" name="cannotFind" value=0>

                {{-- Campo que informa o cancelamento
                    de customer_rejected_order
                    Value 1 = cancela / Value 0 = envio normal --}}
                <input type="hidden" id="rejected" name="rejected" value=0>

                {{-- Campo que informa o cancelamento
                    de customer_unavailable
                    Value 1 = cancela / Value 0 = envio normal --}}
                <input type="hidden" id="unavailable" name="unavailable" value=0>

                {{-- Input que envia os pedidos sem data de pickup --}}
                <input type="hidden" id="noPickupDates" name="noPickupDates" value=0>

                {{-- Input que envia os pedidos sem data de dropoff --}}
                <input type="hidden" id="noDropoffDates" name="noDropoffDates" value=0>

                {{-- Input que informa quantos pedidos pendentes
                    existem no momento em que a página é acessada. --}}
                <input type="hidden" id="pendingQty"
                    name="pendingQty" value={{ $pendingCount }}>

                {{-- Input que informa a quantidade de
                    pedidos. Deve enviar pelo menos 1. --}}
                <div class="less disabled"></div>
                <input placeholder="00" id="qty" name="qty"/>
                <div class="more active"></div>
                <label class="sms">
                    <input type="checkbox" id="noMessage" name="noMessage" value="1">
                    Não enviar SMS
                </label>
            </div>
            <button type="submit">Enviar pedidos</button>
        </form>
    </div>
</section>