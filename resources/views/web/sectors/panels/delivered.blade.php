<div class="orders delivered">
    <div class="title-orders">
        <h2>Entregues</h2>
        <p>{{ TextHelper::formatNumber($deliveredCount) }} {{ TextHelper::pluralize($deliveredCount, 'pedido', 'pedidos') }}</p>
    </div>
    <div class="container-orders">
        <div class="intro-orders grid">
            <h3>Nome</h3>
            <h3>Endereço</h3>
            <h3>Status</h3>
            <h3>Data</h3>
        </div>
        @if ($deliveredCount > 0)
            @foreach ($deliveredOrders as $order)
                @php $delivery = $order->lastDelivery(); @endphp
                <div class="order-description grid">
                    <div class="content-container">
                        <p>
                            {{ $order->consumer }}
                            @if ($delivery !== null && $delivery->uber_uuid !== null)
                                (<a target="_blank" class="link-uber" href={{$delivery->tracking_url}}>{{$delivery->uberUuid()}}</a>)
                            @endif
                        </p>
                    </div>
                    <div class="content-container">
                        <p>{{ $order->fullAddress() }}</p>
                    </div>
                    <div class="content-container">
                        <span>Enviado</span>
                    </div>
                    <div class="content-container">
                        <span>{{ $order->created_at->format('d/m/Y') }}</span>
                    </div>
                </div>
            @endforeach
            @if ($deliveredCount > 10)
                <a href="{{ route('campaigns.sectors.orders.show.delivered', [
                    'campaign' => $campaign->slug,
                    'sector' => $sector->id,
                ]) }}">
                    Ver todos os entregues
                </a>
            @endif
        @else
            <p class="notFound">Não existem pedidos entregues</p>
        @endif
    </div>
</div>