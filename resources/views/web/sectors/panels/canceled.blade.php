<div class="orders canceled">
    <div class="title-orders">
        <h2>Cancelados</h2>
        <p>{{ TextHelper::formatNumber($canceledCount) }} {{ TextHelper::pluralize($canceledCount, 'pedido', 'pedidos') }}</p>
    </div>
    <div class="container-orders">
        <div class="intro-orders grid">
            <h3>Nome</h3>
            <h3>Endereço</h3>
            <h3>Status</h3>
            <h3>Data</h3>
        </div>
        @if ($canceledCount > 0)
            @foreach ($canceledOrders as $order)
                @php $delivery = $order->lastDelivery(); @endphp
                <div class="order-description grid">
                    <div class="content-container">
                        <p>
                            {{ $order->consumer }}
                            @if ($delivery !== null && $delivery->uber_uuid !== null)
                                (<a target="_blank" class="link-uber" href={{$delivery->tracking_url}}>{{$delivery->uberUuid()}}</a>)
                            @endif
                        </p>
                    </div>
                    <div class="content-container">
                        <p>{{ $order->fullAddress() }}</p>
                    </div>
                    <div class="content-container">
                        <span>Cancelado</span>
                        @if ($delivery !== null)
                            <p>{{ $delivery->reason }}</p>
                        @endif
                    </div>
                    <div class="content-container">
                        <span>{{ $order->created_at->format('d/m/Y') }}</span>
                    </div>
                </div>
            @endforeach
            @if ($canceledCount > 10)
                <a href="{{ route('campaigns.sectors.orders.show.canceled', [
                    'campaign' => $campaign->slug,
                    'sector' => $sector->id,
                ]) }}">
                    Ver todos os cancelados
                </a>
            @endif
        @else
            <p class="notFound">Não existem pedidos cancelados</p>
        @endif
    </div>
</div>