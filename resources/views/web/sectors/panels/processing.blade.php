@if ($processingCount > 0)
    <div class="orders delivery">
        <div class="introduction-data">
            <div class="menu-reload">
                <div class="title-orders">
                    <h3 id="reloadLink">Recarregar</h3>
                    <h2>Em andamento</h2>
                    <p>{{ TextHelper::formatNumber($processingCount) }} {{ TextHelper::pluralize($processingCount, 'pedido', 'pedidos') }}</p>
                    <p>{{ TextHelper::formatNumber($couriersCount) }} {{ TextHelper::pluralize($couriersCount, 'entrega', 'entregas') }}</p>
                </div>
                @php $completeDeliveriesClass = $canCompleteOrders ? 'active' : ''; @endphp
                <form method="POST" action="{{ route('deliveries.complete') }}"
                class="{{ $completeDeliveriesClass }}">
                    @csrf
                    <input type="hidden" id="sector" name="sector" value="{{ $sector->id }}"/>
                    <input type="hidden" id="campaign" name="campaign" value="{{ $campaign->id }}"/>
                    <button type="submit">Finalizar Entregas</button>
                </form>
                <div class="tabs">
                    <h3 class="active">Pedidos</h3>
                    <h3>Entregadores</h3>
                </div>
            </div>
        </div>
        <div class="container-orders order-table">
            <div class="intro-orders grid">
                <h3>Nome</h3>
                <h3>Endereço</h3>
                <h3>Status</h3>
            </div>
            @if ($processingOrders->isNotEmpty())
                @foreach ($processingOrders as $order)
                    @php
                        $delivery = $order->lastDelivery();
                        $tagClass = DeliveryStatusHelper::tagClass($delivery);
                    @endphp
                    <div class="order-description grid {{ $tagClass }}">
                        <div class="content-container">
                            <p>
                                {{ $order->consumer }}
                                @if ($delivery !== null && $delivery->uber_uuid !== null)
                                    (<a target="_blank" class="link-uber" href={{$delivery->tracking_url}}>{{$delivery->uberUuid()}}</a>)
                                @endif
                            </p>
                        </div>
                        <div class="content-container">
                            <p>{{ $order->fullAddress() }}</p>
                        </div>
                        @if ($order->isWaiting())
                            <div class="content-container waiting">
                                <span>{{ $order->status->toView() }}</span>
                            </div>
                        @endif
                        @if ($order->isProcessing())
                            @if ($delivery->canCancel() || $delivery->canReschedule())
                                <div class="content-container">
                                    <div class="position-container">
                                        <div class="tag-status">
                                            <span>{{ DeliveryStatusHelper::name($delivery) }}</span>
                                            <span>{{ DeliveryStatusHelper::notes($delivery) }}</span>
                                        </div>
                                    </div>
                                    @include('web.buttons.cancel-reschedule')
                                </div>
                            @else
                                <div class="content-container">
                                    <span>{{ DeliveryStatusHelper::name($delivery) }}</span>
                                    <span>{{ DeliveryStatusHelper::notes($delivery) }}</span>
                                </div>
                            @endif
                        @endif
                    </div>
                @endforeach
            @else
                <p class="notFound">Não existem pedidos em andamento</p>
            @endif
        </div>
        @include('web.generic.couriers')
        @if (isset($detail) === false || $detail === false)
            <a href="{{ route('campaigns.sectors.orders.show.processing', [
                'campaign' => $campaign->slug,
                'sector' => $sector->id,
            ]) }}">
                Ver todos em andamento
            </a>
        @endif
    </div>
@endif