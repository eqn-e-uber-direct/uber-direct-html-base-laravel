{{-- CÓDIGO PARA VOLTAR NO CAMINHO DE PAO --}}
@php
    $currentUrl = url()->current(); // Obtém a URL atual
    $currentUrlWithoutTrailingSlash = preg_replace('/\/[^\/]*$/', '', $currentUrl); // Remove a última barra e o conteúdo após ela
@endphp

<x-guest-layout>
    @section('title', $campaign->title . ' - ' . $sector->name . ' - ' . __('order_status.' . $status->value))
    <x-header></x-header>
    <main class="group-detail-inner" id="top">
        <section class="intro">
            <div class="title">
                <div class="path">
                    <a href="{{ route('campaigns.index') }}">Todas as campanhas</a>
                    <a href="{{ route('campaigns.show', $campaign->slug) }}">Pedidos - {{ $campaign->title }}</a>
                    <a href="{{ $currentUrlWithoutTrailingSlash }}">{{ $sector->name }}</a>
                    <p>{{ __('order_status.' . $status->value) }}</p>
                </div>
                <div class="intro">
                    <h2>{{ $sector->name }}</h2>
                    <p>CEPs com início entre {{ $sector->start }} e {{ $sector->end }}.</p>
                </div>
            </div>
        </section>
        <div class="orders delivery">
            @include('web.sectors.panels.processing', [
                'processingOrders' => collect($orders->items()),
                'processingCount' => $ordersCount,
                'detail' => true,
            ])
        </div>
        {{ $orders->appends(request()->query())->links('partials.my-paginate', ['paginator' => $orders]) }}
        <a class="up" href="#top"></a>
    </main>
    <x-footer></x-footer>
</x-guest-layout>
