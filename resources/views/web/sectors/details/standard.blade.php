{{-- CÓDIGO PARA VOLTAR NO CAMINHO DE PAO --}}
@php
    $currentUrl = url()->current(); // Obtém a URL atual
    $currentUrlWithoutTrailingSlash = preg_replace('/\/[^\/]*$/', '', $currentUrl); // Remove a última barra e o conteúdo após ela
@endphp

<x-guest-layout>
    @section('title', $campaign->title . ' - ' . $sector->name . ' - ' . __('order_status.' . $status->value))
    <x-header></x-header>
    <main class="group-detail-inner" id="top">
        <section class="intro">
            <div class="title">
                <div class="path">
                    <a href="{{ route('campaigns.index') }}">Todas as campanhas</a>
                    <a href="{{ route('campaigns.show', $campaign->slug) }}">Pedidos - {{ $campaign->title }}</a>
                    <a href="{{ $currentUrlWithoutTrailingSlash }}">{{ $sector->name }}</a>
                    <p>{{ __('order_status.' . $status->value) }}</p>
                </div>
                <div class="intro">
                    <h2>{{ $sector->name }}</h2>
                    <p>CEPs com início entre {{ $sector->start }} e {{ $sector->end }}.</p>
                </div>
            </div>
        </section>
        <div class="orders {{ $status->value }}">
            <div class="title-orders">
                <h2>{{ __('order_status.' . $status->value) }}s</h2>
                <p>{{ TextHelper::formatNumber($ordersCount) }} {{ TextHelper::pluralize($ordersCount, 'pedido', 'pedidos') }}</p>
            </div>
            <div class="container-orders">
                <div class="intro-orders grid">
                    <h3>Nome</h3>
                    <h3>Endereço</h3>
                    <h3>Status</h3>
                    <h3>Data</h3>
                </div>
                @if ($orders->isEmpty() === false)
                    @foreach ($orders as $order)
                        <div class="order-description grid">
                            <div class="content-container">
                                <p>{{ $order->consumer }}</p>
                            </div>
                            <div class="content-container">
                                <p>{{ $order->fullAddress() }}</p>
                            </div>
                            <div class="content-container">
                                <span>{{ __('order_status.' . $status->value) }}</span>
                                @if ($order->isCanceled())
                                    <p>{{ $order->lastDeliveryReason() }}</p>
                                @endif
                            </div>
                            <div class="content-container">
                                <span>{{ $order->created_at->format('d/m/Y') }}</span>
                            </div>
                        </div>
                    @endforeach
                @else
                    <p class="notFound">Nenhum pedido</p>
                @endif
            </div>
        </div>
        {{ $orders->appends(request()->query())->links('partials.my-paginate', ['paginator' => $orders]) }}
        <a class="up" href="#top"></a>
    </main>
    <x-footer></x-footer>
</x-guest-layout>