<x-guest-layout>
    @if ($errors->any())
        <div class="alert alert-error">
            @foreach($errors->all() as $error)
                <div>
                    <p>{{ $error }}</p>
                </div>
            @endforeach
        </div>
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            <div>
                <p>{{ session('success') }}</p>
            </div>
        </div>
    @endif
    @section('title', $campaign->title . ' - ' . $sector->name)
    <x-header></x-header>
    <main class="group-detail group-detail-inner" id="top">
        @include('web.sectors.panels.intro')
        @include('web.sectors.panels.processing')
        @include('web.sectors.panels.pending')
        @include('web.sectors.panels.delivered')
        @include('web.sectors.panels.canceled')
        <a class="up" href="#top"></a>
    </main>
    <x-footer></x-footer>
</x-guest-layout>
