@if ($couriers ?? null)
    <div class="container-orders delivery-table">
        <div class="intro-orders grid">
            <h3>Entregador</h3>
            <h3>Veículo</h3>
            <h3>Quantidade</h3>
        </div>
        @if ($couriers->isNotEmpty())
            @foreach ($couriers as $group)
                @php $courier = $group->get('courier'); @endphp
                <div class="order-description grid">
                    <div class="content-container">
                        <p>{{ $courier->name }}</p>
                    </div>
                    <div class="content-container">
                        <p>{{ $courier->viewVehicle() }}</p>
                    </div>
                    <div class="content-container">
                        <p>{{ $group->get('deliveries') }}</p>
                    </div>
                </div>
            @endforeach
        @else
            <p class="notFound">Sem entregadores</p>
        @endif
    </div>
@endif

@if ($batches ?? null)
    <div class="container-orders delivery-table">
        @if ($batches->isNotEmpty())
            @foreach ($batches as $batchId => $batch)
                @php $batchCount = $batch->count(); @endphp
                <div class="order-description grid">
                    <div class="content-container">
                        <p>Não definido<br>(Batch ID {{ $batchId }})</p>
                    </div>
                    <div class="content-container">
                        <p>Aguardando definição</p>
                    </div>
                    <div class="content-container">
                        <p>{{ $batchCount }}</p>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
@endif
