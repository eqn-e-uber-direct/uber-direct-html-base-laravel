<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="msapplication-TileImage" content="{{ asset('favIcons/favicon.ico') }}" />
    <link rel="icon" type="image/x-icon" href="{{ asset('favIcons/favicon.ico') }}">
    <!-- Ícones para diferentes dispositivos e dimensões -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favIcons/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favIcons/favicon-16x16.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favIcons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('favIcons/favicon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="512x512" href="{{ asset('favIcons/favicon-512x512.png') }}">
    <!-- Arquivo de manifesto para PWA -->
    <link rel="manifest" href="{{ asset('favIcons/site.webmanifest') }}">
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>EQN - Uber | @yield('title')</title>
</head>

<body class="font-sans text-gray-900 antialiased">
    {{ $slot }}
</body>

</html>
