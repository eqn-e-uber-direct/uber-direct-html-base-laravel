<x-guest-layout>
    @section('title', 'Login')
    <!-- Session Status -->
    <x-auth-session-status class="mb-4" :status="session('status')" />

    <x-header-login></x-header-login>
    <main class="login">

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <!-- Email Address -->
            <div class="containerForm">
                <label for="email">Seu email</label>
                <input autocomplete="username" type="email"
                    class="{{ $errors->has('email') ? 'wrongInfo loginError' : '' }}"
                    placeholder="Digite seu e-mail cadastrado" id="email" name="email" :value="old('email')"
                    required />
            </div>

            <!-- Password -->
            <div class="containerForm">
                <label for="password">Sua senha</label>
                <div class="password-eye">
                    <input type="password" placeholder="Digite sua senha" id="password" name="password" required
                        autocomplete="current-password"
                        class="{{ $errors->has('email') || $errors->has('password') ? 'wrongInfo loginError' : '' }}" />
                    <span class="eye"></span>
                </div>
                <div class="wrong-field {{ $errors->has('email') || $errors->has('password') ? 'active' : '' }}">
                    @if ($errors->has('password'))
                        <p class="alert alert-danger">
                            {{ trans('auth.password') }}
                        </p>
                    @endif
                    @if ($errors->has('email'))
                        <p class="alert alert-danger">
                            {{ trans('auth.failed') }}
                        </p>
                    @endif
                </div>
            </div>

            <!-- Remember Me -->
            {{-- <div class="block mt-4">
            <label for="remember_me" class="inline-flex items-center">
                <input id="remember_me" type="checkbox"
                    class="rounded border-gray-300 text-indigo-600 shadow-sm focus:ring-indigo-500" name="remember">
                <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
            </label>
        </div> --}}

            {{-- <div class="flex items-center justify-end mt-4">
            @if (Route::has('password.request'))
                <a class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                    href="{{ route('password.request') }}">
                    {{ __('Forgot your password?') }}
                </a>
            @endif --}}
            {{-- </div> --}}
            <button class="enter" type="submit">Entrar</button>
        </form>
</x-guest-layout>
