@if ($paginator->hasPages())
    <div class="paginate">
        <div class="paginator">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <div class="disabled less" aria-disabled="true" aria-label="@lang('pagination.previous')"></div>
            @else
                <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">
                    <div class="less active"></div>
                </a>
            @endif

            <div class="pages">
                {{-- Pagination Elements --}}
                @if ($paginator->currentPage() <= 1)
                    @for ($i = 1; $i <= min(3, $paginator->lastPage()); $i++)
                        @if ($i == $paginator->currentPage())
                            <p class="active" aria-current="page">{{ $i }}</p>
                        @else
                            <a href="{{ $paginator->url($i) }}">
                                <p>{{ $i }}</p>
                            </a>
                        @endif
                    @endfor
                @else
                    @for ($i = $paginator->currentPage() - 1; $i <= min($paginator->currentPage() + 1, $paginator->lastPage()); $i++)
                        @if ($i == $paginator->currentPage())
                            <p class="active" aria-current="page">{{ $i }}</p>
                        @else
                            <a href="{{ $paginator->url($i) }}">
                                <p>{{ $i }}</p>
                            </a>
                        @endif
                    @endfor
                @endif
            </div>

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">
                    <div class="more active"></div>
                </a>
            @else
                <div aria-hidden="true" aria-disabled="true" aria-label="@lang('pagination.next')" class="more disabled"></div>
            @endif
        </div>
    </div>
@endif
