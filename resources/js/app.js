import.meta.glob(["../img/**", "../fonts/**"]);
import $ from "jquery";

document.addEventListener("DOMContentLoaded", function() {
    var reloadLink = document.querySelector(".orders.delivery #reloadLink");

    if(reloadLink){
        reloadLink.addEventListener("click", function(event) {
            event.preventDefault();

            // Pega a URL atual
            var currentUrl = window.location.href;

            // Recarrega a página
            window.location.href = currentUrl;
        });
    }
});

$(document).ready(function () {
    $(".eye").click(function () {
        $("body").toggleClass("eyesOff");

        const bodyClass = $("body").hasClass("eyesOff");
        const passwordInput = $("#password");

        if (bodyClass) {
            passwordInput.attr("type", "text");
        } else {
            passwordInput.attr("type", "password");
        }
    });

    // GROUP-DETAIL
    var input = $(".order-amount input");
    var lessButton = $(".order-amount .less");
    var moreButton = $(".order-amount .more");

    lessButton.on("click", function () {
        var value = parseInt(input.val()) || 0; // Garante que o valor seja sempre um número ou zero
        if (value > 0) {
            value--;
            input.val(value);
            if (value === 0) {
                lessButton.addClass("disabled");
            }
        }
    });

    moreButton.on("click", function () {
        var value = parseInt(input.val()) || 0; // Garante que o valor seja sempre um número ou zero
        value++;
        input.val(value);
        lessButton.removeClass("disabled");
    });

    // GROUP-LOCAL

    $(".tabs h3").click(function () {
        if (!$(this).hasClass("active")) {
            $("body").toggleClass("delivery");
            // Adiciona a classe "active" à tab clicada
            $(this).addClass("active");
            // Remove a classe "active" de todas as outras tabs dentro de .tabs
            $(".tabs h3").not(this).removeClass("active");
        }
    });

    $(".up").click(function (e) {
        e.preventDefault();

        var target = $(this.hash);

        $("html").animate(
            {
                scrollTop: target.offset().top - 120,
            },
            800
        );
    });

    var button = $(".up");

    function checkScroll() {
        var screenHeight = $(window).height();

        if ($(window).scrollTop() > screenHeight) {
            button.addClass("active");
        } else {
            button.removeClass("active");
        }
    }

    // Adicionar um evento de rolagem à janela
    $(window).scroll(checkScroll);

    // Janela de alerta para o form de entregar pedidos pendentes.
    $('form#deliverOrdersForm').submit(function (event) {
        var pendingQty = parseInt($('#pendingQty').val());
        var qty = parseInt($('#qty').val());

        var sentLessThanTwenty = qty < 20;

        var sentHigherThanPending = qty > pendingQty;

        var qtyMessage = qty == 1
            ? '1 pedido'
            : qty + ' pedidos';

        var pendingMessage = pendingQty == 1
            ? '1 pendente'
            : pendingQty + ' pendentes';

        if (sentLessThanTwenty) {
            var message = 'Você está enviando apenas ' + qtyMessage + '. Recomendamos que envie pelo menos 20 pedidos. Quer enviar mesmo assim?';
        }

        if (sentHigherThanPending) {
            var message = 'Você quer enviar ' + qtyMessage + ', mas possui apenas ' + pendingMessage + ' neste setor. O processo será iniciado com a quantidade disponível. OK?';
        }

        if (sentLessThanTwenty && sentHigherThanPending) {
            var message = 'Você está enviando apenas ' + qtyMessage + ', o que está abaixo do recomendado. Você também está tentando abrir uma janela com mais pedidos do que estão disponíveis. O processo será iniciado com a quantidade disponível. OK?';
        }

        var confirmed = confirm(message);

        if (!confirmed) {
            event.preventDefault();
        }
    });
});
