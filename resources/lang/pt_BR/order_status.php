<?php

use App\Enums\OrderStatus;

return [
   OrderStatus::PENDING->value => 'Pendente',
   OrderStatus::PROCESSING->value => 'Andamento',
   OrderStatus::DELIVERED->value => 'Entregue',
   OrderStatus::CANCELED->value => 'Cancelado',
];