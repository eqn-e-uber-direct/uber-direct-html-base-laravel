<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Essas credenciais não foram encontradas em nossos registros.',
    'password' => 'A senha digitada não parece estar correta. Verifique a senha informada e
    tente novamente.',
    'throttle' => 'Muitas tentativas de login. Tente novamente em :seconds segundos.',

];
