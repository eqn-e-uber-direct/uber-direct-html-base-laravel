<?php

use App\Http\Controllers\CancelDeliveryController;
use App\Http\Controllers\CompleteDeliveriesController;
use App\Http\Controllers\DeliverOrdersController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShowCampaignsController;
use App\Http\Controllers\DetailCanceledOrdersController;
use App\Http\Controllers\DetailDeliveredOrdersController;
use App\Http\Controllers\DetailPendingOrdersController;
use App\Http\Controllers\DetailProcessingOrdersController;
use App\Http\Controllers\RescheduleDeliveryController;
use App\Http\Controllers\Settings\ShowConfigSettingsController;
use App\Http\Controllers\ShowOrdersController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware('auth')->group(function () {

    // Home.
    Route::get("/", ShowCampaignsController::class)
        ->name('campaigns.index');

    // Configurações.
    Route::get('/settings/configs', ShowConfigSettingsController::class)
        ->name('settings.configs');

    // Campanhas.
    Route::prefix('campanhas')
        ->name('campaigns')
        ->group(function () {

            // Estas rotas foram desativadas por não serem mais necessárias,
            // entretanto, estão ainda comentadas para caso o cliente decida
            // continuar usando.

            // // Detalhe da campanha.
            // Route::get('{campaign:slug}', DetailCampaignsController::class)
            //     ->name('.show');

            // // Setores.
            // Route::prefix('{campaign:slug}/setor/{sector}')
            //     ->name('.sectors.orders')
            //     ->group(function () {

            //         // Lista de pedidos.
            //         Route::get('pedidos', ShowSectorOrdersController::class)
            //             ->name('.index');

            //         // Entrega os pedidos de acordo com a quantidade informada.
            //         Route::post('pedidos/entregar', DeliverSectorOrdersController::class)
            //             ->name('.deliver');

            //         // Lista os pedidos em processo.
            //         Route::get('pedidos/andamento', DetailSectorProcessingOrdersController::class)
            //             ->name('.show.processing');

            //         // Lista os pedidos pendentes.
            //         Route::get('pedidos/pendentes', DetailSectorPendingOrdersController::class)
            //             ->name('.show.pending');

            //         // Lista os pedidos entregues.
            //         Route::get('pedidos/entregues', DetailSectorDeliveredOrdersController::class)
            //             ->name('.show.delivered');

            //         // Lista os pedidos cancelados.
            //         Route::get('pedidos/cancelados', DetailSectorCanceledOrdersController::class)
            //             ->name('.show.canceled');
            // });

            // Pedidos
            Route::prefix('{campaign:slug}/pedidos')
                ->name('.orders')
                ->group(function() {

                    // Lista os pedidos.
                    Route::get('', ShowOrdersController::class)
                        ->name('.index');

                    // Entrega os pedidos de acordo com a quantidade informada.
                    Route::post('entregar', DeliverOrdersController::class)
                        ->name('.deliver');

                    // Lista os pedidos em processo.
                    Route::get('andamento', DetailProcessingOrdersController::class)
                        ->name('.show.processing');

                    // Lista os pedidos pendentes.
                    Route::get('pendentes', DetailPendingOrdersController::class)
                        ->name('.show.pending');

                    // Lista os pedidos entregues.
                    Route::get('entregues', DetailDeliveredOrdersController::class)
                        ->name('.show.delivered');

                    // Lista os pedidos cancelados.
                    Route::get('cancelados', DetailCanceledOrdersController::class)
                        ->name('.show.canceled');

                });
    });

    // Entregas
    Route::prefix('entregas')
        ->name('deliveries')
        ->group(function () {

            // Marca uma entrega para cancelamento.
            Route::post('{delivery}/cancelar', CancelDeliveryController::class)
                ->name('.cancel');

            // Marca uma entrega para reagendamento.
            Route::post('{delivery}/reagendar', RescheduleDeliveryController::class)
                ->name('.reschedule');

            // Conclui todas as entregas conforme seu status.
            Route::post('finalizar', CompleteDeliveriesController::class)
                ->name('.complete');
        });
});

// Route::get('/test', function (\Illuminate\Http\Request $request) {
//     // dd(\App\Actions\Sector\FindSectorFromCep::run($request->get('cep')));
//     $testingTraits = new \App\Supports\TestingTraits();
// });

require __DIR__.'/auth.php';
