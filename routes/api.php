<?php

use App\Http\Controllers\Api\AuthenticateUserController;
use App\Http\Controllers\Api\CancelOrdersController;
use App\Http\Controllers\Api\UpdateCampaignController;
use App\Http\Controllers\Api\Uber\UpdateDeliveryController;
use App\Http\Controllers\Api\UpdateOrderController;
use App\Http\Controllers\Api\UpdateProductController;
use App\Http\Controllers\Api\VerifyUserController;
use App\Http\Controllers\Api\CheckAddressController;
use App\Http\Controllers\Api\Dev\ForceUpdateDeliveriesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('login', AuthenticateUserController::class);

Route::post('deliveries', UpdateDeliveryController::class)
    ->withoutMiddleware(['throttle:api']);

Route::middleware('auth:sanctum')->group(function () {

    Route::post('addresses/check', CheckAddressController::class)
        ->withoutMiddleware(['throttle:api']);

    Route::post('campaigns', UpdateCampaignController::class)
        ->withoutMiddleware(['throttle:api']);

    Route::post('products', UpdateProductController::class)
        ->withoutMiddleware(['throttle:api']);

    Route::post('orders', UpdateOrderController::class)
        ->withoutMiddleware(['throttle:api']);

    Route::post('orders/cancel', CancelOrdersController::class)
        ->withoutMiddleware(['throttle:api']);

    Route::get('verify', VerifyUserController::class)
        ->withoutMiddleware(['throttle:api']);

    // Dev routes.
    Route::post('dev/deliveries/update', ForceUpdateDeliveriesController::class)
        ->withoutMiddleware(['throttle:api']);

    // Route::get('dev/test', function () {
    //     dd("test code here");
    // });

});
