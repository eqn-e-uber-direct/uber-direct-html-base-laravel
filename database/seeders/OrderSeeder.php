<?php

namespace Database\Seeders;

use App\Models\Campaign;
use App\Enums\OrderStatus;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Actions\Sector\FindSectorFromCep;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::transaction(function () {
            try {
                $campaign = Campaign::first();

                $campaignSlug = $this->command->ask(
                    'Informe o slug da campanha desse pedido (o slug padrão é a primeira campanha no sistema)',
                    $campaign->slug
                );

                if ($campaignSlug !== $campaign->slug) {
                    $campaign = Campaign::where('slug', $campaignSlug)->first();
                }

                if (!$campaign) {
                    throw new \Exception(
                        'Não existe campanha com o slug informado ('.$campaignSlug.')'
                    );
                }

                $orderCep = $this->command->ask(
                    'Informe o CEP do endereço do pedido'
                );

                if (is_null($orderCep)) {
                    throw new \Exception('Você não informou o CEP do endereço');
                }

                $sector = FindSectorFromCep::run((string)$orderCep);

                if (!$sector) {
                    throw new \Exception(
                        'Este CEP ('.$orderCep.') não pertence aos grupos no sistema'
                    );
                }

                $orderAddress = $this->command->ask(
                    'Informe o endereço do pedido'
                );

                if (is_null($orderAddress)) {
                    throw new \Exception('Você não informou o endereço');
                }

                $orderConsumer = $this->command->ask(
                    'Informe o nome do consumidor do pedido',
                    "Fulano de tal"
                );

                $orderStatus = $this->command->ask(
                    'Informe o status do pedido (digite um valor entre "pending", "processing", "delivered", "canceled")',
                    OrderStatus::PENDING->value
                );

                if (!in_array($orderStatus, OrderStatus::all()->pluck('value')->toArray())) {
                    throw new \Exception(
                        'Não existe esse status informado ('.$campaignSlug.')'
                    );
                }

                $order = $campaign->orders()->create([
                    'status' => $orderStatus,
                    'cep' => $orderCep,
                    'address' => $orderAddress,
                    'consumer' => $orderConsumer,
                    'sector_id' => $sector->id,
                ]);

                $this->command->info(
                    'Pedido ('.$order->id.') para o CEP "'.$order->cep.'" e endereço "'.$order->address.'"criado com sucesso'
                );
            }
            catch (\Exception $e) {
                $this->command->error($e->getMessage());
                DB::rollBack();
                return;
            }
        });
    }
}
