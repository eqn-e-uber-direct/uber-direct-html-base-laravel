<?php

namespace Database\Seeders;

use App\Models\Campaign;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CampaignSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::transaction(function () {
            $title = $this->command->ask('Informe o título do campanha');

            $slug = $this->command->ask('Informe um slug para esta campanha');

            $color = $this->command->ask('Informe o código hexadecimal para a cor da campanha');

            $startAt = $this->command->ask(
                'Informe a data de inicio do campanha no formato d/m/Y H:i',
                now()->format('d/m/Y H:i')
            );

            $endAt = $this->command->ask(
                'Informe a data de término do campanha no formato d/m/Y H:i',
                now()->addDays(30)->format('d/m/Y H:i')
            );

            $campaign = Campaign::create([
                'title' => $title,
                'slug' => $slug,
                'color' => $color,
                'start_at' => $startAt,
                'end_at' => $endAt,
            ]);

            $this->command->info(
                'Campanha "'.$campaign->title.'" ('.$campaign->id.') criada com sucesso'
            );
        });
    }
}
