<?php

namespace Database\Seeders;

use App\Models\Sector;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class StartingSectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::transaction(function () {

            // Deleta todos que existem no banco.
            Sector::all()->each(fn ($sector) => $sector->forceDelete());

            // Grupo 1
            Sector::create([
                'name' => 'Centro - Centro Histórico e Bom Retiro',
                'start' => '01000',
                'end' => '01199',
                'places' => json_encode([
                    'Sé',
                    'Santa Efigênia',
                    'República',
                    'Centro',
                    'Barra Funda',
                    'Bom Retiro',
                    'Luz',
                    'Ponte Pequena',
                ]),
            ]);

            // Grupo 2
            Sector::create([
                'name' => 'Centro - Vila Buarque e Consolação',
                'start' => '01200',
                'end' => '01399',
                'places' => json_encode([
                    'Santa Cecília',
                    'Pacaembú',
                    'Sumaré',
                    'Higienópolis',
                    'Consolação',
                    'Bela Vista',
                ]),
            ]);

            // Grupo 3
            Sector::create([
                'name' => 'Centro - Jardins e Liberdade',
                'start' => '01400',
                'end' => '01599',
                'places' => json_encode([
                    'Cerqueira César',
                    'Jardim Paulista',
                    'Jardim América',
                    'Jardim Europa',
                    'Liberdade',
                    'Cambuci',
                    'Aclimação',
                    'Vila Monumento',
                    'Jardim da Glória',
                ]),
            ]);

            // Grupo 4
            Sector::create([
                'name' => 'Zona Norte - Santana e Tucuruvi',
                'start' => '02000',
                'end' => '02299',
                'places' => json_encode([
                    'Santana',
                    'Carandiru',
                    'Vila Guilherme',
                    'Jardim São Paulo',
                    'Vila Maria',
                    'Parque Novo Mundo',
                    'Jardim Japão',
                    'Tucuruvi',
                    'Jaçanã',
                    'Parque Edu Chaves',
                    'Vila Medeiros',
                    'Vila Edi',
                ]),
            ]);

            // Grupo 5
            Sector::create([
                'name' => 'Zona Norte - Tremembé e Mandaqui',
                'start' => '02300',
                'end' => '02499',
                'places' => json_encode([
                    'Jardim Tremembé',
                    'Barro Branco',
                    'Água Fria',
                    'Mandaqui',
                    'Imirim',
                    'Lausane Paulista',
                    'Santa Teresinha',
                ]),
            ]);

            // Grupo 6
            Sector::create([
                'name' => 'Zona Norte - Casa Verde e Cachoeirinha',
                'start' => '02500',
                'end' => '02699',
                'places' => json_encode([
                    'Casa Verde',
                    'Parque Peruche',
                    'Vila Nova Cachoeirinha',
                    'Jardim Peri Peri',
                ]),
            ]);

            // Grupo 7
            Sector::create([
                'name' => 'Zona Norte - Limão, Brasilândia e Freguesia do Ó',
                'start' => '02700',
                'end' => '02999',
                'places' => json_encode([
                    'Limão',
                    'Nossa Senhora do Ó',
                    'Itaberaba',
                    'Brasilândia',
                    'Morro Grande',
                    'Freguesia do Ó',
                    'Pirituba',
                    'Piqueri',
                ]),
            ]);

            // Grupo 8
            Sector::create([
                'name' => 'Zona Leste - Brás, Moóca',
                'start' => '03000',
                'end' => '03199',
                'places' => json_encode([
                    'Belenzinho',
                    'Brás',
                    'Belém',
                    'Pari',
                    'Canindé',
                    'Catumbi',
                    'Parque S.Jorge',
                    'Moóca',
                    'Alto da Moóca',
                    'Vila Prudente',
                    'A. Rosa',
                    'Quarta Parada',
                    'Parque Moóca',
                    'Vila Zelina',
                ]),
            ]);

            // Grupo 9
            Sector::create([
                'name' => 'Zona Leste - São Lucas, Anália Franco',
                'start' => '03200',
                'end' => '03399',
                'places' => json_encode([
                    'Vila Ema',
                    'Parque São Lucas',
                    'Vila Alpina',
                    'Sapopemba',
                    'Tatuapé',
                    'Vila Formosa',
                    'Jardim Colorado',
                    'Vila Gomes Cardim',
                    'Jardim Anália Franco',
                ]),
            ]);

            // Grupo 10
            Sector::create([
                'name' => 'Zona Leste - Carrão, Aricanduva e Vila Matilde',
                'start' => '03400',
                'end' => '03599',
                'places' => json_encode([
                    'Vila Carrão',
                    'Carrãozinho',
                    'Vila Matilde',
                    'Cidade Patriarca',
                    'Artur Alvim',
                ]),
            ]);

            // Grupo 11
            Sector::create([
                'name' => 'Zona Leste - Penha e Gangaíba',
                'start' => '03600',
                'end' => '03799',
                'places' => json_encode([
                    'Penha',
                    'Vila Esperança',
                    'Vila Ré',
                    'Cidade A. E. Carvalho',
                    'Cangaiba',
                    'Engenho Goulart',
                    'Ponte Rasa',
                ]),
            ]);

            // Grupo 12
            Sector::create([
                'name' => 'Zona Leste - Ermelino Matarazzo e São Mateus',
                'start' => '03800',
                'end' => '03999',
                'places' => json_encode([
                    'Ermelino Matarazzo',
                    'Vila Paranaguá',
                    'São Mateus',
                    'Iguaçu',
                ]),
            ]);

            // Grupo 13
            Sector::create([
                'name' => 'Zona Leste - São Miguel Paulista',
                'start' => '08000',
                'end' => '08499',
                'places' => json_encode([
                    'São Miguel Paulista',
                    'Itaim Paulista',
                    'Itaquera',
                    'São Mateus',
                    'Guaianazes',
                ]),
            ]);

            // Grupo 14
            Sector::create([
                'name' => 'Zona Sul - Moema e Vila Mariana (Oeste)',
                'start' => '04000',
                'end' => '04099',
                'places' => json_encode([
                    'Vila Mariana',
                    'Vila Clementino',
                    'Paraíso',
                    'Indianópolis',
                    'Moema',
                    'Planalto Paulista',
                    'Mirandópolis',
                ]),
            ]);

            // Grupo 15
            Sector::create([
                'name' => 'Zona Sul - Saúde e Vila Mariana (Leste)',
                'start' => '04100',
                'end' => '04199',
                'places' => json_encode([
                    'Jardim Glória',
                    'Saúde',
                    'Água Funda',
                    'Vila Mercês',
                    'Vila Liviero',
                ]),
            ]);

            // Grupo 16
            Sector::create([
                'name' => 'Zona Sul - Ipiranga e Jabaquara',
                'start' => '04200',
                'end' => '04399',
                'places' => json_encode([
                    'Ipiranga',
                    'Vila Carioca',
                    'Sacomã',
                    'Moinho Velho',
                    'São João Clímaco',
                    'Jabaquara',
                    'Jardim Aeroporto',
                    'Vila Sta. Catarina',
                    'Vila Guarani',
                    'Vila Mascote',
                ]),
            ]);

            // Grupo 17
            Sector::create([
                'name' => 'Zona Sul - Cidade Ademar',
                'start' => '04400',
                'end' => '04499',
                'places' => json_encode([
                    'Cidade Ademar',
                    'Pedreira',
                    'Jardim Miriam',
                    'Americanópolis',
                ]),
            ]);

            // Grupo 18
            Sector::create([
                'name' => 'Zona Sul - Itaim Bibi e Campo Belo',
                'start' => '04500',
                'end' => '04699',
                'places' => json_encode([
                    'Brooklin Novo',
                    'Itaim Bibi',
                    'Vila Olímpia',
                    'Moema',
                    'Vila Nova Conceição',
                    'Brooklin Paulista',
                    'Campo Belo',
                    'Aeroporto',
                    'Cidade Ademar',
                    'Campo Grande',
                    'Jurubatuba',
                    'Chácara Flora',
                    'Chácara Monte Alegre',
                ]),
            ]);

            // Grupo 19
            Sector::create([
                'name' => 'Zona Sul - Santo Amaro',
                'start' => '04700',
                'end' => '04799',
                'places' => json_encode([
                    'Santo Amaro',
                    'Chácara Santo Antônio',
                    'Granja Julieta',
                    'Socorro',
                    'Veleiros',
                    'Interlagos',
                    'Alto da Boa Vista',
                ]),
            ]);

            // Grupo 20
            Sector::create([
                'name' => 'Zona Sul - Cidade Dutra, Grajaú e Guarapiranga',
                'start' => '04800',
                'end' => '04999',
                'places' => json_encode([
                    'Cidade Dutra',
                    'Rio Bonito',
                    'Parque Grajaú',
                    'Parelheiros',
                    'Guarapiranga',
                    'Capela do Socorro',
                ]),
            ]);

            // Grupo 21
            Sector::create([
                'name' => 'Zona Oeste - Lapa e Perdizes',
                'start' => '05000',
                'end' => '05099',
                'places' => json_encode([
                    'Lapa',
                    'Perdizes',
                    'Água Branca',
                    'Alto da Lapa',
                    'Vila Anastácia',
                    'Pompéia',
                    'Vila Romana',
                ]),
            ]);

            // Grupo 22
            Sector::create([
                'name' => 'Zona Oeste - Pirituba, Jaraguá e Perus',
                'start' => '05100',
                'end' => '05299',
                'places' => json_encode([
                    'Vila Jaguará',
                    'Pirituba',
                    'Parque São Domingos',
                    'Jaraguá',
                    'Perus',
                ]),
            ]);

            // Grupo 23
            Sector::create([
                'name' => 'Zona Oeste - Jaguaré, Leopoldina e Pinheiros',
                'start' => '05300',
                'end' => '05599',
                'places' => json_encode([
                    'Vila Leopoldina',
                    'Ceasa',
                    'Jaguaré',
                    'Rio Pequeno',
                    'Vila Hamburguesa',
                    'Vila Remédios',
                    'Pinheiros',
                    'Vila Madalena',
                    'Alto de Pinheiros',
                    'Butantã',
                    'Caxingui',
                    'Cidade Universitária (USP)',
                    'Jardim Peri-Peri',
                    'Jardim Bonfiglioli'
                ]),
            ]);

            // Grupo 24
            Sector::create([
                'name' => 'Zona Oeste - Morumbi, Campo Limpo e Capão Redondo',
                'start' => '05600',
                'end' => '05899',
                'places' => json_encode([
                    'Campo Limpo',
                    'Pirajuçara',
                    'Capão Redondo',
                    'Vila Das Belezas',
                    'Cidade Jardim',
                    'Morumbi',
                    'Vila Sônia',
                    'Jardim Guedala',
                    'Jardim Leonor',
                    'Real Parque',
                ]),
            ]);

        });
    }
}
