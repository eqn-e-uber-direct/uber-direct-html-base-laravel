<?php

namespace Database\Seeders;

use App\Enums\OrderStatus;
use App\Models\Delivery;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class LinkCourierOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::transaction(function () {

            $orderId = $this->command->ask('Informe o ID do pedido');

            $courierId = $this->command->ask('Informe o ID do entregador');

            $oldDelivery = Delivery::where('courier_id', $courierId)
                ->where('order_id', $orderId)
                ->get();

            $oldDelivery->each(fn ($model) => $model->delete());

            $delivery = Delivery::create([
                'order_id' => $orderId,
                'courier_id' => $courierId
            ]);

            if ($delivery) {
                $delivery->order->update(['status' => OrderStatus::PROCESSING]);
            }

            $this->command->info(
                'Entregador ('.$courierId.') vinculado com o pedido ('.$orderId.') com sucesso.'
            );
        });
    }
}
