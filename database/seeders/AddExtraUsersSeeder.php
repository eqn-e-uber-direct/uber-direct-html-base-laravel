<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class AddExtraUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::transaction(function () {

            $user = User::where('email', 'vinicius.aurelio@paginaviva.com.br')
                ->first();

            if ($user === null) {
                User::create([
                    'name' => 'Vinicius Aurelio',
                    'email' => 'vinicius.aurelio@paginaviva.com.br',
                    'password' => Hash::make('OLSi5k!q=Rn6'),
                    'email_verified_at' => now(),
                ]);
            }

            $user = User::where('email', 'ademar.m@paginaviva.com.br')
                ->first();

            if ($user === null) {
                User::create([
                    'name' => 'Ademar',
                    'email' => 'ademar.m@paginaviva.com.br',
                    'password' => Hash::make('oO*T0VTc+1CH'),
                    'email_verified_at' => now(),
                ]);
            }
        });
    }
}
