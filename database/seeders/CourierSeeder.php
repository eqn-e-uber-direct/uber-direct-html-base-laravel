<?php

namespace Database\Seeders;

use App\Models\Courier;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CourierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::transaction(function () {

            $name = $this->command->ask('Informe o nome do entregador');

            $plate = $this->command->ask('Informe a placa do entregador');

            $courier = Courier::create([
                'name' => $name,
                'plate' => $plate
            ]);

            $this->command->info('Entregador ('.$courier->id.') criado com sucesso.');

        });
    }
}
