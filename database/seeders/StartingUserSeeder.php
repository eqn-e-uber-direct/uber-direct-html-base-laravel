<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class StartingUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::transaction(function () {

            User::create([
                'name' => 'Admin',
                'email' => 'admin@meeg.app',
                'password' => Hash::make('mist3r-M33G'),
                'email_verified_at' => now(),
            ]);

        });
    }
}
