<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('uber_direct_connections', function (Blueprint $table) {
            $table->timestamp('expires_at')
                ->after('id')
                ->nullable()
                ->comment("Token expiration date from Uber Direct request");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('uber_direct_connections', function (Blueprint $table) {
            $table->dropColumn('expires_at');
        });
    }
};
