<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();

            $table->string('name')
                ->nullable();

            $table->string('sku')
                ->nullable();

            $table->integer('weight')
                ->default(1)
                ->comment('Weight in grams');

            $table->integer('length')
                ->default(1)
                ->comment('Length in centimeters');

            $table->integer('height')
                ->default(1)
                ->comment('Height in centimeters');

            $table->integer('depth')
                ->default(1)
                ->comment('Depth in centimeters');

            $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
