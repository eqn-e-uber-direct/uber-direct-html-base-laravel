<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('sectors', function (Blueprint $table) {
            $table->dropColumn('ceps');

            $table->string('start')
                ->after('name')
                ->nullable()
                ->comment("Starting zipcode of the sector");

            $table->string('end')
                ->after('start')
                ->nullable()
                ->comment('Ending zipcode of the sector');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('sectors', function (Blueprint $table) {
            $table->json('ceps')
                ->after('name')
                ->nullable()
                ->comment("Array list of CEPs");

            $table->dropColumn(['start', 'end']);
        });
    }
};
