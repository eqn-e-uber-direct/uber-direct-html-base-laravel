<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('deliveries', function (Blueprint $table) {
            $table->boolean('problem')
                ->after('reason')
                ->default(false)
                ->comment("Show if there was a problem with the delivery from Uber Direct.");

            $table->boolean('cancel_order')
                ->after('problem')
                ->default(false)
                ->comment("Show if the order was canceled by the user or not.");

            $table->boolean('reschedule_order')
                ->after('cancel_order')
                ->default(false)
                ->comment("Show if the order was rescheduled by the user or not.");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('deliveries', function (Blueprint $table) {
            $table->dropColumn('problem', 'cancel_order', 'reschedule_order');
        });
    }
};
