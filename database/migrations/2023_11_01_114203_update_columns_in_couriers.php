<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('couriers', function (Blueprint $table) {
            $table->string('phone_number')
                ->after('id')
                ->nullable()
                ->comment("The courier`s phone number is unique. Format is string^\+[0-9]+$");

            $table->string('vehicle_type')
                ->after('name')
                ->nullable()
                ->comment("Type of vehicle the courier is using. This data is from Uber Direct.");

            $table->string('vehicle_make')
                ->after('vehicle_type')
                ->nullable()
                ->comment("Make of vehicle the courier is using. This data is from Uber Direct.");

            $table->string('vehicle_model')
                ->after('vehicle_make')
                ->nullable()
                ->comment("Model of vehicle the courier is using. This data is from Uber Direct.");

            $table->string('vehicle_color')
                ->after('vehicle_model')
                ->nullable()
                ->comment("Color of vehicle the courier is using. This data is from Uber Direct.");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('couriers', function (Blueprint $table) {
            $table->dropColumn([
                'phone_number',
                'vehicle_type',
                'vehicle_make',
                'vehicle_model',
                'vehicle_color',
            ]);
        });
    }
};
