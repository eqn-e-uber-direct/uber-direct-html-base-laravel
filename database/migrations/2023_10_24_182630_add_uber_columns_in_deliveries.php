<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('deliveries', function (Blueprint $table) {
            $table->string('uber_id')
                ->after('order_id')
                ->nullable()
                ->comment("Uber delivery ID from response.");

            $table->json('data')
                ->after('courier_id')
                ->nullable()
                ->comment("Data returned from Uber API's response.");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('deliveries', function (Blueprint $table) {
            $table->dropColumn('uber_id');
            $table->dropColumn('data');
        });
    }
};
