<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('address_extra')
                ->after('address')
                ->nullable()
                ->comment("Segunda linha do endereço, normalmente usado para o bairro.");

            $table->string('note')
                ->after('address_extra')
                ->nullable()
                ->comment("Observações adicionais, normalmente usado para o complemento do endereço.");

            $table->string('phone_number')
                ->after('note')
                ->nullable()
                ->comment("Número do telefone do consumidor.");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('address_extra');
            $table->dropColumn('note');
            $table->dropColumn('phone_number');
        });
    }
};
