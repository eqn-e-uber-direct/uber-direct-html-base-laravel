<?php

return [
    'active' => env('EQN_STATUS', 1),
    'path' => env('EQN_PATH', 'https://euqueronestle.com.br/api/uber'),
    'username' => env('EQN_USERNAME', 'uber-direct'),
    'password' => env('EQN_PASSWORD', 'm33g-ub3r'),

    'schedules' => [
        'updates' => [
            'deliveries' => filter_var(
                env('SCHEDULE_UPDATE_DELIVERIES', 'true'),
                FILTER_VALIDATE_BOOLEAN
            ),
            // `deliveries_hour` must be a valid string for time in 24 hours format.
            'deliveries_hour' => env('SCHEDULE_UPDATE_DELIVERIES_HOUR', '16:50'),
            'refresh_token' => filter_var(
                env('SCHEDULE_REFRESH_TOKEN', 'true'),
                FILTER_VALIDATE_BOOLEAN
            ),
        ]
    ]
];