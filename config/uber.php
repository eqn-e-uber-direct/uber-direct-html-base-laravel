<?php

return [
    // Dados principais da conexão com o Uber Direct.
    'path' => env('UBER_PATH', 'https://api.uber.com/v1'),

    'login_path' => env('UBER_LOGIN_PATH', 'https://login.uber.com/oauth/v2/token'),

    'client_secret' => env('UBER_CLIENT_SECRET'),

    'client_id' => env('UBER_CLIENT_ID'),

    'customer_id' => env('UBER_CUSTOMER_ID'),

    'webhook' => [
        'status' => env('UBER_WEBHOOK_STATUS_KEY')
    ],

    // Dados relacionados com o usuário distribuidor.
    'user' => [
        'name' => env('USER_NAME', 'Página Viva CRM'),
        'phone' => env('USER_PHONE', '+5511980951236'),
        'store_code' => env('USER_STORE_CODE', '1'),
    ],

    'address' => [
        'street' => env('USER_ADDRESS_STREET', 'Rua Cardeal Arcoverde, 1627'),
        'extra' => env('USER_ADDRESS_EXTRA', 'Pinheiros'),
        'note' => env('USER_ADDRESS_NOTE', 'Opção de estacionamento no Bolsão para motos localizado na esquina com a Rua Mateus Grou, 646 - Pinheiros - CEP 05415-040'),
        'zipcode' => env('USER_ADDRESS_CEP', '05407002'),
    ],

    'barcode' => [
        'number' => env('USER_BARCODE_NUMBER', '19450413'),
        'type' => env('USER_BARCODE_TYPE', 'CODE39'),
    ],

    // Dados relacionados com a janela de horários para as entregas.
    // Esses campos devem ser valores numéricos inteiros que serão
    // os minutos adicionados a data gerada no momento da entrega.
    // Se pickup_ready for 10, significa que vai ser adicionado 10
    // minutos ao now(). Se for usada a variável "hour", então tem
    // que ser a hora específica em vez de ser informada a quantidade
    // de minutos. Ex: 19:00.

    'delivery' => [
        'pickup_ready' => [
            'min' => env('UBER_DELIVERY_PICKUP_READY'),
            'hour'=> env('UBER_DELIVERY_PICKUP_READY_HOUR'),
        ],
        'pickup_deadline' => [
            'min' => env('UBER_DELIVERY_PICKUP_DEADLINE'),
            'hour' => env('UBER_DELIVERY_PICKUP_DEADLINE_HOUR'),
        ],
        'dropoff_ready' => [
            'min' => env('UBER_DELIVERY_DROPOFF_READY'),
            'hour' => env('UBER_DELIVERY_DROPOFF_READY_HOUR'),
        ],
        'dropoff_deadline' => [
            'min' => env('UBER_DELIVERY_DROPOFF_DEADLINE'),
            'hour' => env('UBER_DELIVERY_DROPOFF_DEADLINE_HOUR'),
        ],
    ],
];